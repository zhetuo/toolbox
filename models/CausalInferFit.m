% 2 params, assuming uniform prior
clear; close all;
direct='..\data\output_arcmin\';
load([direct 'subj7Data.mat'])
outputFolder='..\report\BayesianFit\Figures\';

subj=[1:7];%[3:7];
for i=subj
    i

    fitResult{i}.reErrV=subInfo{i}.reErr;
    fitResult{i}.hitV=subInfo{i}.hitV;
    fitResult{i}.respV=2*(subInfo{i}.respV-0.5);
    fitResult{i}.top1V=subInfo{i}.top1V;
    fitResult{i}.driftDV=subInfo{i}.driftDD;
    fitResult{i}.stimGapV=subInfo{i}.stimGapV;
    
    for j=length(fitResult{i}.respV):-1:1
        if fitResult{i}.top1V(j)
            fitResult{i}.posCueV(j) = fitResult{i}.driftDV(j);
        else
            fitResult{i}.posCueV(j) = -fitResult{i}.driftDV(j);    
        end
        
        if fitResult{i}.stimGapV(j)<0
            fitResult{i}.reV(j)=-fitResult{i}.reErrV(j);  % positive means it aids subject's correct decision
        else
            fitResult{i}.reV(j)=fitResult{i}.reErrV(j);
        end
    end
    
    Num=20;
    for randI=Num:-1:1
        disp(sprintf('subject %d, random sample %d', i, Num-randI))
        % resample to get training set
        [xt,idx]=resample2uniform(fitResult{i}.posCueV,20,400); xt=double(xt);
        xs=double(fitResult{i}.reErrV(idx));
        rV=fitResult{i}.respV(idx);

        %fitting
        %%
        for iter=10:-1:1
            f = @(SD)costFunCI(SD, [xt;xs],rV);
            init=[2*rand(1,3)+0.5 rand()];
            [sdV(iter,:),costV(iter)]=fmincon(f,init,[],[],[],[],[0.5 0.5 0.5 0],[4 4 10 1],[],optimoptions('fmincon','Display','off'));
        end
        [~,minIdx]=min(costV);
        SDM(randI,:)=sdV(minIdx,:);
%         f = @(SD)costFunCI(SD, [xt;xs],rV); 
%         SDM(randI,:)=fmincon(f,[4*rand(1,3) rand()],[],[],[],[],zeros(1,4),[4 4 10 1],[],optimoptions('fmincon','Display','off'));
    end
    
    fitResult{i}.SDM=SDM;
    %%
    for randI=40:-1:1
        [xt,idx]=resample2uniform(fitResult{i}.posCueV,20,400); xt=double(xt);
        xs=double(fitResult{i}.reErrV(idx));
        rV=fitResult{i}.respV(idx);
        for ri=Num:-1:1
            costM(ri,randI)=costFunCI(SDM(ri,:), [xt;xs],rV);
        end
    end
    [~,minIdx]=min(mean(costM')); 
    SD=SDM(minIdx,:);
    fitResult{i}.SD=SD;

    
    save('fitResult_CI2.mat','fitResult')
end

