function [xe,resp,SDe,P_right]=BayesianPredictN(xM,SD)
    SDsum=0;
    for i=1:length(SD)
        SDsum=SDsum+1/SD(i)^2;
    end
    SDe=1/sqrt(SDsum);
    
    xSum=0;
    for i=1:(length(SD)-1)
        xSum=xSum+xM(i,:)/SD(i)^2;
    end
    xe=xSum*(SDe^2);
    resp=normrnd(xe,SDe)>0;
    
    P_right=1-normcdf(0,xe,SDe);
end