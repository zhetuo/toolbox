
function cost=costFunN(SD, xV,respV)
    
    errSum=0;
    
    SDsum=0;
    for j=1:length(SD)
        SDsum=SDsum+1/SD(j)^2;
    end
    SDe=1/sqrt(SDsum);
    
    for i=length(respV):-1:1
        xSum=0;
        for j=1:(length(SD)-1)
            xSum=xSum+xV(j,i)/SD(j)^2;
        end
        xe=xSum*(SDe^2);
      
        
        if respV(i)>0
            p=1-normcdf(0,xe,SDe); % probability that subject chose right
        else
            p=normcdf(0,xe,SDe); % probability that subject chose left
        end
        p=p+1E-10;
        errSum=errSum-log(p);
%         errSum=errSum+(1/(1+exp(-sum(tmp(:))))-double(respV(i)))^2;
    end
    
    cost=errSum/length(respV);

end