function cost=costFunCI(SD,xM,respV)

    errSum=0;
    sdA=SD(1); sdV=SD(2); sdP=SD(3); p0=SD(4);

    for i=1:length(respV)
        xA=xM(1,i);  xV=xM(2,i); 
        
        tmpA=(xV-xA)^2*sdP^2+xV^2*sdA^2+xA^2*sdV^2;
        tmpB=sdV^2*sdA^2+sdV^2*sdP^2+sdA^2*sdP^2;
        P_xV_xA_C1=exp(-tmpA/(2*tmpB))/(2*pi*sqrt(tmpB));


        tmpC=sdV^2+sdP^2;  tmpD=sdA^2+sdP^2;
        P_xV_xA_C2=exp(-(xV^2/tmpC+xA^2/tmpD)/2)/(2*pi*sqrt(tmpC*tmpD));
    
        P_C1=P_xV_xA_C1*p0/(P_xV_xA_C1*p0+P_xV_xA_C2*(1-p0));
        P_C2=1-P_C1;
        
        % if C=1 (common cause), then integrate xV and xA
        SDe_C1=1/sqrt(1/sdV^2+1/sdA^2+1/sdP^2);
        xe_C1=(xV/sdV^2+xA/sdA^2)*SDe_C1^2;
        
        % if C=2 (separate causes), then choose one that is more certain
        SDe_V=1/sqrt(1/sdV^2+1/sdP^2);
        xe_V=(xV/sdV^2)*SDe_V^2;
        
        SDe_A=1/sqrt(1/sdA^2+1/sdP^2);
        xe_A=(xA/sdA^2)*SDe_A^2;
        
        uncertain_V=normcdf(0,xe_V,SDe_V); 
        if uncertain_V>0.5
            uncertain_V=1-uncertain_V;
        end
        
        uncertain_A=normcdf(0,xe_A,SDe_A); 
        if uncertain_A>0.5
            uncertain_A=1-uncertain_A;
        end
        
        if uncertain_V>uncertain_A
            xe_C2=xe_A;
            SDe_C2=SDe_A;
        else
            xe_C2=xe_V;
            SDe_C2=SDe_V;
        end
        
        xe=P_C1*xe_C1+P_C2*xe_C2;
        SDe=P_C1*SDe_C1+P_C2*SDe_C2;
        
        if respV(i)>0
            p=1-normcdf(0,xe,SDe); % probability that subject chose right
        else
            p=normcdf(0,xe,SDe); % probability that subject chose left
        end
        p=p+1E-10;
        errSum=errSum-log(p);
%         errSum=errSum+(1/(1+exp(-sum(tmp(:))))-double(respV(i)))^2;
    end
    
    cost=errSum/length(respV);

end