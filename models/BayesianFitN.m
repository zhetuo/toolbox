function out=BayesianFitN(fitResult,idx)

out=fitResult;
SDnum=size(fitResult{1}.featureM,1)+1;

Num=20;
for i=1:length(fitResult)
    disp(sprintf('subject %d', i))

    fM=[];
    clear SDM;
    
    tic;
    for randI=Num:-1:1
        
        % resample to get training set
        for fi=1:size(fitResult{i}.featureM,1)
            fM(fi,:)=fitResult{i}.featureM(fi,idx{i});     
        end
        rV=fitResult{i}.respV(idx{i});
        % fitting
        for iter=10:-1:1
            f = @(SD)costFunN(SD,fM,rV);
              [sdV(iter,:),costV(iter)]=fmincon(f,4*rand(1,SDnum),[],[],[],[],zeros(1,SDnum),[4*ones(1,SDnum-1) 20],[],optimoptions('fmincon','Display','off'));
      
        end
        [~,minIdx]=min(costV);
        SDM(randI,:)=sdV(minIdx,:);

    end
    

    for randI=40:-1:1
        test_idx=[];
        for gap=[-2 -1 1 2]
            test_idx=[test_idx randsample(find(fitResult{i}.stimGapV==gap),50,1)];
        end
        for fi=1:size(fitResult{i}.featureM,1)
            test_fM(fi,:)=fitResult{i}.featureM(fi,test_idx);     
        end
        test_rV=fitResult{i}.respV(test_idx);
        for ri=Num:-1:1
            costM(ri,randI)=costFunN(SDM(ri,:),test_fM,test_rV);
        end
    end
    [~,minIdx]=min(mean(costM')); 
    SD=SDM(minIdx,:);

    out{i}.SD=SD;
    
    toc;
end
end