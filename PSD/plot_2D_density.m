function DM_normalized=plot_2D_density(XD,YD,xv,yv)
    % XD: a vector of x positions
    % YD: a vector of y positions
    % xv: a vector of bins (left edge) for XD
    % yv: a vector of bins (left edge) for YD
    stepX=mean(diff(xv)); stepY=mean(diff(yv));
    densityMap=zeros(length(yv),length(xv));
    for i=1:length(XD)
        for j=1:length(YD)
            xi=floor((XD(j)-xv(1))/stepX);
            yi=floor((YD(j)-yv(1))/stepY);
            if xi>0 && xi<=length(xv) && yi>0 && yi<=length(yv)
                densityMap(yi,xi)=densityMap(yi,xi)+1;
            end
        end
    end

    DM_smooth=imgaussfilt(densityMap,2); 
    DM_normalized=DM_smooth/max(DM_smooth(:));
    DM_normalized(DM_smooth<5)=NaN;
    imagesc(xv+1.5*stepX, yv+1.5*stepY, DM_normalized);
    xlabel('horizontal (arcmin)'); ylabel('vertical (arcmin)');
    grid on;
    set(gca,'FontSize',14)
    set(gca, 'ydir', 'reverse');
    
    colorM=parula(256);
    stepN=30;
    color0=[(1:-(1-colorM(1,1))/stepN:colorM(1,1))' (1:-(1-colorM(1,2))/stepN:colorM(1,2))' (1:-(1-colorM(1,3))/stepN:colorM(1,3))'];
    caxis([0 1])
    colormap( [color0; parula(256)] )
    colorbar;
end