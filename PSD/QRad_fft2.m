% This function estimates the spatiotemporal spectral density of retinal input flow via fft^2 method.
%
% INPUT:
%       Kval = vector of spatial frequencies 
%       KRadAng = vectors of angles    
%       Nw = Number of temporal frequency samples
%       Xe, Ye =  eye movement trace
%       Win = hanning window 
% OUTPUT:
%       PS = a 2D power spectrum
%
%
%  Software developed in the Active Perception Laboratory, University of Rochester.
%  If used, please cite the following articles:
%  Mostofi et al, Spatiotemporal Content of Saccade Transients, Current Biology, 2020, DOI:https://doi.org/10.1016/j.cub.2020.07.085 
%  Intoy & Rucci, Finely tuned eye movements enhance visual acuity. Nat Commun 11, 795 (2020). https://doi.org/10.1038/s41467-020-14616-2




function PS  = QRad_fft2(Kval, Nw, KRadAng, Xe, Ye,Win)
 
    Ns = length(Xe);       % Number of eye movement samples
    Nk = length(Kval);     % Number of spatial frequency samples

    PS = zeros(Nw, Nk);    

    for SpatialFrequency = 1:length(Kval)
        kk = Kval(SpatialFrequency);
        for kAng = KRadAng
            kx = kk*cos(kAng);  ky = kk*sin(kAng);
            eTerm = exp(-i*2*pi*(kx*Xe+ky*Ye)/60);
            fAll = fft(eTerm'.*Win,Nw);
            PS(:, SpatialFrequency) = PS(:, SpatialFrequency) + fftshift(abs(fAll).^2);
        end
    end

    PS = PS/length(KRadAng);

end