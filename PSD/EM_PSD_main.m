%  An example to calculate spatiotemporal power specturm of retinal input flow, using QRad_W and QRad_fft2 functions.
%
%  Software developed in the Active Perception Laboratory, University of Rochester.
%  If used, please cite the following articles:
%  Mostofi et al, Spatiotemporal Content of Saccade Transients, Current Biology, 2020, DOI:https://doi.org/10.1016/j.cub.2020.07.085 
%  Intoy & Rucci, Finely tuned eye movements enhance visual acuity. Nat Commun 11, 795 (2020). https://doi.org/10.1038/s41467-020-14616-2


clear all; close all; clc;

load('exampleEM.mat') % load in example eye movements
wNum = 512; % number of samples along temporal frequency w
kNum = 512; % number of samples along spatial frequency k
PixelAngle=1; % spatial sampling frequecy in the display, arcmin/pixel
sampFreq = 1000; % temporal sampling frequency of the eye movements, Hz
Win = hann(wNum);  

kV = 10.^linspace( log10(0.01), log10(30/PixelAngle),kNum); % samples along spatial frequency k
wV = linspace(0, sampFreq, wNum);                           % samples along temporal frequency w
KRadAng=linspace(0, 2*pi, 120); 


Q_kw_sacc=zeros(wNum,kNum);
Q_kw_drift=zeros(wNum,kNum);

for i=1:10  % for each eye movement
    disp(sprintf('eye movement number %d',i));

    Q_kw_sacc = Q_kw_sacc + QRad_fft2(kV, wNum, KRadAng, saccTraces(i).x, saccTraces(i).y, Win);
    Q_kw_drift = Q_kw_drift + QRad_Welch(kV, wNum, KRadAng, driftTraces(i).x, driftTraces(i).y, Win, sampFreq);
end
Q_kw_sacc = Q_kw_sacc./sum(Q_kw_sacc(:)); Q_kw_sacc=fftshift(Q_kw_sacc,1); 
Q_kw_drift = Q_kw_drift./sum(Q_kw_drift(:)); 
save('PS.mat','Q_kw_sacc','Q_kw_drift')

%% plot power spectrum in spatiotempoal frequency domain
kLim = 30; kStart = 2; wLim = 35.5; wStart = 3;
figure1=figure; drawPS_2D(figure1,Q_kw_sacc,wV,kV,wStart,wLim,kStart,kLim,[-90 -30]);
figure2=figure; drawPS_2D(figure2,Q_kw_drift,wV,kV,wStart,wLim,kStart,kLim,[-90 -30]);
