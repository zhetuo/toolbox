% This function estimates the spatiotemporal spectral density of retinal input flow via the factorization method first described in Kuang et al., 2012.
%
% INPUT:
%       Kval = vector of spatial frequencies 
%       KRadAng = vectors of angles    
%       Nw = Number of temporal frequency samples
%       Xe, Ye =  eye movement trace
%       Win = hanning window 
%       fs = temproal sampling frequency
% OUTPUT:
%       PS = a 2D power spectrum
%
%
%  Software developed in the Active Perception Laboratory, University of Rochester.
%  If used, please cite the following articles:
%  Mostofi et al, Spatiotemporal Content of Saccade Transients, Current Biology, 2020, DOI:https://doi.org/10.1016/j.cub.2020.07.085 
%  Intoy & Rucci, Finely tuned eye movements enhance visual acuity. Nat Commun 11, 795 (2020). https://doi.org/10.1038/s41467-020-14616-2


function PS  = QRad_Welch(Kval, Nw, KRadAng, Xe, Ye,Win, fs)
 
Ns = length(Xe);       % Number of eye movement samples
Nk = length(Kval);     % Number of spatial frequency samples

PS = zeros(Nw, Nk);

for SpatialFrequency = 1:length(Kval)
    kk = Kval(SpatialFrequency);
    contAng = 0;
    for kAng = KRadAng
        contAng = contAng + 1;
        kx = kk*cos(kAng);  ky = kk*sin(kAng);
        ss = exp(-i*2*pi*(kx*Xe+ky*Ye)/60);
        ps_trace = pwelch(ss,Win',length(Win)/2,Nw,fs,'twosided');
        PS(:, SpatialFrequency) = PS(:, SpatialFrequency) + ps_trace;
    end
end

PS = PS/length(Kval);




