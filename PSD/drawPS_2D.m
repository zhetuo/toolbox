%  Plot 2-dimension spectral density in spatiotemporal frequency domain. 
%
%  Software developed in the Active Perception Laboratory, University of Rochester.
%  If used, please cite the following articles:
%  Mostofi et al, Spatiotemporal Content of Saccade Transients, Current Biology, 2020, DOI:https://doi.org/10.1016/j.cub.2020.07.085 
%  Intoy & Rucci, Finely tuned eye movements enhance visual acuity. Nat Commun 11, 795 (2020). https://doi.org/10.1038/s41467-020-14616-2


function drawPS_2D(hfig,S_kw,wV,kV,wStart,wLim,kStart,kLim,clim)
%   This function draws the 2d power spectrum 
%   INPUT:
%           hfig : handle of figure
%           S_kw : 2D power specturm 
%           wV : samples along temporal frequency w
%           kV : samples along spatial frequency k
%           wStart, start sample of temporal frequency w
%           wLim : limit of temporal frequency w in Hz
%           kStart, start sample of spatial frequency k
%           kLim : limit of spatial frequency k in cpd
%           clim : limits of color bar



send = find(kV<=kLim, 1,'last');
tend = find(wV<=wLim, 1, 'last');
wV_p = wV(wStart:tend);
kV_p = kV(kStart:send);
S_kw_p = S_kw(wStart:tend, kStart:send);

figure(hfig);

contourf(kV_p, wV_p, 10*log10(S_kw_p),256,'LineStyle','none');
if exist('clim','var')
    caxis(clim);
end
 colorbar;
hold on

set(gca, 'YScale', 'log', 'XScale', 'log')

colormap hot
set(gca,'XTick' , [0.1 1 10 20 50], 'XTickLabel', {'0.1','1', '10','20','50'});
set(gca,'YTick' , [0 10 20 50], 'YTickLabel', {'5','10', '20','50'});
xlabel('spatial frequency (cycles/deg)')
ylabel('temporal frequency (Hz)')

ylim([wV_p(1) wLim])
xlim([kV_p(1) kLim])
axis square

hold off

return;

