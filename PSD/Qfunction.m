function pw = Qfunction(t, s, D)
%pw = Qfunction(t, s, D)
% return the power of brownian motions with diffusion coefficient D, spatial
% frequency s, and temporal freuqency t as a 3d matrix sized:
% [length(t) x length(s) x length(D)]
% inputs:
%   t vector of temporal frequencies
%   s vector of spatial frequencies
%   D in degrees^2/s
% author Janis Intoy

t = t(:);
s = s(:)';
D = reshape(D, [1, 1, length(D)]);

% Q = 2*D*S^2 / (4*pi^2*D^2*S^4 + T^2)
temp = bsxfun(@times, D, s.^2);
den = bsxfun(@plus, 4*pi^2*temp.^2, t.^2);

pw = 2 * bsxfun(@rdivide, temp, den);


% sanity check
%{
pw2 = zeros(length(t), length(s), length(D));
for ti = 1:length(t)
    for si = 1:length(s)
        for di = 1:length(D)
            pw2(ti, si, di) = 2 * D(di) * s(si)^2 ...
                / (4 * pi^2 * D(di)^2 * s(si)^4 + t(ti)^2);
        end
    end
end
assert(max(abs(pw(:) - pw2(:))) < .1);
%}

end