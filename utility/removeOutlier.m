function [out,idx,range]=removeOutlier(A,k)
   
    c=-1/(sqrt(2)*erfcinv(3/2));
    
    m=median(A);
    MAD=c*median(abs(A-m));
    
    if ~exist('k','var')
        k=3;
    end
    
    idx=find(A>k*MAD+m|A<-k*MAD+m);
    out=A;
    out(idx)=[];
    
    range=[min(out) max(out)];
end