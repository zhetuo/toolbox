function [kIdx,prototype]=kMeanTrace(traceNT,kNum,tt)

    rowN=2; colN=round(kNum/2); % rowN=floor(sqrt(kNum)); colN=kNum/rowN;
    kIdx=kmeans(traceNT,kNum);

    figure('position',[10 10 1200 600]); 
    for ki=1:kNum
        prototype.mu(ki,:)=mean(traceNT(kIdx==ki,:));
        prototype.sd(ki,:)=std(traceNT(kIdx==ki,:));
        subplot(rowN,colN,ki); hold on;
        if exist('tt','var')
            plot(tt,traceNT(kIdx==ki,:)');
            shadedErrorBar(tt,prototype.mu(ki,:),prototype.sd(ki,:),'k',1);
        else
            plot(traceNT(kIdx==ki,:)');
            shadedErrorBar(1:size(prototype.mu,2),prototype.mu(ki,:),prototype.sd(ki,:),'k',1);
        end
        title(sprintf('group %d (%.2f%%)',ki,sum(kIdx==ki)*100/length(kIdx)));
    end

end



