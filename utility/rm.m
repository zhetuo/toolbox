function y=rm(x,i)
    if exist('i','var')
        if length(i)>1
            y=x-mean(rmNaN(x(:,i))')';
        else
            y=x-x(:,i);
        end
    else
        y=x-mean(rmNaN(x)')';
    end
end