function [out,X,Y]=Gaussian2D(x,y,meanV,varM)

    if size(varM,1)==1
        varM=[varM(1)^2 0; 0 varM(2)^2];
    end

    [X,Y]=meshgrid(x,y);

    
    for i=length(x):-1:1
        for j=length(y):-1:1
            xy=[X(j,i); Y(j,i)];
            out(j,i)=1/(2*pi*sqrt(det(varM)))*exp(-((xy-meanV')'*inv(varM)*(xy-meanV'))/2);
        end
    end

     

end