function fileNames=findFile(direct,searchName,trim)
    folderInfo = dir(direct);
    fileNames=[];
    for i=1:length(folderInfo)
        containBOOL=1;
        for j=1:length(searchName)
            containBOOL=containBOOL && contains(lower(folderInfo(i).name),lower(searchName{j}));
        end
        if containBOOL
            if exist('trim','var')
                fileNames=[fileNames {folderInfo(i).name(1+trim(1):end-trim(2))}];
            else
                fileNames=[fileNames {folderInfo(i).name}];
            end
        end
    end
end