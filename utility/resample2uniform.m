function [valV, idxV]=resample2uniform(inputV,binNum,N)
    binEdges=linspace(prctile(inputV,5),prctile(inputV,95),binNum+1);
    binEdges(1)=min(inputV); binEdges(end)=max(inputV);
    valV=[]; idxV=[];
    for binIdx=1:length(binEdges)-1 
        idx=randsample(find(inputV>=binEdges(binIdx)&inputV<binEdges(binIdx+1)),round(N/binNum),1);
        if size(idx,1)>size(idx,2)
            idx=idx';
        end
        idxV=[idxV idx];
        valV=[valV inputV(idx)];
    end
end