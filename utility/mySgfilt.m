function y = mySgfilt(x,k,F,dn)
    for i=size(x):-1:1
        y(i,:) = sgfilt(x(i,:),k,F,dn);
    
    end


