% n: length of Hann window, is expected to be odd
% x: raw eye trace
function y=hannSmooth(x, n)
    hannFilter=hann(n);
    hannFilter=hannFilter/sum(hannFilter);
    
    tmp=conv(x,hannFilter);
    y=tmp(((n-1)/2+1):(length(tmp)-(n-1)/2));

end
