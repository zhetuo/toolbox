function [mu,sd,meanV]=bootstrap(x,B)
    for i=B:-1:1
        meanV(i)=mean(randsample(x,length(x),1));
    end
    mu=mean(meanV);
    sd=std(meanV);
end