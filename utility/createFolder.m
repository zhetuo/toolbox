function outputFolder=createFolder(direct)

    outputFolder=direct;
    if ~exist(outputFolder,'dir')
        mkdir(outputFolder);
    end
end