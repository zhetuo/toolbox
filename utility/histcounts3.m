function [n, xmid, ymid, zmid] = histcounts3(x, y, z, nbins)
% [n, xmid, ymid, zmid] = histcounts3(x, y, z, nbins)
% histcounts3 in three dimensions (n) with bin centers. nbins specifies the
% number of bins to use in each dimension [y, x, z] or the bin centers as a
% cell {ymid, xmid, zmid}
if iscell(nbins)
     xmid = nbins{2};
     ymid = nbins{1};
     zmid = nbins{3};
     
     nbins = [length(ymid), length(xmid), length(zmid)];
else
    xmin = quantile(x, .05);%min(x);
    xmax = quantile(x, .95);%max(x);
    xmid = linspace(xmin, xmax, nbins(2));
    
    ymin = quantile(y, .05);%min(y);
    ymax = quantile(y, .95);%max(y);
    ymid = linspace(ymin, ymax, nbins(1));
    
    zmin = quantile(z, .05);%min(z);
    zmax = quantile(z, .95);%max(z);
    zmid = linspace(zmin, zmax, nbins(3));
end


xsub = interp1(xmid, 1:nbins(2), x, 'nearest');
ysub = interp1(ymid, 1:nbins(1), y, 'nearest');
zsub = interp1(zmid, 1:nbins(3), z, 'nearest');

kill = isnan(xsub) | isnan(ysub) | isnan(zsub);
xsub(kill) = [];
ysub(kill) = [];
zsub(kill) = [];

ind = sub2ind(nbins, ysub, xsub, zsub);

uind = unique(ind);

n = zeros(nbins);
for ii = 1:length(uind)
    [r, c, d] = ind2sub(nbins, uind(ii));
    n(r, c, d) = sum(ind == uind(ii));
end

end