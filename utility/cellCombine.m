function out=cellCombine(data,varName)
    
    if exist('varName','var')  % combine a specific field
        if size(varName,1)>size(varName,2)
            varName=varName';
        end
        out=[];
        for i=1:length(data)
            if isstruct(data)
                out=[out eval(['cellVector(i).' varName])];
            elseif iscell(data)
                out=[out eval(['cellVector{i}.' varName])];
            end
        end
    else % combine all fields
        fieldNames=fieldnames(data{1});

        for fi=1:length(fieldNames)
            tmp=data{1}.(fieldNames{fi});
            if iscell(tmp)
                for i=1:size(tmp,1)
                    for j=1:size(tmp,2)
                        if isstruct(tmp{i,j})
                            for di=1:length(data) 
                                tmpCell{di}=data{di}.(fieldNames{fi}){i,j};
                            end
                            out.(fieldNames{fi}){i,j}=cellCombine(tmpCell);
                        else

                           out.(fieldNames{fi}){i,j}=[];
                           for di=1:length(data) 
                               out.(fieldNames{fi}){i,j}=[out.(fieldNames{fi}){i,j} data{di}.(fieldNames{fi}){i,j}];
                           end

                        end
                    end
                end
            else
                if length(tmp)<100 
                    out.(fieldNames{fi})=data{1}.(fieldNames{fi});
                else
                   out.(fieldNames{fi})=[];
                   for di=1:length(data) 
                       tmp=data{di}.(fieldNames{fi});
                       if size(tmp,1)>size(tmp,2)
                            tmp=tmp';
                        end
                       out.(fieldNames{fi})=[out.(fieldNames{fi}) tmp];
                   end
                end
            end
        end

    end
end