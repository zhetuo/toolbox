classdef rawVideoReader < handle
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Properties
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (SetAccess = private)
        fid = 0 
        width = 0
        height = 0
        frameSize = 0
        totalFrames = 0
        currentPos = 0
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Methods
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods
        %%%% Constructor(fName, width, height)
        %{ 
          Inputs:
           - fName:  file name string
           - width:  pixel width of video frame
           - height: pixel height of video frame
        
          Returns: rawVideoReader object
        %}        
        function obj = rawVideoReader(fName, width, height)
            obj.fid = fopen(fName,'r');
            obj.width = width;
            obj.height = height;
            obj.frameSize = width*height;
            fseek(obj.fid,0,'eof');
            obj.totalFrames = ftell(obj.fid)/obj.frameSize;
            fseek(obj.fid,0,'bof');
        end
        
        %%%% getTotalFrames()
        % Returns: total number of frames in video file
        function totalFrames = getTotalFrames(obj)
            totalFrames = obj.totalFrames;
        end
        
        %%%% reset()
        % Resets file pointer in rawVideoReader object
        function reset(obj)
            fseek(obj.fid,0,'bof');
            obj.currentPos = ftell(obj.fid);
        end
        
        %%%% getNextFrame()
        % Returns: array of next frame in video file
        %          - dim: {height width}
        function frame = getNextFrame(obj)
            frame = fread(obj.fid,[obj.width obj.height])';
            obj.currentPos = ftell(obj.fid);
        end
        
        %%%% getFrameRange(fstart,numFrames)
        %{ 
          Inputs:
           - fstart:     starting frame to read
           - numFrames:  number of frames to read
        
          Returns: 3D array of frames in specified range
                   - dim: {height width frame_i}
        %} 
        function frames = getFrameRange(obj,fstart,numFrames)
            if isempty(fstart)
                fstart = obj.currentPos;
            end
            
            fremainder = obj.totalFrames - fstart + 1;
            if (isempty(numFrames) || numFrames > fremainder)
                numFrames = fremainder;
            end
            
            startInd = (fstart-1)*obj.frameSize;
            fseek(obj.fid,startInd,'bof');
            byteLen = numFrames*obj.frameSize;
            frames = fread(obj.fid,byteLen);
            frames = reshape(frames,[obj.width obj.height numFrames]);
            frames = permute(frames,[2 1 3]);
            obj.currentPos = ftell(obj.fid);
        end
        
        %%%% showFrame(frame_i)
        %{
          Input:
           - frameNum: frame index to be extracted and displayed        
        %}
        function showFrame(obj,frame_i)
            f = obj.getFrameRange(frame_i,1);
            figure(frame_i);
            imshow(f,[]);
        end
    end
end