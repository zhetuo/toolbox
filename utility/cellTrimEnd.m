function out=cellTrimEnd(data,Dur)
    out=data;
    fieldNames=fieldnames(data);
    
    for fi=1:length(fieldNames)
        tmp=out.(fieldNames{fi});
        if iscell(tmp)
            for i=1:size(tmp,1)
                for j=1:size(tmp,2)
                    if isstruct(tmp{i,j})
                        out.(fieldNames{fi}){i,j}=cellTrimEnd(tmp{i,j},Dur);
                    else
                        out.(fieldNames{fi}){i,j}=out.(fieldNames{fi}){i,j}(:,Dur);
                    end
                end
            end
        else
            if length(tmp)<length(Dur)
                
            else

               tmp=out.(fieldNames{fi});
               if size(tmp,1)>size(tmp,2)
                    tmp=tmp';
                end
               out.(fieldNames{fi})=tmp(:,Dur);
              
            end
        end
    end

end