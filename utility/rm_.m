function out=rm_(in)
    out=in;
    if iscell(in)
        for i=1:length(in)
            out{i}=rm_(in{i});
        end
    else
        for i=1:length(in)
            if in(i)=='_'
                out(i)=' ';
            end
        end
    end
    

