function y=rmNaN(x)
    if size(x,1)>size(x,2)
        x=x';
    end
    if size(x,1)>1
        y=x(:,~any(isnan(x)));
    else
        y=x(:,~isnan(x));
    end
end