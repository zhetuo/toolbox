% Helmholtz or Harms angles for small angles
theta = deg2rad(linspace(-2, 2, 50)); % 

err = bsxfun(@times, theta, cos(theta(:)) .* sin(theta(:))) - ...
    bsxfun(@times, theta.^2, cos(theta(:)) .* sin(theta(:)).^3);

figure(10); clf;
imagesc(rad2deg(theta), rad2deg(theta), abs(rad2deg(err') * 60));
xlabel('$\theta_h$ (deg)', 'Interpreter', 'latex'); 
ylabel('$\theta_v$ (deg)', 'Interpreter', 'latex');
colorbar;
standardPlot;
title('$|H_{e,h} - \theta_h|$', 'Interpreter', 'latex');
axis square;
print('Helmholtz_Harms_compare.png', '-dpng');

%% plot an example showing the two coordinate systems to a single point
rL = [0, -.5, 0];
rR = [0, .5, 0];

xyz1 = [.5, .6, .4] + rL;
xyz1 = xyz1 / norm(xyz1);

xyz2 = [.45, -.56, .25] + rR;
xyz2 = xyz2 / norm(xyz2);

figure(11); clf;
% left eye
plot3([rL(1), xyz1(1)], [rL(2), xyz1(2)], [rL(3), xyz1(3)], 'k.-', 'markersize', 20);
hold on;
plot3(xyz1(1)*[1,1], xyz1(2)*[1,1], [xyz1(3), rL(3)], 'k--');
plot3(xyz1(1)*[1,1], [xyz1(2), rL(2)], xyz1(3)*[1,1], 'k--');
patch([rL(1), xyz1(1), xyz1(1)], [rL(2), rL(2), xyz1(2)], [rL(3), rL(3), rL(3)],...
    'FaceColor', 'b', 'EdgeColor', 'none', 'FaceAlpha', .4);
patch('XData', [rL(1), xyz1(1), xyz1(1)], 'YData', [0, 0, 0]+rL(2), 'ZData', [0, 0, xyz1(3)] +rL(3),...
    'FaceColor', 'r', 'EdgeColor', 'none', 'FaceAlpha', .4);
patch('XData', [rL(1), xyz1(1), xyz1(1)], 'YData', [rL(2), rL(2), xyz1(2)], 'ZData', [rL(3), xyz1(3), xyz1(3)],...
    'FaceColor', 'c', 'EdgeColor', 'none', 'FaceAlpha', .4);

% right eye
plot3([rR(1), xyz2(1)], [rR(2), xyz2(2)], [rR(3), xyz2(3)], 'k.-', 'markersize', 20);
plot3(xyz2(1)*[1,1], xyz2(2)*[1,1], [xyz2(3), rR(3)], 'k--');
plot3(xyz2(1)*[1,1], [xyz2(2), rR(2)], xyz2(3)*[1,1], 'k--');
patch([rR(1), xyz2(1), xyz2(1)], [rR(2), rR(2), xyz2(2)], [rR(3), rR(3), rR(3)],...
    'FaceColor', 'g', 'EdgeColor', 'none', 'FaceAlpha', .4);
patch('XData', [rR(1), xyz2(1), xyz2(1)], 'YData', [0, 0, 0]+rR(2), 'ZData', [0, 0, xyz2(3)] +rR(3),...
    'FaceColor', 'm', 'EdgeColor', 'none', 'FaceAlpha', .4);
patch('XData', [rR(1), xyz2(1), xyz2(1)], 'YData', [rR(2), rR(2), xyz2(2)], 'ZData', [rR(3), xyz2(3), xyz2(3)],...
    'FaceColor', 'y', 'EdgeColor', 'none', 'FaceAlpha', .4);

% left eye angles
rr = .3;
theta_h = atan2(xyz1(2) - rL(2), xyz1(1) - rL(1));
tt = linspace(0, theta_h, 30);
plot3(rr*cos(tt)+rL(1), rr*sin(tt)+rL(2), zeros(size(tt))+rL(3), 'b-', 'linewidth', 5);

rr = .3;
theta_v = atan2(xyz1(3) - rL(3), xyz1(1) - rL(1));
tt = linspace(0, theta_v, 30);
plot3(rr*cos(tt)+rL(1), zeros(size(tt))+rL(2), rr*sin(tt)+rL(3), 'r-', 'linewidth', 5);

rr = .4;
h2 = atan2(xyz1(2) - rL(2), norm(xyz1([1, 3]) - rL([1, 3])));
tt = linspace(0, h2, 30);
plot3(rr*cos(tt) * cos(theta_v) +rL(1), rr*sin(tt) +rL(2), rr*sin(theta_v)*cos(tt) +rL(3), 'c-', 'linewidth', 5);

% right eye angles
rr = .3;
theta_h = atan2(xyz2(2) - rR(2), xyz2(1) - rR(1));
tt = linspace(0, theta_h, 30);
plot3(rr*cos(tt)+rR(1), rr*sin(tt)+rR(2), zeros(size(tt))+rR(3), 'g-', 'linewidth', 5);

rr = .3;
theta_v = atan2(xyz2(3) - rR(3), xyz2(1) - rR(1));
tt = linspace(0, theta_v, 30);
plot3(rr*cos(tt)+rR(1), zeros(size(tt))+rR(2), rr*sin(tt)+rR(3), 'm-', 'linewidth', 5);

rr = .4;
h2 = atan2(xyz2(2) - rR(2), norm(xyz2([1, 3]) - rR([1, 3])));
tt = linspace(0, h2, 30);
plot3(rr*cos(tt) * cos(theta_v) +rR(1), rr*sin(tt) +rR(2), rr*sin(theta_v)*cos(tt) +rR(3), 'y-', 'linewidth', 5);


grid on; axis image;
set(gca, 'View', [-48   27]);
xlabel('x'); ylabel('y'); zlabel('z');
set(gca, 'YDir', 'reverse');
xlim([-.01, 1.1]); ylim([-.8, .8]); zlim([-.01, .7]);
standardPlot;
print('em_coordinate_example.png', '-dpng');