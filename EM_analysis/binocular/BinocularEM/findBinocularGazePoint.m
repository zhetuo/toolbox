function [r_bgp, s_R, s_L] = findBinocularGazePoint(b, u_R, u_L, r_cyc)
% [r_BGP, s_R, s_L] = findBinocularGazePoint(b, u_R, u_L)
% compute binocular gaze point according to RFM manual - see eqns 8.31,
% 8.32, 8.36

u_RL = sum(u_R .* u_L, 1); % this variable may be big for long recordings
den = 1 - u_RL.^2;

bul = b' * u_L;
bur = b' * u_R;

s_R = (bul .* u_RL - bur) ./ den;
s_L = - (bur .* u_RL - bul) ./ den;

r_bgp = r_cyc + (bsxfun(@times, s_R, u_R) + bsxfun(@times, s_L, u_L)) / 2;

end
