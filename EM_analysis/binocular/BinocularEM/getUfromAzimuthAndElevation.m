function u = getUfromAzimuthAndElevation(az, el)
D = sqrt(1 + tan(az(:)).^2 + tan(el(:)).^2);
u = bsxfun(@rdivide, [ones(length(az), 1), tan(az(:)), tan(el(:))],  D)';
end