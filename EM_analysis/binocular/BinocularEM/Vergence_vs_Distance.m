% vergence angle by distance
p = 6;
d = linspace(130, 180, 100);

vergence = 2 * atan2(p/2, d) * 180 / pi * 60;

%%
figure(1); clf; hold on;
plot(vergence, d);
xlabel('vergence angle (arcmin)');
ylabel('distance (cm)');

%% plot theoretical horopter
d = 150;
vergence0 = 2 * atan2(p/2, d);

a = deg2rad(linspace(-20, 20, 100)); 
mL = tan(pi/2 - vergence0/2 - a);
mR = tan(pi/2 + vergence0/2 + a);

xh = p * (mL + mR)./ (mL - mR);
dh = mL .* xh - p/2;

figure(1); clf;
plot(xh, dh);