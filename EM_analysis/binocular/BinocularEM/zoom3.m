function zoom3(center, diam)
if ischar(center)
    xlim auto;
    ylim auto;
    zlim auto;
else
    xlim(center(1) + diam * [-1/2, 1/2]);
    ylim(center(2) + diam * [-1/2, 1/2]);
    zlim(center(3) + diam * [-1/2, 1/2]);
end
end