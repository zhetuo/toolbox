function [az, el] = getAzimuthAndElevation(r1, r2)
% [az, al] = getAzimuthAndAltitude(r1, r2) gets the two angles where r1 is
% treated as the origin
% r1 and r2 are 3d vectors
% az, al in radians

az = atan2(r2(2) - r1(2), r2(1) - r1(1));
el = atan2(r2(3) - r1(3), r2(1) - r1(1));
end