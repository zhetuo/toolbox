%% illustration of head-fixed binocular viewing
% this script illustrates the math of binocular eye movements
rng(0);

%% parameters
ipd = 6.3;              % IPD (cm)
eye_diam = 2.42;        % diameter of eye (cm)
d = 8;                % distance between cyclopean eye and monitor center
monitorDim = [0, 10, 10];% monitor dimensions

% eye movement simulations
nFrames = 30;
diffusionConstant = 20; % arcmin^2/s
Fs = 60;

%% positions in 3d space
r_R = [0, ipd/2, 0]';        % right eye
r_L = [0, -ipd/2, 0]';         % left eye
r_mon = [d, 0, 0]';         % monitor center

%% derived parameters
% position of cyclopean eye
r_cyc = (r_R + r_L) / 2;

% estimate origin eye angle relative to x-axis at fixation
[theta_Rh_offset, theta_Rv_offset] = getAzimuthAndElevation(r_R, r_mon);
[theta_Lh_offset, theta_Lv_offset] = getAzimuthAndElevation(r_L, r_mon);

% fixation vergence
theta_fv = acos((r_L - r_mon)' * (r_R - r_mon) / ...
    norm(r_L - r_mon) / norm(r_R - r_mon));

% baseline vector between the two eyes
b = r_R - r_L;

% gram-schmidt orthogonalization for helmholtz coordinate system
% assume that the head is stationary in time
yh = b / norm(b);
xh = ([1, 0, 0]' - ([1, 0, 0] * yh) * yh) / norm([1, 0, 0]' - ([1, 0, 0] * yh) * yh);
zh = cross(yh, xh);
R = [xh(:), yh(:), zh(:)];

%% draw the 3d space
[x_sph, y_sph, z_sph] = sphere(50);

figure(1); clf;

% plot eye spheres
h_eyeL = surf(x_sph * eye_diam/2 + r_L(1),...
    y_sph * eye_diam/2 + r_L(2),...
    z_sph * eye_diam/2 + r_L(3),...
    x_sph);
hold on;

h_eyeR = surf(x_sph * eye_diam/2 + r_R(1),...
    y_sph * eye_diam/2 + r_R(2),...
    z_sph * eye_diam/2 + r_R(3),...
    x_sph);

colormap gray;
set([h_eyeL, h_eyeR], 'EdgeColor', 'none', 'FaceAlpha', .4);

% plot monitor and center of monitor
h_mon = patch('XData', r_mon(1) + monitorDim(1)*[-1/2 -1/2, 1/2, 1/2],...
    'YData', r_mon(2) + monitorDim(2)*[-1/2 -1/2, 1/2, 1/2],...
    'ZData', r_mon(3) + monitorDim(3)*[-1/2 1/2, 1/2, -1/2],...
    'FaceColor', [.5, .5, .5], 'FaceAlpha', .7);
plot3(r_mon(1), r_mon(2), r_mon(3), '.', 'markersize', 20);

% plot line of sight during fixation
h_los_R = plot3( [r_R(1), r_mon(1)], ...
    [r_R(2), r_mon(2)], ...
    [r_R(3), r_mon(3)], 'k--');
h_los_L = plot3( [r_L(1), r_mon(1)], ...
    [r_L(2), r_mon(2)], ...
    [r_L(3), r_mon(3)], 'k--');

axis image;
xlabel('x'); ylabel('y'); zlabel('z');
set(gca, 'View', [-33.3000+180 8.0000], 'YDir', 'reverse');

%% change the rotations of the eyes
[Xe, Ye] = EM_Brownian(diffusionConstant, Fs, nFrames, 2);
Xe = bsxfun(@minus, Xe, Xe(:, 1));
Ye = bsxfun(@minus, Ye, Ye(:, 1));

% initialize a structure to save data
vergence = nan(nFrames, 2);
vergence_raw = nan(nFrames, 2);
for fr = 1:nFrames
    
    % define horizontal and vertical rotations of each eye
    theta_Rh_raw = deg2rad(Xe(1, fr) / 60); % convert to radians
    theta_Rv_raw = deg2rad(Ye(1, fr)) / 60;
    theta_Lh_raw = deg2rad(Xe(2, fr) / 60);
    theta_Lv_raw = deg2rad(Ye(2, fr) / 60);
    
    % the absolute angles of the eye from the x-axis include the offset
    theta_Rh = theta_Rh_raw + theta_Rh_offset;
    theta_Rv = theta_Rv_raw + theta_Rv_offset;
    theta_Lh = theta_Lh_raw + theta_Lh_offset;
    theta_Lv = theta_Lv_raw + theta_Lv_offset;
    
    % get the unit vectors of each line of sight
    u_R = getUfromAzimuthAndElevation(theta_Rh, theta_Rv);
    u_L = getUfromAzimuthAndElevation(theta_Lh, theta_Lv);
    
    % compute binocular gaze point. s are th distances from each eye to the
    % line of closest approach (loca)
    [r_bgp, s_R, s_L] = findBinocularGazePoint(b, u_R, u_L, r_cyc);
    loca_R = r_R + s_R * u_R;
    loca_L = r_L + s_L * u_L;
    
    % compute cyclopean line of sight
    [theta_h_cyc, theta_v_cyc] = getAzimuthAndElevation(r_cyc, r_bgp);
    u_cyc = getUfromAzimuthAndElevation(theta_h_cyc, theta_v_cyc);
    
    % the way I used to calculate cyclopean line of sight (i.e. version)
    theta_h_old = (theta_Rh + theta_Lh) / 2;
    theta_v_old = (theta_Rv + theta_Lv) / 2;
    u_cyc_old = getUfromAzimuthAndElevation(theta_h_old, theta_v_old);
    
    % unit vectors of each line of sight in Helmholtz coordinates (head
    % coordinates)
    u_R_hh = u_R' * R;
    u_L_hh = u_L' * R;
    
    % helmoltz angles of each eye (eqns 8.16 and 8.17)
    [hh1_R, hh2_R] = getHelmholtzAngles(u_R_hh);
    [hh1_L, hh2_L] = getHelmholtzAngles(u_L_hh);
    
    % ocular vergence (equation 8.18)
    vergence_v = abs(hh1_R - hh1_L);
    vergence_h = abs(hh2_R - hh2_L);
    vergence(fr, :) = [vergence_h, vergence_v];
    
    vergence_raw(fr, :) = abs([theta_Rh - theta_Lh, theta_Rv - theta_Lv]);
    
    % update plots with current lines of sights
    s = max([1.25 * d, s_R, s_L]);
    h_R = plot3(r_R(1) + [0, s*u_R(1)],...
        r_R(2) + [0, s*u_R(2)],...
        r_R(3) + [0, s*u_R(3)], 'k-');
    h_L = plot3(r_L(1) + [0, s*u_L(1)],...
        r_L(2) + [0, s*u_L(2)],...
        r_L(3) + [0, s*u_L(3)], 'k-');
    
    
    
    % plot line of closest approach
    h_loca = plot3([loca_R(1), loca_L(1)],...
        [loca_R(2), loca_L(2)],...
        [loca_R(3), loca_L(3)],...
        'k.-', 'markersize', 20);
    
    % plot cyclopean line of sight through bgp
    h_cyc = plot3(r_cyc(1) + [0, s*u_cyc(1)],...
        r_cyc(2) + [0, s*u_cyc(2)],...
        r_cyc(3) + [0, s*u_cyc(3)],...
        'r-');
    h_bgp = plot3(r_bgp(1), r_bgp(2), r_bgp(3), 'r.', 'markersize', 20);
    
    % plot old cyclopean line of sight
    %     h_cyc_old = plot3(r_cyc(1) + [0, s*u_cyc_old(1)],...
    %         r_cyc(2) + [0, s*u_cyc_old(2)],...
    %         r_cyc(3) + [0, s*u_cyc_old(3)],...
    %         'g-');
    
    set([h_R, h_L, h_cyc], 'linewidth', 2);
    set(gca, 'View', [-33.3000+180 8.0000], 'YDir', 'reverse');
    xlim([-eye_diam, 1.25*d]);
    ylim([-ipd, ipd]);
    zlim(monitorDim(3)/2 * [-1, 1]);
    
    pause(1/Fs);
    
    if fr < nFrames
        delete([h_R, h_L, h_loca, h_cyc, h_bgp]);
        if exist('h_cyc_old', 'var')
            delete(h_cyc_old);
        end
    end
    
    
    
end




%% plot vergence traces
% there seems to be an almost negligible difference betwen these two
% calculations for vergence when using realistic parameters.
%
figure(2); clf;
yyaxis left; hold on;
plot(1:nFrames, rad2deg(vergence(:, 1))*60);
plot(1:nFrames, rad2deg(vergence_raw(:, 1))*60);
lineThrough(rad2deg(theta_fv) * 60, 'k', gca, '--');
ylabel('horizontal vergence (arcmin)');
standardPlot;
yyaxis right; hold on;
plot(1:nFrames, rad2deg(vergence(:, 2))*60);
plot(1:nFrames, rad2deg(vergence_raw(:, 2))*60, '--');
ylabel('vertical vergence (arcmin)');
standardPlot
%}


%% parameteres to illustrate BGP in a single frame
% nFrames = 14; rng(0);
% diffusionConstant = 1000000; % arcmin^2/s
delete([h_los_R, h_los_L]);
xlim([0, 10]);
ylim([-5, 5]);
zlim([-3, 3]);
standardPlot;