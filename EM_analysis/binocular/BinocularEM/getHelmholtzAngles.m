function [hh_1, hh_2] = getHelmholtzAngles(u)
% [hh_1, hh2] = getHelmholtzAngles(u), gets helmholtz angles from unit
% vector u
% (eqns 8.16 and 8.17 in RFM manual) 
hh_1 = atan2(u(:, 3), u(:, 1));
hh_2 = atan2(u(:, 2), sqrt(sum(u(:, [1, 3]).^2, 2)));
end