function [saccadePairs, saccadeMon] = pairSaccadicEvents(pptrials, trialIdx, params)
% [saccadePairs, saccadeMon] = pairSaccadicEvents(pptrials, trialIdx)
% arrays of paired microsaccade/saccade information (saccadePairs) or
% candidates of monocular microsaccades with the same structure.

%% find microsaccades using more lenient criteria
MIN_VEL = 90;
MIN_DUR = 8;


Trial1 = findMicrosaccades(pptrials{trialIdx, 1}, ...
    'minvel',MIN_VEL,'minmsa', 0.5,'maxmsa', 30, 'mduration', MIN_DUR);
Trial2 = findMicrosaccades(pptrials{trialIdx, 2}, ...
    'minvel',MIN_VEL,'minmsa', 0.5,'maxmsa', 30, 'mduration', MIN_DUR);

%% find saccade pairs using regular criteria
[saccadePairs_reg, used1, used2] =...
    pairSaccades(pptrials{trialIdx, 1}, pptrials{trialIdx, 2}, trialIdx,...
    [], [], params);

%% for unused regular saccades, search for saccades in the other eye using
% the more lenient criteria
[saccadePairs_1, used1, ~] = ...
    pairSaccades(pptrials{trialIdx, 1}, Trial2, trialIdx, used1, [], params);

[saccadePairs_2, used2, ~] = ...
    pairSaccades(pptrials{trialIdx, 2}, Trial1, trialIdx, used2, [], params);
saccadePairs_2 = reverseEyes(saccadePairs_2);

saccadePairs = [saccadePairs_reg; saccadePairs_1; saccadePairs_2];

% sort by start time of right eye
if ~isempty(saccadePairs)
    [~, sidx] = sort(saccadePairs(:, 1));
    saccadePairs = saccadePairs(sidx, :);
end

%% the leftovers are candidates for monocular microsaccades
saccadeMon1 = monocularSaccades(pptrials{trialIdx, 1}, pptrials{trialIdx, 2}, used1, trialIdx,...
    params);

saccadeMon2 = monocularSaccades(pptrials{trialIdx, 2}, pptrials{trialIdx, 1}, used2, trialIdx,...
    params);
saccadeMon2 = reverseEyes(saccadeMon2);

saccadeMon1 = checkInvalid(saccadeMon1, pptrials);
saccadeMon2 = checkInvalid(saccadeMon2, pptrials);

saccadeMon = [saccadeMon1; saccadeMon2];

end

function saccadeMon = checkInvalid(saccadeMon, pptrials)
flds = {'blinks', 'notracks', 'invalid'};
kill = false(size(saccadeMon, 1), 1);
for smi = 1:size(saccadeMon, 1)
     trial_index = saccadeMon(smi, 21);
     start = saccadeMon(smi, 1);
     stop = saccadeMon(smi, 2);
     if isnan(start)
         start = saccadeMon(smi, 3); 
         stop = saccadeMon(smi, 4);
     end
     
     for eye = 1:2
         for fi = 1:length(flds)
             ss = pptrials{trial_index, eye}.(flds{fi}).start;
             ee = ss + pptrials{trial_index, eye}.(flds{fi}).duration - 1;
         
            if any(abs(ee - start) < 100)
                kill(smi) = true; 
                break;
            end
            
            if any(start > ss & start < ee) | any(stop > ss & stop < ee)
                kill(smi) = true; 
                break;
            end
         end
         if kill(smi)
             break; 
         end
     end
end
saccadeMon(kill, :) = [];
end

function saccadePairs = reverseEyes(saccadePairs)
if isempty(saccadePairs)
    return; 
end
saccadePairs = saccadePairs(:, [3, 4, 1, 2,...
    6, 5, 8, 7, 10, 9, 15:18, 11:14, 19:21]);

tmp = saccadePairs(:, 19);
tmp(saccadePairs(:, 19) == 1) = 2;
tmp(saccadePairs(:, 19) == 2) = 1;
saccadePairs(:, 19) = tmp;
end

function [saccadePairs, used1, used2] = pairSaccades(trial1, trial2, trialIdx, used1, used2, params)
% going through right eye saccades and find matches in the left eye
MIN_OVERLAP = 5;

if isfield(trial1, params.startTimes{1})
    thistrial = trial1;
else
    thistrial = trial2;
end

taskstarts = nan(length(params.tasks), 1);
taskstops = nan(length(params.tasks), 1);
for ti = 1:length(params.tasks)
    taskstarts(ti) = thistrial.(params.startTimes{ti});
    taskstops(ti) = thistrial.(params.stopTimes{ti}); 
end

% right eye information
starts1 = [trial1.microsaccades.start,...
    trial1.saccades.start];
stops1 = starts1 - 1 +...
    [trial1.microsaccades.duration,...
    trial1.saccades.duration];
mids1 = (starts1 + stops1) / 2;

amps1 = [trial1.microsaccades.amplitude,...
    trial1.saccades.amplitude];
dirs1 = [trial1.microsaccades.angle,...
    trial1.saccades.angle];

% left eye information
starts2 = [trial2.microsaccades.start,...
    trial2.saccades.start];
stops2 = starts2 - 1 +...
    [trial2.microsaccades.duration,...
    trial2.saccades.duration];
mids2 = (starts2 + stops2) / 2;

amps2 = [trial2.microsaccades.amplitude,...
    trial2.saccades.amplitude];
dirs2 = [trial2.microsaccades.angle,...
    trial2.saccades.angle];


if ~exist('used1', 'var') || isempty(used1)
    used1 = false(size(starts1));
    
end
if ~exist('used2', 'var') || isempty(used2)
    used2 = false(size(starts2));
end


% search for saccade pairs
saccadePairs = zeros(1, 21);
sidx = 0;

for si = 1:length(starts1)
    if used1(si)
        continue; 
    end
    m1 = mids1(si);
    [~, s2] = min(abs(mids2 - m1));
    if used2(s2)
        continue;
    end
    
    % compute the temporal overlap between the two events
    combostart = max(starts1(si), starts2(s2));
    combostop = min(stops1(si), stops2(s2));
    overlap = combostop - combostart;
    
    if overlap > MIN_OVERLAP
        sss = (starts1(si) + starts2(s2)) / 2;
        task = find(sss >= taskstarts & sss <= taskstops) - 1;
        
        if isempty(task)
            continue; 
        end
        
        % find peak velocity - but make sure it's not an overshoot
        vel1 = trial1.velocity_sg(starts1(si):stops1(si));
        [~, pkloc] = findpeaks(vel1);
        if isempty(pkloc)
            [~, pkloc] = max(vel1);
        elseif length(pkloc) > 1
            [~, mi] = min(abs(pkloc - length(vel1)/2));
            pkloc = pkloc(mi);
        end
        pkvel1 = vel1(pkloc);
        
        vel2 = trial2.velocity_sg(starts2(s2):stops2(s2));
        [~, pkloc] = findpeaks(vel2);
        if isempty(pkloc)
            [~, pkloc] = max(vel2);
        elseif length(pkloc) > 1
            [~, mi] = min(abs(pkloc - length(vel2)/2));
            pkloc = pkloc(mi);
        end
        pkvel2 = vel2(pkloc);
        
        pos1_xstart = trial1.x.shifted(starts1(si));
        pos1_ystart = trial1.y.shifted(starts1(si));
        pos1_xstop = trial1.x.shifted(stops1(si));
        pos1_ystop = trial1.y.shifted(stops1(si));
        
        pos2_xstart = trial2.x.shifted(starts2(s2));
        pos2_ystart = trial2.y.shifted(starts2(s2));
        pos2_xstop = trial2.x.shifted(stops2(s2));
        pos2_ystop = trial2.y.shifted(stops2(s2));
        
        sidx = sidx + 1;
        used1(si) = true;
        used2(s2) = true;
        saccadePairs(sidx, :) = [starts1(si), stops1(si), starts2(s2), stops2(s2),...
            amps1(si), amps2(s2), dirs1(si), dirs2(s2), pkvel1, pkvel2,...
            pos1_xstart, pos1_ystart, pos1_xstop, pos1_ystop,...
            pos2_xstart, pos2_ystart, pos2_xstop, pos2_ystop,...
            0, task, trialIdx];
    end
end

if sidx == 0
    saccadePairs = [];
end


end

function saccadeMon = monocularSaccades(trial1, trial2, used1, trialIdx, params)
if isfield(trial1, params.startTimes{1})
    thistrial = trial1;
else
    thistrial = trial2;
end

taskstarts = nan(length(params.tasks), 1);
taskstops = nan(length(params.tasks), 1);
for ti = 1:length(params.tasks)
    taskstarts(ti) = thistrial.(params.startTimes{ti});
    taskstops(ti) = thistrial.(params.stopTimes{ti}); 
end

% right eye information
starts1 = [trial1.microsaccades.start,...
    trial1.saccades.start];
stops1 = starts1 - 1 +...
    [trial1.microsaccades.duration,...
    trial1.saccades.duration];

amps1 = [trial1.microsaccades.amplitude,...
    trial1.saccades.amplitude];
dirs1 = [trial1.microsaccades.angle,...
    trial1.saccades.angle];


% search for saccade pairs
saccadeMon = nan(1, 21);
sidx = 0;

for si = 1:length(starts1)
    if used1(si)
        continue;
    end
    
    sss = starts1(si);
    task = find(sss >= taskstarts & sss <= taskstops) - 1;
    
    if isempty(task)
        continue; 
    end
    
    % find peak velocity - but make sure it's not an overshoot
    vel1 = trial1.velocity_sg(starts1(si):stops1(si));
    [~, pkloc] = findpeaks(vel1);
    if isempty(pkloc)
        [~, pkloc] = max(vel1);
    elseif length(pkloc) > 1
        [~, mi] = min(abs(pkloc - length(vel1)/2));
        pkloc = pkloc(mi);
    end
    pkvel1 = vel1(pkloc);
    
    % also get pkvel2 to compare monocular microsaccades
    vel2 = trial2.velocity_sg(starts1(si):stops1(si));
    pkvel2 = max(vel2);
    
    pos1_xstart = trial1.x.shifted(starts1(si));
    pos1_ystart = trial1.y.shifted(starts1(si));
    pos1_xstop = trial1.x.shifted(stops1(si));
    pos1_ystop = trial1.y.shifted(stops1(si));
    
    sidx = sidx + 1;
    used1(si) = true;
    saccadeMon(sidx, :) = [starts1(si), stops1(si), nan, nan,...
        amps1(si), nan, dirs1(si), nan, pkvel1, pkvel2,...
        pos1_xstart, pos1_ystart, pos1_xstop, pos1_ystop,...
        nan, nan, nan, nan,...
        1, task, trialIdx];
end

if sidx == 0
    saccadeMon = [];
end
end