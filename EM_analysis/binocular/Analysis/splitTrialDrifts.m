function [fix, fix_label] = splitTrialDrifts(pptrials, trialIndex, EYE)

fixEnd = round(pptrials{trialIndex, 1}.timeRampStart);
taskStart = pptrials{trialIndex, EYE}.analysisStart;



isdrift = pptrials{trialIndex, 1}.isdrift & pptrials{trialIndex, 2}.isdrift;
[startInd, stopInd] = getIndicesFromBin(isdrift);

fix(length(startInd)) = struct('x', [], 'y', []);
fix_label = nan(length(startInd), 1);
for ii = 1:length(startInd)
    ss = startInd(ii);
    ee = stopInd(ii);
    
    fix(ii).x = pptrials{trialIndex, EYE}.x.shifted(ss:ee);
    fix(ii).y = pptrials{trialIndex, EYE}.y.shifted(ss:ee);
    
    if ss < fixEnd
        fix_label(ii) = 0;
    elseif ss > taskStart
        fix_label(ii) = 1;
    else
        fix_label(ii) = 2;
    end
end
end