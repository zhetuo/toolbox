function out = analyzeDriftDiffusions(pptrials, counts, nboots, params, DOPLOT, imgPath)

subjname = pptrials{1, 1}.Subject_Name;

buffer = 50;

fix1 = struct('x', [], 'y', []);
fix2 = struct('x', [], 'y', []);
fix_vers = struct('x', [], 'y', []);
fix_verg = struct('x', [], 'y', []);


fix_info = struct('task', [], 'badcalibration', [], 'exclude', [], 'trialIdx', [], 'driftIdx', []);

ppt_file = dir(fullfile(pathToBOX(), 'CorrugationDiscrimination', 'Data', ...
    sprintf('%s_pptrials.mat', subjname)));

save_name = fullfile(imgPath, sprintf('%s_diffcon_%i.mat', subjname, nboots));
save_file = dir(save_name);
fprintf('\tlooking for / saving to %s\n', sprintf('%s_diffcon_%i.mat', subjname, nboots));
if true %~exist(save_name, 'file') || ...
    %    (exist(save_name, 'file') && datenum(save_file.date) < datenum(ppt_file.date))
    fprintf('\tcomputing new diffusion constants\n');
    out = struct();
    
    cnt = 0;
    for ii = 1:size(pptrials, 1)
        if counts.exclude(ii) || counts.lowperformance(ii)
            continue;
        end
        
        taskstarts = nan(length(params.tasks), 1);
        taskstops = nan(length(params.tasks), 1);
        for ti = 1:length(params.tasks)
            taskstarts(ti) = pptrials{ii, 1}.(params.startTimes{ti});
            taskstops(ti) = pptrials{ii, 1}.(params.stopTimes{ti});
        end
        
        isdrift = pptrials{ii, 1}.isdrift & pptrials{ii, 2}.isdrift;
        
        [ss, ee] = getIndicesFromBin(isdrift);
        
        for jj = 1:length(ss)
            s = ss(jj) + buffer;
            e = ee(jj) - buffer;
            
            if e - s < 0
                continue;
            end
            
            task = find(s >= taskstarts & s <= taskstops) - 1;
            if isempty(task)
                continue;
            end
            
            cnt = cnt + 1;
            exclude = false;
            
%             fix1(cnt).x = pptrials{ii, 1}.x.shifted(s:e);
%             fix1(cnt).y = pptrials{ii, 1}.y.shifted(s:e);
%             
%             fix2(cnt).x = pptrials{ii, 2}.x.shifted(s:e);
%             fix2(cnt).y = pptrials{ii, 2}.y.shifted(s:e);
            
            fix1(cnt).x = pptrials{ii, 1}.x.orig(s:e);
            fix1(cnt).y = pptrials{ii, 1}.y.orig(s:e);
            
            fix2(cnt).x = pptrials{ii, 2}.x.orig(s:e);
            fix2(cnt).y = pptrials{ii, 2}.y.orig(s:e);
            
            fix_vers(cnt).x = pptrials{ii,1}.vs_components(s:e, 1)';
            fix_vers(cnt).y = pptrials{ii,1}.vs_components(s:e, 2)';
            
            fix_verg(cnt).x = pptrials{ii,1}.vs_components(s:e, 3)';
            fix_verg(cnt).y = pptrials{ii,1}.vs_components(s:e, 4)';
            
            if any(isnan(fix1(cnt).x) | isinf(fix1(cnt).x) | isnan(fix2(cnt).y) | isinf(fix2(cnt).y))
                exclude = true;
            end
            
            
            fix_info.task(cnt) = task;
            
            fix_info.badcalibration(cnt) = counts.badcalibration(ii);
            fix_info.exclude(cnt) = exclude;
            fix_info.trialIdx(cnt) = ii;
            fix_info.driftIdx(cnt) = jj;
        end
    end
    
    
    %% calculate diffusion constants for each task
    diffCon = nan(length(params.plottasks), 4);
    diffConBoot = nan(length(params.plottasks), 4, nboots);
    Dsq = cell(length(params.plottasks), 4, 2);
    
    for ti = params.plottasks
        useTrials = fix_info.task(:) == ti & ~fix_info.exclude(:);
        useTrials = useTrials(:);
        
        uIdx = find(useTrials);
        
        [~, ~, dc_fix_1, ~, Dsq_fix_1, SingleSegmentDsq_fix_1] = ...
            CalculateDiffusionCoef(fix1(useTrials));
        [~, ~, dc_fix_2, ~, Dsq_fix_2, SingleSegmentDsq_fix_2] = ...
            CalculateDiffusionCoef(fix2(useTrials));
        
        useTrials2 = useTrials & ~fix_info.badcalibration(:);
        uIdx2 = find(useTrials2);
        [~, ~, dc_fix_vers, ~, Dsq_fix_vers, SingleSegmentDsq_fix_vers] = ...
            CalculateDiffusionCoef(fix_vers(useTrials2));
        [~, ~, dc_fix_verg, ~, Dsq_fix_verg, SingleSegmentDsq_fix_verg] = ...
            CalculateDiffusionCoef(fix_verg(useTrials2));
        
        diffCon(ti+1, 1) = dc_fix_1;
        diffCon(ti+1, 2) = dc_fix_2;
        diffCon(ti+1, 3) = dc_fix_vers;
        diffCon(ti+1, 4) = dc_fix_verg;
        
        Dsq{ti+1, 1, 1} = Dsq_fix_1;
        Dsq{ti+1, 1, 2} = SingleSegmentDsq_fix_1;
        
        Dsq{ti+1, 2, 1} = Dsq_fix_2;
        Dsq{ti+1, 2, 2} = SingleSegmentDsq_fix_2;
        
        Dsq{ti+1, 3, 1} = Dsq_fix_vers;
        Dsq{ti+1, 3, 2} = SingleSegmentDsq_fix_vers;
        
        Dsq{ti+1, 4, 1} = Dsq_fix_verg;
        Dsq{ti+1, 4, 2} = SingleSegmentDsq_fix_verg;
        ti1 = ti + 1;
        %% do bootstrapping also
        parfor bi = 1:nboots
            rs = randsample(uIdx, length(uIdx), true);
            [~, ~, dc_fix_1] = ...
                CalculateDiffusionCoef(fix1(rs));
            [~, ~, dc_fix_2] = ...
                CalculateDiffusionCoef(fix2(rs));
            
            if ~isempty(uIdx2)
                rs = randsample(uIdx2, length(uIdx), true);
                [~, ~, dc_fix_vers] = ...
                    CalculateDiffusionCoef(fix_vers(rs));
                [~, ~, dc_fix_verg] = ...
                    CalculateDiffusionCoef(fix_verg(rs));
            else
                dc_fix_vers = nan;
                dc_fix_verg = nan;
            end
            
            diffConBoot(ti1, :, bi) = [dc_fix_1; dc_fix_2; dc_fix_vers; dc_fix_verg];
        end
    end
    if ~exist(save_name, 'file') % && nboots >= 100
        save(save_name, 'fix*', 'Dsq*', 'diffCon*');
    end
else
    fprintf('\tloading previous diffusion constants\n');
    load(save_name);
end

out.Dsq = Dsq;
out.diffCon = diffCon;
out.diffConBoot = diffConBoot;

if DOPLOT
    plotDriftDiffusions(out, params, subjname, imgPath);
end

end

