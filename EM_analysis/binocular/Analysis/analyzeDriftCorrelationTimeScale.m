function analyzeDriftCorrelationTimeScale(...
    pptrials, pptrials_orig, params, pixel2angle, dist2monitor, baseline, r_cyc, theta_offset,...
    counts, DOPLOT, imgPath)

winsizes = 31:60:400;
% winsizes = 151;

fname = fullfile(imgPath, 'drift_winsize_2.mat');

if exist(fname, 'file')
    temp = load(fname, 'winsizes');
else
    temp.winsizes = 0;
end

if (~(exist(fname, 'file') && isequal(winsizes, temp.winsizes)))
    fprintf('analyzing drift correlation by time scale\n');
    pptrials_winsize = cell(size(winsizes));
    out = cell(size(winsizes));
    for wi = 1:length(winsizes)
        fprintf('\twinsize = %i\n', winsizes(wi));
        pptrials_winsize{wi} = processInstantaneousEyeMovements(...
            pptrials, pptrials_orig, pixel2angle, dist2monitor, ...
            baseline, r_cyc, theta_offset, winsizes(wi));
        
        buffer = (winsizes(wi) - 1) / 2;
        
        imgPath2 = fullfile(imgPath, sprintf('winsize_%i', winsizes(wi)));
        if ~exist(imgPath2, 'dir')
            mkdir(imgPath2);
        end
        
        out{wi} = analyzeDrifts(pptrials_winsize{wi}, counts, params, DOPLOT, imgPath2, buffer);
    end
    save(fname, 'out', 'winsizes');
else
    load(fname, 'out');
end

plotCorrelationVWinsize(out, winsizes, pptrials{1}.Subject_Name, DOPLOT, imgPath);
end