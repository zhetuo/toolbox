function plotSaccadeInfo(out, params, subjname, imgPath)
if length(subjname) > 4
    subjname_print = subjname(1:4);
else
    subjname_print = subjname; 
end

% out is the output of analyzeDrifts.m
if ~exist('imgPath', 'var')
    imgPath = '';
end

tasks = params.tasks;
plottasks = params.plottasks + 1;
cols = params.taskColors;

fh = 600;

FONTSIZE = 20;
LFONTSIZE = 16;
SCATTERDOTSIZE = 15;

eye_labels = {'right eye', 'left eye'};

cols4 = params.taskEyeColors;

%% distributions of saccade amplitudes (1d)
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('amp')
xlabel('amplitude (arcmin)');
ylabel('sacc prob');

mysavefig('ms-amp-distribution');

%% distribution of saccade directions (polar)
fh = fh + 1;
figure(fh); clf;
h = nan(length(plottasks), 2);
for ti = plottasks
    tmpc = out.(sprintf('dir_%s_hist_bins', tasks{ti}));
    tmph = out.(sprintf('dir_%s_hist', tasks{ti}));
    for EYE = 1:2
        c = tmpc{EYE};
        n1 = tmph{EYE};
        h(ti, EYE) = polar([c, c(1)], [n1, n1(1)]);
        set(h(ti, EYE), 'Color', cols4{ti, EYE});
        hold on;
    end
end
set(h, 'linewidth', 2);
xlabel('sacc direction prob');
legend(h(:, 1), tasks);
mysavefig('ms-dir-distribution');

%% distributions of saccade peak velocities (1d)
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('pkvel')
xlabel('peak velocity (deg / s)');
ylabel('sacc prob');

mysavefig('ms-pkvel-distribution');

%% saccade main sequences
% graph for each task, containing both eyes
for ti = plottasks
    fh = fh + 1;
    figure(fh); clf; hold on;
    tmpa = out.(sprintf('amp_%s_data', tasks{ti}));
    tmpv = out.(sprintf('pkvel_%s_data', tasks{ti}));
    if isempty(tmpa{1}) || isempty(tmpv{1})
        continue; 
    end
    h = nan(2, 1);
    for EYE = 1:2
        h(EYE) = plot(tmpa{EYE}, tmpv{EYE}, '.', ...
            'Color', cols4{ti, EYE}, 'markersize', SCATTERDOTSIZE);
    end
    legend(h, eye_labels, 'FontSize', LFONTSIZE, 'Location', 'northeast');
    title(tasks{ti});
    xlabel('amplitude (arcmin)');
    ylabel('peak vel (deg / s)');
    standardPlot;
    set(gca, 'XScale', 'log', 'YScale', 'log');
    mysavefig(sprintf('%s_mainsequence', tasks{ti}));
end

%% amplitude: Left vs Right
for ti = plottasks
    if ~isfield(out, sprintf('amp_%s_rho_data', tasks{ti}))
        continue; 
    end
    tmp = out.(sprintf('amp_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('amp_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), true);
    title(sprintf('%s: amp (arcmin)', tasks{ti}));
    
    mysavefig(sprintf('%s_amp-corr', tasks{ti}));
end

%% amplitude: Left vs Right (horizontal component only)
for ti = plottasks
    if ~isfield(out, sprintf('amp_x_%s_rho_data', tasks{ti}))
        continue; 
    end
    
    tmp = out.(sprintf('amp_x_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('amp_x_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), false);
    title(sprintf('%s: amp (horizontal) (arcmin)', tasks{ti}));
    
    mysavefig(sprintf('%s_amp-x-corr', tasks{ti}));
end

%% direction: Left vs Right
for ti = plottasks
    if ~isfield(out, sprintf('dir_%s_rho_data', tasks{ti}))
        continue; 
    end
    
    tmp = out.(sprintf('dir_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('dir_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), false);
    
    xlim([-40, 400]); ylim([-40, 400]);
    title(sprintf('%s: dir (deg)', tasks{ti}));
    
    mysavefig(sprintf('%s_dir-corr', tasks{ti}));
end

%% peak velocity: Left vs Right
for ti = plottasks
    if ~isfield(out, sprintf('pkvel_%s_rho_data', tasks{ti}))
        continue; 
    end
    tmp = out.(sprintf('pkvel_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('pkvel_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), true);
    title(sprintf('%s: pk vel (deg/s)', tasks{ti}));
    
    mysavefig(sprintf('%s_pkvel-corr', tasks{ti}));
end

%% amplitude difference
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('amp_diff')
xlabel('amplitude difference (arcmin)');
ylabel('sacc prob');

mysavefig('ms-amp-diff-distribution');

%% amplitude difference (horizontal only)
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('V_amp')
xlabel('amplitude difference (arcmin)');
ylabel('sacc prob');
title('horz and vert differences');

mysavefig('ms-V-amp-distribution');

%% pkvel difference
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('pkvel_diff')
xlabel('peak vel difference (deg / s)');
ylabel('sacc prob');

mysavefig('ms-pkvel-diff-distribution');

%% version amplitudes (horz and vert)
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('S_ampxy')
xlabel('amplitude (arcmin)');
title('version amplitude components');

mysavefig('ms-S_ampxy-distribution');

%% version amplitudes (average)
fh = fh + 1;
figure(fh); clf; hold on;
plot_1d_dist('S_amp')
xlabel('amplitude (arcmin)');
title('version amplitude');

mysavefig('ms-S_amp-distribution');

%% vergence vs version components (horz)
for ti = plottasks
     if ~isfield(out, sprintf('VSx_%s_rho_data', tasks{ti}))
        continue; 
     end
    
    tmp = out.(sprintf('VSx_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('VSx_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), false);
    title(sprintf('%s: Vx v x (arcmin)', tasks{ti}));
    xlabel('x'); ylabel('Vx');
    
    mysavefig(sprintf('%s_VSx-corr', tasks{ti}));
end

%% vergence vs version components (vert)
for ti = plottasks
    if ~isfield(out, sprintf('VSy_%s_rho_data', tasks{ti}))
        continue; 
    end
     
    tmp = out.(sprintf('VSy_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('VSy_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), false);
    title(sprintf('%s: Vy v y (arcmin)', tasks{ti}));
    xlabel('y'); ylabel('Vy');
    
    mysavefig(sprintf('%s_VSy-corr', tasks{ti}));
end

%% nested function for saving figures
    function mysavefig(figname)
        annotation('textbox',[.02, .9, .1, .1], 'String', subjname_print,...
            'FitBoxToText', 'on', 'FontSize', FONTSIZE, 'FontName', 'arial',...
            'FontWeight', 'bold');
        
        if ~isempty(imgPath)
            fname = fullfile(imgPath, sprintf('%s_%s', subjname_print, figname));
            print([fname, '.png'], '-dpng');
            print([fname, '.eps'], '-depsc');
        end
    end

%% nested function to plot 1d distributions (not polar) across tasks & eye
    function plot_1d_dist(prop)
        hh = nan(length(plottasks), 2);
        for tii = plottasks
            tmpcc = out.(sprintf('%s_%s_hist_bins', prop, tasks{tii}));
            tmphh = out.(sprintf('%s_%s_hist', prop, tasks{tii}));
            for EYEi = 1:2
                if length(tmphh) < EYEi
                    break; 
                end
                hh(tii, EYEi) = plot(tmpcc{EYEi}(1:end-1), tmphh{EYEi}(1:end-1),...
                    'linewidth', 2, 'DisplayName', tasks{tii},...
                    'Color', cols4{tii, EYEi});
            end
        end
        
        for tii = plottasks
            tmpms = out.(sprintf('%s_%s_MS', prop, tasks{tii}));
            for EYEi = 1:2
                if length(tmpms) < EYEi
                    break; 
                end
                vertLineThrough(tmpms{EYEi}(1), cols4{tii, EYEi}, gca, '--');
            end
        end
        
        standardPlot;
        legend(hh(:, 1), tasks(plottasks), 'Location', 'northeast', 'FontSize', LFONTSIZE);
    end

%% nested function to plot correlation data
    function plotcorr(right, left, rho, p, islog, samelim)
        if ~exist('samelim', 'var')
            samelim = true; 
        end
        plot(right, left, '.', 'Color', cols{ti}, 'markersize', SCATTERDOTSIZE);
        xlabel('right eye'); ylabel('left eye');
        standardPlot;
        
        if islog
            set(gca, 'XScale', 'log', 'YScale', 'log');
        end
        
        xl = xlim;
        if samelim
            yl = xl;
            ylim(xl);
        else
            yl = ylim; 
        end
        
        if ~islog
            xx = xl(1) + .8 * diff(xl);
            yy = yl(1) + .05*diff(yl);
        else
            xx = 10.^(log10(xl(1)) + .8 * diff(log10(xl)));
            yy = 10.^(log10(yl(1)) + .05*diff(log10(yl)));
        end
        text(xx, yy,...
            sprintf('$$\\rho=%1.1f$$, $$p=%1.2f$$', rho, p),...
            'FontSize', FONTSIZE - 2, 'Interpreter', 'latex');
        plot(xl([1, end]), yl([1, end]), 'k--', 'linewidth', 1);
        axis image;
    end


end

