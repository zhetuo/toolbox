function out = analyzeDrifts_3d(pptrials, counts, params, DOPLOT, imgPath, buffer)
% this file contains drift analysis that requires good localization in
% space

if ~exist('buffer', 'var')
    buffer = 20;
else
    buffer = max(buffer, 20);
end

subjname = pptrials{1, 1}.Subject_Name;

tasks = params.tasks; %
startTimes = params.startTimes; % 
stopTimes =  params.stopTimes;


group_label = {'instantaneous', 'indvdrift'};
groupfunc = {@trimSegment, @subtractStartFromSegment};


out = struct();
%% 3d analysis requies good calibration
useTrials = ~counts.badcalibration & ~counts.notracks & ~counts.exclude ...
     & ~counts.lowperformance;

%% extract all vergence movements during drift
allVersX = cellfun(@(x) x.vs_components(:, 1)', pptrials(useTrials, 1),...
    'UniformOutput', false);
allVersY = cellfun(@(x) x.vs_components(:, 2)', pptrials(useTrials, 1),...
    'UniformOutput', false);

allVergX = cellfun(@(x) x.vs_components(:, 3)', pptrials(useTrials, 1),...
    'UniformOutput', false);
allVergY = cellfun(@(x) x.vs_components(:, 4)', pptrials(useTrials, 1),...
    'UniformOutput', false);

%% distribution of versions including all drifts
cs = params.drift.vergence_pos;
for gi = 1:length(groupfunc)
    g = groupfunc{gi};
    c = cs{gi};

    for ti = 1:length(tasks)
        vx = cellfun(@(x, y, z) ...
            g(...
                x(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))), ... % vergence data
                y.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))) & z.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti})))), ... % isdrift data
            allVersX, pptrials(useTrials,2), pptrials(useTrials, 1),...
            'UniformOutput', false);
        vy = cellfun(@(x, y, z) ...
            g(...
                x(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))), ... % vergence data
                y.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))) & z.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti})))), ... % isdrift data
            allVersY, pptrials(useTrials,2), pptrials(useTrials, 1),...
            'UniformOutput', false);
        
        vx = cat(2, vx{:});
        vy = cat(2, vy{:});
        
        nx = hist(vx, c);
        ny = hist(vy, c);
        nx = nx / sum(nx) ;
        ny = ny / sum(ny);
        
        mx = nanmean(vx);
        sdx = nanstd(vx);
        my = nanmean(vy);
        sdy = nanstd(vy);
        
        out.(sprintf('vs_S_%s_%s_data', group_label{gi}, tasks{ti})) = {vx, vx};
        out.(sprintf('vs_S_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mx, sdx / sqrt(length(vx))],...
            [my, sdy / sqrt(length(vy))]};
        out.(sprintf('vs_S_%s_%s_hist', group_label{gi}, tasks{ti})) = {nx, ny};
        out.(sprintf('vs_S_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = {c, c};
        
    end
end


%% distribution of vergences including all drifts
cs = params.drift.vergence_pos;
for gi = 1:length(groupfunc)
    g = groupfunc{gi};
    c = cs{gi};

    for ti = 1:length(tasks)
        vx = cellfun(@(x, y, z) ...
            g(...
                x(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))), ... % vergence data
                y.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))) & z.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti})))), ... % isdrift data
            allVergX, pptrials(useTrials,2), pptrials(useTrials, 1),...
            'UniformOutput', false);
        vy = cellfun(@(x, y, z) ...
            g(...
                x(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))), ... % vergence data
                y.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti}))) & z.isdrift(round(z.(startTimes{ti})):round(z.(stopTimes{ti})))), ... % isdrift data
            allVergY, pptrials(useTrials,2), pptrials(useTrials, 1),...
            'UniformOutput', false);
        
        vx = cat(2, vx{:});
        vy = cat(2, vy{:});
        
        nx = hist(vx, c);
        ny = hist(vy, c);
        nx = nx / sum(nx) ;
        ny = ny / sum(ny);
        
        mx = nanmean(vx);
        sdx = nanstd(vx);
        my = nanmean(vy);
        sdy = nanstd(vy);
        
        out.(sprintf('vs_V_%s_%s_data', group_label{gi}, tasks{ti})) = {vx, vy};
        out.(sprintf('vs_V_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mx, sdx / sqrt(length(vx))],...
            [my, sdy / sqrt(length(vy))]};
        out.(sprintf('vs_V_%s_%s_hist', group_label{gi}, tasks{ti})) = {nx, ny};
        out.(sprintf('vs_V_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = {c, c};
        
    end
end

%% drift version vs vergence
useTrials = ~counts.exclude;
vel3_save = cell(length(groupfunc), length(tasks));

for gi = 1:length(groupfunc)
    
    g = groupfunc{gi};
    
    c = linspace(params.drift.vergence_vel_lims(1), ...
                 params.drift.vergence_vel_lims(2),...
                 params.nbins(gi));

    for ti = 1:length(tasks)
        velx = cellfun(@(x, y) g(...
            x.vs_velocities(round(x.(startTimes{ti})):round(x.(stopTimes{ti})), 1)',...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        vely = cellfun(@(x, y) g(...
            x.vs_velocities(round(x.(startTimes{ti})):round(x.(stopTimes{ti})), 2)',...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        velvx = cellfun(@(x, y) g(...
            x.vs_velocities(round(x.(startTimes{ti})):round(x.(stopTimes{ti})), 3)',...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        velvy = cellfun(@(x, y) g(...
            x.vs_velocities(round(x.(startTimes{ti})):round(x.(stopTimes{ti})), 4)',...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        
        velx = cat(2, velx{:}) * 1000; % arcmin / s
        vely = cat(2, vely{:}) * 1000; % arcmin / s
        velvx = cat(2, velvx{:}) * 1000; % arcmin / s
        velvy = cat(2, velvy{:}) * 1000; % arcmin / s
        
        
        kill = velx > 300 | velx > 300 | velvx > 300 | velvy > 300 |...
            isnan(velx) | isnan(velx) | isnan(velvx) | isnan(velvy);
        velx(kill) = [];
        vely(kill) = [];
        velvx(kill) = [];
        velvy(kill) = [];
        
        vel3_save{gi, ti} = [velx(:), vely(:), velvx(:), velvy(:)];
        
        [rrx, ppx] = corrcoef(velx, velvx);
        [rry, ppy] = corrcoef(vely, velvy);

        
        % horizontal computations
        [n2, xedges, yedges] = histcounts2(velx, velvx, c, c);
        
        out.(sprintf('vs_velx_%s_%s_data', group_label{gi}, tasks{ti})) = {velx, velvx};
        out.(sprintf('vs_velx_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mean(velx), std(velx) / sqrt(length(velx))],...
            [mean(velvx), std(velvx) / sqrt(length(velvx))]};
        out.(sprintf('vs_velx_%s_%s_hist', group_label{gi}, tasks{ti})) = n2;
        out.(sprintf('vs_velx_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = {xedges, yedges};
        out.(sprintf('vs_velx_%s_%s_rho', group_label{gi}, tasks{ti})) = [rrx(1, 2), ppx(1, 2)];
        
        % vertical computations
        [n3, xedges, yedges] = histcounts2(vely, velvy, c, c);
        
        out.(sprintf('vs_vely_%s_%s_data', group_label{gi}, tasks{ti})) = {vely, velvy};
        out.(sprintf('vs_vely_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mean(vely), std(vely) / sqrt(length(vely))],...
            [mean(velvy), std(velvy) / sqrt(length(velvy))]};
        out.(sprintf('vs_vely_%s_%s_hist', group_label{gi}, tasks{ti})) = n3;
        out.(sprintf('vs_vely_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = {xedges, yedges};
        out.(sprintf('vs_vely_%s_%s_rho', group_label{gi}, tasks{ti})) = [rry(1, 2), ppy(1, 2)];
    end
end

%% plotting
if DOPLOT
    plotDriftInfo_3d(out, params, subjname, imgPath);
end

end


function x = nanBuffer(x, buffer)
if ~exist('buffer', 'var')
    buffer = 20;
end

bin = ~isnan(x);

[startIndices, stopIndices] = getIndicesFromBin(bin);

for i = 1:length(startIndices)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        x(ss:ee) = nan;
        continue;
    end
    
    x(ss:ss+buffer) = nan;
    x(ee-buffer:ee) = nan;
end

end

function x = trimSegment(x, bin, buffer)
if ~exist('buffer', 'var')
    buffer = 20;
end

[startIndices, stopIndices] = getIndicesFromBin(bin);

m = cell(size(startIndices))';

for i = 1:length(m)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        continue;
    end
    
    m{i} = x(ss+buffer:ee-buffer);
end

x = cat(2, m{:});

end

function m = meanBySegment(x, bin, buffer)

[startIndices, stopIndices] = getIndicesFromBin(bin);
m = nan(size(startIndices))';

for i = 1:length(m)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        continue;
    end
    
    m(i) = mean(x(ss+buffer:ee-buffer));
end
end

function x = subtractMeanFromSegment(x, bin, buffer)
if ~exist('buffer', 'var')
    buffer = 20; 
end

[startIndices, stopIndices] = getIndicesFromBin(bin);

for i = 1:length(startIndices)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        x(ss:ee) = nan;
        continue;
    end
    
    x(ss:ss+buffer) = nan;
    x(ee-buffer:ee) = nan;
    
    x(ss:ee) = x(ss:ee) - nanmean(x(ss+buffer:ee-buffer));
end
end

function x = subtractStartFromSegment(x, bin, buffer)
if ~exist('buffer', 'var')
    buffer = 20; 
end

[startIndices, stopIndices] = getIndicesFromBin(bin);

for i = 1:length(startIndices)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        x(ss:ee) = nan;
        continue;
    end
    
    x(ss:ss+buffer) = nan;
    x(ee-buffer:ee) = nan;
    
    x(ss:ee) = x(ss:ee) - x(ss + buffer + 1);
end
end