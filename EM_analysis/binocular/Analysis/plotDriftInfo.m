function plotDriftInfo(out, params, subjname, imgPath)
if length(subjname) > 4
    subjname_print = subjname(1:4);
else
    subjname_print = subjname; 
end

% out is the output of analyzeDrifts.m
if ~exist('imgPath', 'var')
    imgPath = ''; 
end

tasks = params.tasks;
plottasks = params.plottasks + 1;
cols = params.taskColors;

fh = 200;

FONTSIZE = 20;
LFONTSIZE = 16;

eye_labels = {'right eye', 'left eye'};

% note that the plots made here are based only on instantaneous
glabel = 'instantaneous';

%% drift duration - histograms
fh = fh + 1;
figure(fh); clf; hold on;
h = nan(length(plottasks), 1);
cnt = 0;
for ti = plottasks
    cnt = cnt + 1;
    tmpx = out.(sprintf('duration_%s_hist_bins', tasks{ti}));
    tmpy = out.(sprintf('duration_%s_hist', tasks{ti}));
    
    h(cnt) = plot(tmpx(1:end-1),...
        tmpy(1:end-1),...
        '-', 'linewidth', 2, 'Color', cols{ti});
    
end
for ti = plottasks
    tmp = out.(sprintf('duration_%s_MS', tasks{ti}));
    vertLineThrough(tmp(1), cols{ti});
end
standardPlot;
xlim(tmpx([1, end]));
xlabel('drift duration (ms)');
hlegs = legend(h, tasks(plottasks), 'Location', 'northeast');
set(hlegs, 'FontSize', LFONTSIZE, 'FontName', 'arial');

mysavefig('duration');

%% relative drift positions (2d histograms)
for ti = plottasks
    tmpc = out.(sprintf('pos_%s_hist_bins', tasks{ti}));
    tmp = out.(sprintf('pos_%s_hist', tasks{ti}));
    for eye = 1:2
       fh = fh + 1;
       figure(fh); clf;
       imagesc(tmpc, tmpc, tmp{eye}');
       standardPlot;
       title(sprintf('%s: %s', tasks{ti}, eye_labels{eye}));
       colormap(flipud(colormap('hot')));
       axis image;
       xlabel('horizontal (arcmin)');
       ylabel('vertical (arcmin)');
       mysavefig(sprintf('%s_driftpos_eye%i', tasks{ti}, eye));
    end
end

%% drift change in vergence (1d histograms)
nms = {'x', 'y'};
for di = 1:2 % x and y
    fh = fh + 1;
    figure(fh); clf; hold on;
    h = nan(length(plottasks), 1);
    for ti = plottasks
        tmpx = out.(sprintf('dV_%s_hist_bins', tasks{ti}));
        tmpy = out.(sprintf('dV_%s_hist', tasks{ti}));
        h(ti) = plot(tmpx{di}(1:end-1), tmpy{di}(1:end-1), '-', 'linewidth', 2, 'Color', cols{ti});
    end
    for ti = plottasks
        tmp = out.(sprintf('dV_%s_MS', tasks{ti}));
        vertLineThrough(tmp{di}(1), cols{ti});
    end
    xlabel('vergence change (arcmin)');
    hlegs = legend(h, tasks(plottasks), 'Location', 'northeast');
    set(hlegs, 'FontSize', LFONTSIZE, 'FontName', 'arial');
    standardPlot;
    mysavefig(sprintf('dV%s', nms{di}));
end

%% drift speeds (1d histogram)
% for each eye, compare histograms of each task
for eye = 1:2
    fh = fh + 1;
    figure(fh); clf; hold on;
    h = nan(length(plottasks), 1);
    cnt = 0;
    for ti = plottasks
        cnt = cnt + 1;
        tmpx = out.(sprintf('speed_%s_%s_hist_bins', glabel, tasks{ti}));
        tmpy = out.(sprintf('speed_%s_%s_hist', glabel, tasks{ti}));
        h(cnt) = plot(tmpx(1:end-1), tmpy{eye}(1:end-1), '-', 'linewidth', 2, 'Color', cols{ti});
    end
    for ti = plottasks
        tmp = out.(sprintf('speed_%s_%s_MS', glabel, tasks{ti}));
        vertLineThrough(tmp{eye}(1), cols{ti});
    end
    hlegs = legend(h, tasks(plottasks), 'Location', 'northeast');
    set(hlegs, 'FontSize', LFONTSIZE, 'FontName', 'arial');
    xlabel('speed (arcmin/s)');
    standardPlot;
    mysavefig(sprintf('speed_eye%i', eye));
end

%% correlation of drift speed
% for each task
for ti = plottasks
    dd = out.(sprintf('speed_%s_%s_data', glabel, tasks{ti}));
    re = dd{1}; le = dd{2};
    
    [n2, xedges, yedges] = histcounts2(re, le, 100);
    [yy, xx] = meshgrid(yedges(1:end-1) + mean(diff(yedges))/2,...
        xedges(1:end-1) + mean(diff(xedges))/2);
    
    tmp = out.(sprintf('speed_%s_%s_rho', glabel, tasks{ti}));
    rr = tmp(1); pp = tmp(2);
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    
    scatter(re, le, ...
        1, 'b', 'filled', 'MarkerFaceAlpha', .3);
    contour(xx, yy, n2 / max(n2(:)), 5); colormap('hot');
    text(double(quantile(re, .95)), double(quantile(le, .95)),...
        sprintf('r = %1.2f', rr), 'FontSize', 14);
    text(double(quantile(re, .95)), double(quantile(le, .85)),...
        sprintf('p = %1.2f', pp), 'FontSize', 14);
    axis image;
    xlabel({sprintf('RIGHT speed'), 'arcmin/s'});
    ylabel('LEFT');
    standardPlot;
    title(tasks{ti});
    
    mysavefig(sprintf('speed_corr_%s', tasks{ti}));
end

%% correlation of drift velocities
flds = {'velx', 'vely'};
for fi = 1:length(flds) % horizontal and vertical
    fld = flds{fi};
    for ti = plottasks
        tmpbins = out.(sprintf('%s_%s_%s_hist_bins', fld, glabel, tasks{ti}));
        [yy2, xx2] = meshgrid(tmpbins{2}(1:end-1) + mean(diff(tmpbins{2}))/2,...
            tmpbins{1}(1:end-1) + mean(diff(tmpbins{1}))/2);
        
        tmpn = out.(sprintf('%s_%s_%s_hist', fld, glabel, tasks{ti}));
        
        tmp = out.(sprintf('%s_%s_%s_data', fld, glabel, tasks{ti}));
        tmpr = out.(sprintf('vely_%s_%s_rho', glabel, tasks{ti}));
        rrx = tmpr(1); ppx = tmpr(2);
        
        fh = fh + 1;
        figure(fh); clf; hold on;
        scatter(tmp{1}, tmp{2}, 1, 'b', 'filled', 'MarkerFaceAlpha', .3);
        contour(xx2, yy2, tmpn / max(tmpn(:)), 5); colormap('hot');
        title(sprintf('%s: %s', tasks{ti}, fld));
        axis image;
        
        xlabel({'right eye vel', 'arcmin/s'});
        ylabel('left eye vel');

        set(gca, 'FontSize', 16, 'FontName', 'arial');
        text(double(quantile(tmp{1}, .95)), double(quantile(tmp{2}, .95)),...
            sprintf('r = %1.2f', rrx), 'FontSize', 14);
        text(double(quantile(tmp{1}, .95)), double(quantile(tmp{2}, .75)),...
            sprintf('p = %1.2f', ppx), 'FontSize', 14);
        xlim([-250, 250]); ylim([-250, 250]);
        standardPlot;
        mysavefig(sprintf('%s_corr_%s', fld, tasks{ti}));
    end
end

%% drift curvature (1d histogram)
% for each eye, compare histograms of each task
for eye = 1:2
    fh = fh + 1;
    figure(fh); clf; hold on;
    h = nan(length(plottasks), 1);
    cnt = 0;
    for ti = plottasks
        cnt = cnt + 1;
        tmpx = out.(sprintf('curv_%s_%s_hist_bins', glabel, tasks{ti}));
        tmpy = out.(sprintf('curv_%s_%s_hist', glabel, tasks{ti}));
        h(cnt) = plot(tmpx(1:end-1), tmpy{eye}(1:end-1), '-', 'linewidth', 2, 'Color', cols{ti});
    end
    for ti = plottasks
        tmp = out.(sprintf('curv_%s_%s_MS', glabel, tasks{ti}));
        vertLineThrough(tmp{eye}(1), cols{ti});
    end
    hlegs = legend(h, tasks(plottasks), 'Location', 'northeast');
    set(hlegs, 'FontSize', LFONTSIZE, 'FontName', 'arial');
    xlabel('curvature (1/arcmin)');
    standardPlot;
    mysavefig(sprintf('curvature_eye%i', eye));
end

%% skipping curvature correlation for now


%% nested function for saving figures
    function mysavefig(figname)
        annotation('textbox',[.02, .9, .1, .1], 'String', subjname_print,...
            'FitBoxToText', 'on', 'FontSize', FONTSIZE, 'FontName', 'arial',...
            'FontWeight', 'bold');
        
        if ~isempty(imgPath)
            fname = fullfile(imgPath, sprintf('%s_%s', subjname_print, figname));
            print([fname, '.png'], '-dpng');
            print([fname, '.eps'], '-depsc');
        end
    end

end

