function [r, p] = plotHistOfDriftDir(out, winsize, subjname, DOPLOT, imgPath)
tasks = {'Fixation', 'Snellen'};

lim = 100 * 30 / winsize + 20;

edges = linspace(-lim, lim, 50);
bins = edges(1:end-1) + mean(diff(edges)) / 2;

r = nan(size(tasks));
p = nan(size(tasks));

fh = 400;
figure(fh); clf;
for ti = 1:length(tasks)
    velx = out.(sprintf('velx_instantaneous_%s_data', tasks{ti}));
    
    vel1x = velx{1};
    vel2x = velx{2};
    
    verg = (vel1x - vel2x) / 2;
    vers = (vel1x + vel2x) / 2;
    
    [rr, pp] = corrcoef(verg, vers);
    r(ti) = rr(1, 2);
    p(ti) = pp(1, 2);
    
    n = histcounts2(verg, vers, edges, edges);
    n = n / sum(n(:));
    
    [xx, yy] = meshgrid(bins, bins);
    
    subplot(1, 2, ti); hold on;
    pcolor(bins, bins, n);
    colormap(flipud(colormap('hot')));
    shading interp;
    axis image;
    ax1 = gca;
    title(tasks{ti});
    
    ylabel('vergence velocity');
    xlabel('version velocity');
    
    text(bins(1) + .65 * diff(bins([1, end])), ...
        bins(1) + .9 * diff(bins([1, end])),...
        sprintf('r = %1.2f', rr(1, 2)), 'FontSize', 14);
    text(bins(1) + .65 * diff(bins([1, end])), ...
        bins(1) + .8 * diff(bins([1, end])),...
        sprintf('p = %1.2f', pp(1, 2)), 'FontSize', 14);
    
    ax2 = axes('Position', get(ax1, 'Position'));
    contour(xx, yy, n / max(n(:)), 5); colormap(ax2, 'winter');
    ax2.Visible = 'off';
    ax2.XTick = [];
    ax2.YTick = [];
    
    linkaxes([ax1, ax2], 'xy');
    
    axis image;
    
    annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
        'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
        'FontWeight', 'bold');
    
    annotation('textbox',[.52, .9, .1, .1], 'String', sprintf('winsize = %ims', winsize),...
        'FitBoxToText', 'on', 'FontSize', 14, 'FontName', 'arial',...
        'FontWeight', 'bold');
    
    if DOPLOT
        fname = fullfile(imgPath, sprintf('%s_drift_vel_vv_winsize%i', subjname, winsize));
        print([fname, '.png'], '-dpng');
        print([fname, '.eps'], '-depsc');
    end
end
end