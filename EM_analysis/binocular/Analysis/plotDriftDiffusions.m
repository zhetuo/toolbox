function plotDriftDiffusions(out, params, subjname, imgPath)
if length(subjname) > 4
    subjname_print = subjname(1:4);
else
    subjname_print = subjname; 
end

tasks = params.tasks;

Dsq = out.Dsq;
diffCon = out.diffCon;
diffConBoot = out.diffConBoot;

fh = 789;

FONTSIZE = 20;
LFONTSIZE = 16;
SCATTERDOTSIZE = 15;

typelabels = {'right', 'left', 'version', 'vergence'};
typecols = [.5 .5 .5; .5 .5 .5; 0 0 0; 1 0 0];
typesyms = 's^oo';

%% plot Dsq for each task and X, Y, Vx, Vy
for ti = 1:length(params.plottasks)
    for cc = 1:size(Dsq, 2)
        dsq = Dsq{ti, cc, 1};
        dsq_ss = Dsq{ti, cc, 2};
        if ~isempty(dsq_ss)
            fh = fh + 1;
            figure(fh); clf; hold on;
            plot(1:length(dsq), dsq_ss);
            plot(1:length(dsq), dsq, 'k-', 'linewidth', 2);
            xlim([1, length(dsq)]);
            xlabel('time lag (ms)', 'FontSize', FONTSIZE);
            ylabel({'variance of displacement', '(arcmin^2/s)'}, 'FontSize', FONTSIZE);
            title(sprintf('%s: %s', params.tasks{ti}, typelabels{cc}), 'FontSize', FONTSIZE);
            
            mysavefig(sprintf('dsq-%s-%s', params.tasks{ti}, typelabels{cc}));
        end
    end
end

%% plot diffusion constants
mdc = mean(diffConBoot, 3);
sdc = std(diffConBoot, [], 3);

fh = fh + 1;
figure(fh); clf; hold on;
for cc = 1:size(Dsq, 2)
    errorbar(params.plottasks, mdc(:, cc), sdc(:, cc),...
        [typesyms(cc), '-'], 'Color', typecols(cc, :), ...
        'linewidth', 2, 'markersize', 10, 'capsize', 14);
end
set(gca, 'XTick', params.plottasks, 'XTickLabels', params.tasks);
ylabel('diffusion constant (arcmin^2/s)', 'FontSize', FONTSIZE);
legend(typelabels, 'Location', 'northeast', 'FontSize', LFONTSIZE);
xlim([params.plottasks(1)-.25, params.plottasks(end)+.25]);
mysavefig('diffusionconstant');

%% nested function for saving figures
    function mysavefig(figname)
        annotation('textbox',[.02, .9, .1, .1], 'String', subjname_print,...
            'FitBoxToText', 'on', 'FontSize', FONTSIZE, 'FontName', 'arial',...
            'FontWeight', 'bold');
        
        if ~isempty(imgPath)
            fname = fullfile(imgPath, sprintf('%s_%s', subjname_print, figname));
            print([fname, '.png'], '-dpng');
            print([fname, '.eps'], '-depsc');
        end
    end
end