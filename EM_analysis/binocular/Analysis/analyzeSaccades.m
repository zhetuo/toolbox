function out = analyzeSaccades(pptrials, counts, params, DOPLOT, imgPath)
% note that doplot here only applies to monocular microsaccades which could
% take a while
if ~exist('imgPath', 'var') || isempty(imgPath)
    DOSAVEPLOT = false;
else
    DOSAVEPLOT = true;
end
subj = pptrials{1}.Subject_Name;

run('saccadeMatrixIndex.m');
out = struct();
%%
tasks = params.tasks; % generalize params.tasks;
plottasks = params.plottasks;

ntasks = length(tasks);


%% concatenate all saccade data from all trials
saccades = cellfun(@(x) x.saccadePairs, pptrials(~counts.exclude  & ~counts.lowperformance, 1),...
    'UniformOutput', false);
msaccades = cellfun(@(x) x.saccadeMon, pptrials(~counts.exclude  & ~counts.lowperformance, 1),...
    'UniformOutput', false);
saccades = cat(1, saccades{:});
msaccades = cat(1, msaccades{:});
saccades = [saccades; msaccades];

isMonocular = isnan(saccades(:, EYE1_START)) | isnan(saccades(:, EYE2_START));

%% count number of saccades
fprintf('SACCADE COUNTS (all) \n');
for ti = 0:(ntasks-1)
    thistask = saccades(:, TASK_INDEX) == ti;
    ntotal = sum(thistask);
    nmono = sum(isMonocular(:) & thistask(:));
    fprintf('\t%s - %i - monocular %i\n', tasks{ti+1}, ntotal, nmono);
end
ntotal = length(isMonocular);
nmono = sum(isMonocular(:));
fprintf('\ttotal - %i - monocular %i\n', ntotal, nmono);


%% amplitude histograms
c = params.saccade.amp;
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    m1 = nanmean(theseSaccades(:, EYE1_AMP));
    m2 = nanmean(theseSaccades(:, EYE2_AMP));
    s1 = nanstd(theseSaccades(:, EYE1_AMP)) / sqrt(sum(~isnan(theseSaccades(:, EYE1_AMP))));
    s2 = nanstd(theseSaccades(:, EYE2_AMP)) / sqrt(sum(~isnan(theseSaccades(:, EYE2_AMP))));
    
    n1 = histcounts(theseSaccades(:, EYE1_AMP), c);
    n1 = n1 / sum(n1);
    n2 = histcounts(theseSaccades(:, EYE2_AMP), c);
    n2 = n2 / sum(n2);
    
    out.(sprintf('amp_%s_data', tasks{ti+1})) = {theseSaccades(:, EYE1_AMP), theseSaccades(:, EYE2_AMP)};
    out.(sprintf('amp_%s_hist', tasks{ti+1})) = {n1, n2};
    out.(sprintf('amp_%s_hist_bins', tasks{ti+1})) = {c, c};
    out.(sprintf('amp_%s_MS', tasks{ti+1})) = ...
        {[m1, s1], [m2, s2]};
    
end

%% direction histograms
c = params.saccade.dir;
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    n1 = histcounts(theseSaccades(:, EYE1_DIR), c);
    n1 = n1 / sum(n1);
    n2 = histcounts(theseSaccades(:, EYE2_DIR), c);
    n2 = n2 / sum(n2);
    
    out.(sprintf('dir_%s_data', tasks{ti+1})) = {theseSaccades(:, EYE1_DIR), theseSaccades(:, EYE2_DIR)};
    out.(sprintf('dir_%s_hist', tasks{ti+1})) = {n1, n2};
    out.(sprintf('dir_%s_hist_bins', tasks{ti+1})) = {c, c};
end

%% peak velocity histograms
c = params.saccade.pkvel;
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    m1 = nanmean(theseSaccades(:, EYE1_PKVEL)) / 60;
    m2 = nanmean(theseSaccades(:, EYE2_PKVEL))/ 60;
    s1 = nanstd(theseSaccades(:, EYE1_PKVEL)) / 60 / sqrt(sum(~isnan(theseSaccades(:, EYE1_PKVEL))));
    s2 = nanstd(theseSaccades(:, EYE2_PKVEL)) / 60 / sqrt(sum(~isnan(theseSaccades(:, EYE2_PKVEL))));
    
    n1 = histcounts(theseSaccades(:, EYE1_PKVEL) / 60, c);
    n1 = n1 / sum(n1);
    n2 = histcounts(theseSaccades(:, EYE2_PKVEL) / 60, c);
    n2 = n2 / sum(n2);
    
    out.(sprintf('pkvel_%s_data', tasks{ti+1})) = {theseSaccades(:, EYE1_PKVEL), theseSaccades(:, EYE2_PKVEL)};
    out.(sprintf('pkvel_%s_hist', tasks{ti+1})) = {n1, n2};
    out.(sprintf('pkvel_%s_hist_bins', tasks{ti+1})) = {c, c};
    out.(sprintf('pkvel_%s_MS', tasks{ti+1})) = ...
        {[m1, s1], [m2, s2]};
    
end

%% left vs right: amplitude
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    amp1 = theseSaccades(:, EYE1_AMP);
    amp2 = theseSaccades(:, EYE2_AMP);
    
    use = ~isMonocular(thisTask);
    
    if sum(use) < 5
        continue; 
    end
    
    [rr, pp] = corrcoef(amp2(use), amp1(use));
    
    out.(sprintf('amp_%s_rho_data', tasks{ti+1})) = {amp1(use), amp2(use)};
    out.(sprintf('amp_%s_rho', tasks{ti+1})) = [rr(1, 2), pp(1, 2)];
    
end

%% left vs right: amplitude (horizontal only)
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    amp1 = theseSaccades(:, EYE1_AMP) .* cos(theseSaccades(:, EYE1_DIR));
    amp2 = theseSaccades(:, EYE2_AMP) .* cos(theseSaccades(:, EYE2_DIR));
    
    use = ~isMonocular(thisTask);
    
    if sum(use) < 5
        continue; 
    end
    
    [rr, pp] = corrcoef(amp2(use), amp1(use));
    
    out.(sprintf('amp_x_%s_rho_data', tasks{ti+1})) = {amp1(use), amp2(use)};
    out.(sprintf('amp_x_%s_rho', tasks{ti+1})) = [rr(1, 2), pp(1, 2)];
end

%% left vs right: direction
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    tmp1 = theseSaccades(:, EYE1_DIR);
    tmp2 = theseSaccades(:, EYE2_DIR);
    
    d = tmp1 - tmp2 > pi;
    tmp2(d) = tmp2(d) + 2*pi;
    d = tmp2 - tmp1 > pi;
    tmp1(d) = tmp1(d) + 2*pi;
    
    tmp1 = tmp1 * 180 / pi;
    tmp2 = tmp2 * 180 / pi;
    
    
    use = ~isMonocular(thisTask);
    
    if sum(use) < 5
        continue; 
    end
    [rr, pp] = corrcoef(tmp2(use), tmp1(use));
    
    out.(sprintf('dir_%s_rho_data', tasks{ti+1})) = {tmp1(use), tmp2(use)};
    out.(sprintf('dir_%s_rho', tasks{ti+1})) = [rr(1, 2), pp(1, 2)];

end

%% left vs right: velocity
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    vel1 = theseSaccades(:, EYE1_PKVEL)/60;
    vel2 = theseSaccades(:, EYE2_PKVEL)/60;
    
    use = ~isMonocular(thisTask);
    if sum(use) < 5
        continue; 
    end
    
    [rr, pp] = corrcoef(vel2(use), vel1(use));
    
    out.(sprintf('pkvel_%s_rho_data', tasks{ti+1})) = {vel1(use), vel2(use)};
    out.(sprintf('pkvel_%s_rho', tasks{ti+1})) = [rr(1, 2), pp(1, 2)];
end

%% amplitude difference
c = params.saccade.amp_diff;
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    dv = (theseSaccades(:, EYE1_AMP) - ...
        theseSaccades(:, EYE2_AMP)) / 2;
    
    mdv = nanmean(dv);
    sdv = nanstd(dv) / sqrt(sum(~isnan(dv)));
    
    n = histcounts(dv, c);
    n = n / sum(n);
    
    out.(sprintf('amp_diff_%s_data', tasks{ti+1})) = {dv};
    out.(sprintf('amp_diff_%s_hist', tasks{ti+1})) = {n};
    out.(sprintf('amp_diff_%s_hist_bins', tasks{ti+1})) = {c};
    out.(sprintf('amp_diff_%s_MS', tasks{ti+1})) = ...
        {[mdv, sdv]};
end

%% amplitude difference (horz and vert) - change in vergence
c = params.saccade.V_amp;

for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    % vergence components (doesn't really apply to Y
    dx = (theseSaccades(:, EYE1_AMP) .* cos(theseSaccades(:, EYE1_DIR)) - ...
        theseSaccades(:, EYE2_AMP) .* cos(theseSaccades(:, EYE2_DIR))) / 2;
    dy = (theseSaccades(:, EYE1_AMP) .* sin(theseSaccades(:, EYE1_DIR)) - ...
        theseSaccades(:, EYE2_AMP) .* sin(theseSaccades(:, EYE2_DIR))) / 2;
    
    nx = histcounts(dx, c);
    ny = histcounts(dy, c);
    
    nx = nx / sum(nx);
    ny = ny / sum(ny);
    
    mx = nanmean(dx);
    sx = nanstd(dx) / sqrt(sum(~isnan(dx)));
    my = nanmean(dy);
    sy = nanstd(dy) / sqrt(sum(~isnan(dy)));
    
    out.(sprintf('V_amp_%s_data', tasks{ti+1})) = {dx, dy};
    out.(sprintf('V_amp_%s_hist', tasks{ti+1})) = {nx, ny};
    out.(sprintf('V_amp_%s_hist_bins', tasks{ti+1})) = {c, c};
    out.(sprintf('V_amp_%s_MS', tasks{ti+1})) = ...
        {[mx, sx], [my, sy]};
end

%% velocity difference
c = params.saccade.pkvel_diff;
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    dv = (theseSaccades(:, EYE1_PKVEL) - ...
        theseSaccades(:, EYE2_PKVEL)) / 60 / 2;
    
    mdv = nanmean(dv);
    sdv = nanstd(dv) / sqrt(sum(~isnan(dv)));
    
    n = histcounts(dv, c);
    n = n / sum(n);
    
    out.(sprintf('pkvel_diff_%s_data', tasks{ti+1})) = {dv};
    out.(sprintf('pkvel_diff_%s_hist', tasks{ti+1})) = {n};
    out.(sprintf('pkvel_diff_%s_hist_bins', tasks{ti+1})) = {c};
    out.(sprintf('pkvel_diff_%s_MS', tasks{ti+1})) = ...
        {[mdv, sdv]};
end

%% version components of EM
c = params.saccade.S_ampxy;
c2 = params.saccade.S_amp;
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    % version components 
    dx = (theseSaccades(:, EYE1_AMP) .* cos(theseSaccades(:, EYE1_DIR)) + ...
        theseSaccades(:, EYE2_AMP) .* cos(theseSaccades(:, EYE2_DIR)))/2;
    dy = (theseSaccades(:, EYE1_AMP) .* sin(theseSaccades(:, EYE1_DIR)) + ...
        theseSaccades(:, EYE2_AMP) .* sin(theseSaccades(:, EYE2_DIR)))/2;
    
    amp = sqrt(dx.^2 + dy.^2);
    
    mdx = nanmean(dx);
    mdy = nanmean(dy);
    sdx = nanstd(dx)/ sqrt(sum(~isnan(dx)));
    sdy = nanstd(dy) / sqrt(sum(~isnan(dy)));
    mamp = nanmean(amp);
    samp = nanstd(amp) / sqrt(sum(~isnan(amp)));
    
    nx = histcounts(dx, c);
    ny = histcounts(dy, c);
    na = histcounts(amp, c2);
    nx = nx / sum(nx);
    ny = ny / sum(ny);
    na = na / sum(na);
    
    out.(sprintf('S_ampxy_%s_data', tasks{ti+1})) = {dx, dy};
    out.(sprintf('S_ampxy_%s_hist', tasks{ti+1})) = {nx, ny};
    out.(sprintf('S_ampxy_%s_hist_bins', tasks{ti+1})) = {c, c};
    out.(sprintf('S_ampxy_%s_MS', tasks{ti+1})) = ...
        {[mdx, sdx], [mdy, sdy]};
    
    out.(sprintf('S_amp_%s_data', tasks{ti+1})) = {amp};
    out.(sprintf('S_amp_%s_hist', tasks{ti+1})) = {na};
    out.(sprintf('S_amp_%s_hist_bins', tasks{ti+1})) = {c2};
    out.(sprintf('S_amp_%s_MS', tasks{ti+1})) = ...
        {[mamp, samp]};
end

%% Vergence vs Version components of saccades
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    % vergence components
    dx = (theseSaccades(:, EYE1_AMP) .* cos(theseSaccades(:, EYE1_DIR)) - ...
        theseSaccades(:, EYE2_AMP) .* cos(theseSaccades(:, EYE2_DIR)))/2;
    dy = (theseSaccades(:, EYE1_AMP) .* sin(theseSaccades(:, EYE1_DIR)) - ...
        theseSaccades(:, EYE2_AMP) .* sin(theseSaccades(:, EYE2_DIR)))/2;
    
    sx = (theseSaccades(:, EYE1_AMP) .* cos(theseSaccades(:, EYE1_DIR)) + ...
        theseSaccades(:, EYE2_AMP) .* cos(theseSaccades(:, EYE2_DIR)))/2;
    sy = (theseSaccades(:, EYE1_AMP) .* sin(theseSaccades(:, EYE1_DIR)) + ...
        theseSaccades(:, EYE2_AMP) .* sin(theseSaccades(:, EYE2_DIR)))/2;
    
    use = ~isMonocular(thisTask);
    
    if sum(use) < 5
        continue; 
    end
    
    [rrx, ppx] = corrcoef(dx(use), sx(use));
    [rry, ppy] = corrcoef(dy(use), sy(use));
    
    out.(sprintf('VSx_%s_rho_data', tasks{ti+1})) = {sx(use), dx(use)};
    out.(sprintf('VSx_%s_rho', tasks{ti+1})) = [rrx(1, 2), ppx(1, 2)];
    
    out.(sprintf('VSy_%s_rho_data', tasks{ti+1})) = {sy(use), dy(use)};
    out.(sprintf('VSy_%s_rho', tasks{ti+1})) = [rry(1, 2), ppy(1, 2)];
end

%% plotting
if DOPLOT
    subjname = pptrials{1, 1}.Subject_Name;
    plotSaccadeInfo(out, params, subjname, imgPath); 
end

%% monocular microsaccades (plot all potential candidates)
if false && DOPLOT
    for ii = 1:size(saccades, 1)
        if saccades(ii, SACC_BIN) == 0 % binocular saccade
            continue;
        end
        
        ss1 = saccades(ii, EYE1_START);
        ee1 = saccades(ii, EYE1_STOP);
        ss2 = saccades(ii, EYE2_START);
        ee2 = saccades(ii, EYE2_STOP);
        
        tt = saccades(ii, TRIAL_INDEX);
        
        ss = max(1, min(ss1, ss2) - 30);
        ee = min(max(ee1, ee2) + 30, pptrials{tt, 1}.samples);
        
        x1 = pptrials{tt, 1}.x.orig(ss:ee);
        y1 = pptrials{tt, 1}.y.orig(ss:ee);
        x2 = pptrials{tt, 2}.x.orig(ss:ee);
        y2 = pptrials{tt, 2}.y.orig(ss:ee);
        v1 = pptrials{tt, 1}.velocity(ss:ee) / 60;
        v2 = pptrials{tt, 2}.velocity(ss:ee) / 60;
        
        if saccades(ii, SACC_BIN) == 1
            ltype = '-';
        else
            ltype = '--';
        end
        ax(1) = subplot(3, 1, 1); hold on;
        h1 = plot(ss:ee, x1 - mean(x1), 'b', 'DisplayName', 'right-x');
        h2 = plot(ss:ee, y1 - mean(y1), 'r', 'DisplayName', 'right-y');
        ylabel('(arcmin)');
        plot([ss1, ss1], [-10, 10], 'k', 'linestyle', ltype);
        plot([ee1, ee1], [-10, 10], 'k', 'linestyle', ltype);
        text(ss1, 5, sprintf('amp = %1.2f arcmin', saccades(ii, EYE1_AMP)), 'HorizontalAlignment', 'right');
        legend([h1, h2], 'Location', 'southeast');
        xlim([ss, ee]);
        ylim([-10, 10]);
        
        if saccades(ii, SACC_BIN) == 2
            ltype = '-';
        else
            ltype = '--';
        end
        ax(2) = subplot(3, 1, 2); hold on;
        h1 = plot(ss:ee, x2 - mean(x2), 'b', 'DisplayName', 'left-x');
        h2 = plot(ss:ee, y2 - mean(y2), 'r', 'DisplayName', 'left-y');
        ylabel('(arcmin)');
        plot([ss2, ss2], [-10, 10], 'k', 'linestyle', ltype);
        plot([ee2, ee2], [-10, 10], 'k', 'linestyle', ltype);
        text(ss2, 5, sprintf('amp = %1.2f arcmin', saccades(ii, EYE2_AMP)), 'HorizontalAlignment', 'right');
        legend([h1, h2], 'Location', 'southeast');
        xlim([ss, ee]);
        ylim([-10, 10]);
        
        ax(3) = subplot(3, 1, 3); hold on;
        h1 = plot(ss:ee, v1, 'g', 'DisplayName', 'right');
        h2 = plot(ss:ee, v2, 'm', 'DisplayName', 'left');
        plot([ss, ee], [3, 3], 'k--');
        legend([h1, h2], 'Location', 'southeast');
        ylabel('velocity (deg/s)');
        xlabel('time (ms)');
        xlim([ss, ee]);
        
        linkaxes(ax(1:2), 'y');
        linkaxes(ax, 'x');
        
        annotation('textbox',[.4, .9, .1, .1], 'String','Monocular Microsaccade','FitBoxToText','on',...
            'FontSize', 16, 'FontWeight', 'bold', 'FontName', 'arial');
        
        annotation('textbox',[.02, .9, .1, .1], 'String',{subj, sprintf('Trial %i', tt)},'FitBoxToText','on',...
            'FontSize', 20, 'FontWeight', 'bold', 'FontName', 'arial');
        
        if DOSAVEPLOT
            if ~isMonocular(ii)
                fname = sprintf('%s_microsaccade_index-%i-maybenot', subj, ii);
            else
                fname = sprintf('%s_microsaccade_index-%i-CHECKME', subj, ii);
            end
            print(fh, fullfile(imgPath, 'MonocularMicrosaccades', [fname, '.png']), '-dpng');
            print(fh, fullfile(imgPath, 'MonocularMicrosaccades', [fname, '.eps']), '-depsc');
        end
    end
end


end