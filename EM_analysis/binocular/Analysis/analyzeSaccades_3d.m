function out = analyzeSaccades_3d(pptrials, counts, params, DOPLOT, imgPath)
% here we only analyze data if we feel that the re-calibrations were done
% well so that we can analyze movements in 3d
run('saccadeMatrixIndex.m');
out = struct();

subjname = pptrials{1, 1}.Subject_Name;
%% params
tasks = params.tasks; % generalize params.tasks;
plottasks = params.plottasks;

ntasks = length(tasks);


%% extract saccades to analyze
saccades = cellfun(@(x) x.saccadePairs, pptrials(~counts.exclude & ~counts.badcalibration  & ~counts.lowperformance, 1),...
    'UniformOutput', false);
msaccades = cellfun(@(x) x.saccadeMon, pptrials(~counts.exclude & ~counts.badcalibration  & ~counts.lowperformance, 1),...
    'UniformOutput', false);
saccades = cat(1, saccades{:});
msaccades = cat(1, msaccades{:});
saccades = [saccades; msaccades];

isMonocular = isnan(saccades(:, EYE1_START)) | isnan(saccades(:, EYE2_START));

%% count number of saccades
fprintf('SACCADE COUNTS (good recal) \n');
for ti = 0:(ntasks-1)
    thistask = saccades(:, TASK_INDEX) == ti;
    ntotal = sum(thistask);
    nmono = sum(isMonocular(:) & thistask(:));
    fprintf('\t%s - %i - monocular %i\n', tasks{ti+1}, ntotal, nmono);
end
ntotal = length(isMonocular);
nmono = sum(isMonocular(:));
fprintf('\ttotal - %i - monocular %i\n', ntotal, nmono);

%% pos offset before vs saccade amplitude
for ti = plottasks % fixation and task
    thisTask = saccades(:, TASK_INDEX) == ti;
    theseSaccades = saccades(thisTask, :);
    
    dx_start = (theseSaccades(:, EYE1_XSTART) - theseSaccades(:, EYE2_XSTART))/2;
    dy_start = (theseSaccades(:, EYE1_YSTART) - theseSaccades(:, EYE2_YSTART))/2;
    
    dx = (theseSaccades(:, EYE1_AMP) .* cos(theseSaccades(:, EYE1_DIR)) - ...
        theseSaccades(:, EYE2_AMP) .* cos(theseSaccades(:, EYE2_DIR)))/2;
    dy = (theseSaccades(:, EYE1_AMP) .* sin(theseSaccades(:, EYE1_DIR)) - ...
        theseSaccades(:, EYE2_AMP) .* sin(theseSaccades(:, EYE2_DIR)))/2;
    
    use = ~isMonocular(thisTask);
    
    if sum(use) < 5
        continue; 
    end
    
    [rrx, ppx] = corrcoef(dx(use), dx_start(use));
    [rry, ppy] = corrcoef(dy(use), dy_start(use));
    
    out.(sprintf('Vcorrx_%s_rho_data', tasks{ti + 1})) = {dx_start(use), dx(use)};
    out.(sprintf('Vcorrx_%s_rho', tasks{ti+1})) = [rrx(1, 2), ppx(1, 2)];
    
    out.(sprintf('Vcorry_%s_rho_data', tasks{ti+1})) = {dy_start(use), dy(use)};
    out.(sprintf('Vcorry_%s_rho', tasks{ti+1})) = [rry(1, 2), ppy(1, 2)];
end

%% plotting
if DOPLOT
    plotSaccadeInfo_3d(out, params, subjname, imgPath);
end
end