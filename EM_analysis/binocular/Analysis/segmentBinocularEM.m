function pptrials = segmentBinocularEM(pptrials, counts, params)
% pptrials = segmentBinocularEM(pptrials) classifies binocular traces into
% saccades and drifts. Drifts are defined as drift in both eyes

    for ii = 1:length(pptrials)
        
        % initialize everything with nans
        pptrials{ii, 1}.saccadePairs = [];
        pptrials{ii, 1}.saccadeMon = [];
        
        pptrials{ii, 1}.isdrift = false(pptrials{ii, 1}.samples, 1);
        pptrials{ii, 2}.isdrift = false(pptrials{ii, 1}.samples, 1);
        if ~counts.exclude(ii) || ~(...
                isempty(pptrials{ii, 1}.microsaccades.start) && ...
                isempty(pptrials{ii, 2}.microsaccades.start) && ...
                isempty(pptrials{ii, 1}.saccades.start) && ...
                isempty(pptrials{ii, 2}.saccades.start))
            
            % pairs saccadic events between eyes. if monocular event is found,
            % searches other eye trace using more lenient criteria.
            % updateWithMonocularSaccades integrates new events (from more
            % lenient criteria) into pptrials
            [saccadePairs, saccadeMon] = pairSaccadicEvents(pptrials, ii, params);

            if ~isempty(saccadePairs)
                pptrials = updateWithMonocularSaccades(pptrials, ii, saccadePairs);
                pptrials{ii, 1}.saccadePairs = saccadePairs;
            end
            if ~isempty(saccadeMon)
                pptrials{ii, 1}.saccadeMon = saccadeMon;
            end
            
            % once saccadic events are found, define drift periods
            % eye 1
            driftstarts1 = pptrials{ii, 1}.drifts.start;
            driftstops1 = pptrials{ii, 1}.drifts.duration + driftstarts1 - 1;
            isdrift1 = ...
                getBinFromIndices(pptrials{ii, 1}.samples, driftstarts1, driftstops1);
            pptrials{ii, 1}.isdrift = isdrift1;
            
            % eye 2
            driftstarts2 = pptrials{ii, 2}.drifts.start;
            driftstops2 = pptrials{ii, 2}.drifts.duration + driftstarts2 - 1;
            isdrift2 = ...
                getBinFromIndices(pptrials{ii, 1}.samples, driftstarts2, driftstops2);
            pptrials{ii, 2}.isdrift = isdrift2;
            
        end
    end
end