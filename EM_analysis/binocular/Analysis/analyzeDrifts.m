function out = analyzeDrifts(pptrials, counts, params, DOPLOT, imgPath, buffer)
% analyze drifts - look at monocular characteristics of drift and
% correlation the values between the two eyes. includes analysis of drift
% that do not need good spatial localization - so things like speed,
% curvature, and re-centered positions

subjname = pptrials{1, 1}.Subject_Name;
fh = 200;

tasks = params.tasks; %
startTimes = params.startTimes; % 
stopTimes =  params.stopTimes;

% could do instantaneous only to save time
group_label = {'instantaneous', 'average'};
groupfunc = {@trimSegment, @meanBySegment};

if ~exist('buffer', 'var')
    buffer = 20;
else
    buffer = max(buffer, 20);
end

out.buffer = buffer;

%% drift durations
dur_save = cell(length(tasks), 1);
for ii = 1:size(pptrials, 1)
    if counts.exclude(ii)
        continue; 
    end
    isdrift = pptrials{ii, 1}.isdrift & pptrials{ii, 2}.isdrift;
    
    for ti = 1:length(tasks)
        tt = isdrift(round(pptrials{ii, 1}.(startTimes{ti})):round(pptrials{ii, 1}.(stopTimes{ti})));
        
        if any(~tt) && tt(end)
            ee = find(~tt, 1, 'last');
            tt = tt(1:ee);
        end
        
        if any(~tt) && tt(1)
            ss = find(~tt, 1, 'first');
            tt = tt(ss:end);
        end
        
        [se, ee] = getIndicesFromBin(tt);
        dur_save{ti} = [dur_save{ti}; ee - se + 1];
    end
end
c = params.drift.dur;

for ti = 1:length(tasks)
    tmp = dur_save{ti};
    tmp(tmp < 99) = [];
    nn = hist(tmp, c);
    nn = nn / sum(nn);
    
    out.(sprintf('duration_%s_data', tasks{ti})) = tmp;
    out.(sprintf('duration_%s_hist', tasks{ti})) = nn;
    out.(sprintf('duration_%s_hist_bins', tasks{ti})) = c;
    out.(sprintf('duration_%s_MS', tasks{ti})) = ...
        [mean(tmp), std(tmp) / sqrt(length(tmp))];
end

%% drift positions (relative) - histograms
useTrials = ~counts.exclude & ~counts.badcalibration & ~counts.lowperformance;
pos_save = cell(length(tasks), 1);
c = params.drift.pos;

g = @subtractStartFromSegment;

fh = fh + 1;
figure(fh); clf;
for ti = 1:length(tasks)
    pos1x = cellfun(@(x, y) g(...
        x.x.shifted(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
        x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti})))),...
        pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
    pos2x = cellfun(@(x, y) g(...
        x.x.shifted(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
        x.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))) & y.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti})))),...
        pptrials(useTrials,2), pptrials(useTrials, 1), 'UniformOutput', false);
    pos1y = cellfun(@(x, y) g(...
        x.y.shifted(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
        x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti})))),...
        pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
    pos2y = cellfun(@(x, y) g(...
        x.y.shifted(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
        x.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))) & y.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti})))),...
        pptrials(useTrials,2), pptrials(useTrials, 1), 'UniformOutput', false);

    pos1x = cat(2, pos1x{:});
    pos1y = cat(2, pos1y{:});
    pos2x = cat(2, pos2x{:});
    pos2y = cat(2, pos2y{:});
    
    kill = isnan(pos1x) | isnan(pos2x) | isnan(pos1y) | isnan(pos2y);
    pos1x(kill) = [];
    pos2x(kill) = [];
    pos1y(kill) = [];
    pos2y(kill) = [];
    
    pos_save{ti} = [pos1x(:), pos1y(:), pos2x(:), pos2y(:)];
    
    n1 = histcounts2(pos1x, pos1y, c, c);
    n1 = n1 / sum(n1(:));
    
    n2 = histcounts2(pos2x, pos2y, c, c);
    n2 = n2 / sum(n2(:));
    
    out.(sprintf('posx_%s_data', tasks{ti})) = {pos1x, pos2x};
    out.(sprintf('posy_%s_data', tasks{ti})) = {pos1y, pos2y};
    out.(sprintf('pos_%s_hist', tasks{ti})) = {n1, n2};
    out.(sprintf('pos_%s_hist_bins', tasks{ti})) = c;
    
    
end

%% relative changes in vergence
for ti = 1:length(tasks)
    dvx = (pos_save{ti}(:, 1) - pos_save{ti}(:, 3)) / 2;
    dvy =(pos_save{ti}(:, 2) - pos_save{ti}(:, 4)) / 2;
    
    nx = hist(dvx, params.drift.vergence_change{1});
    ny = hist(dvy, params.drift.vergence_change{2});
    
    nx = nx / sum(nx(:));
    ny = ny / sum(ny(:));
    
    out.(sprintf('dV_%s_data', tasks{ti})) = {dvx, dvy};
    out.(sprintf('dV_%s_hist', tasks{ti})) = {nx, ny};
    out.(sprintf('dV_%s_hist_bins', tasks{ti})) = params.drift.vergence_change;
    out.(sprintf('dV_%s_MS', tasks{ti})) = {...
        [nanmean(dvx), nanstd(dvx)],...
        [nanmean(dvy), nanstd(dvy)]};
end

%% drift speeds (one dimension) - histograms
useTrials = ~counts.exclude & ~counts.lowperformance;
vel_save = cell(length(groupfunc), length(tasks));
for gi = 1:length(groupfunc)
    c = linspace(params.drift.speed_lims(1), params.drift.speed_lims(2),...
        params.nbins(gi));
    
    g = groupfunc{gi};

    ms = nan(2, 2, 3); % task x eye x (mean or sd)
    for ti = 1:length(tasks)
        vel1 = cellfun(@(x, y) g(...
            x.velocity_sg(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        vel2 = cellfun(@(x, y) g(x.velocity_sg(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            x.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))) & y.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,2), pptrials(useTrials, 1), 'UniformOutput', false);
        
        vel1 = cat(2, vel1{:}); % arcmin / s
        vel2 = cat(2, vel2{:}); % arcmin / s

        fprintf('%s velocity outliers: %1.2f%% and %1.2f%%\n',...
            tasks{ti}, ...
            sum(vel1 > 300) * 100/length(vel1),...
            sum(vel2 > 300) * 100/length(vel2));
        
        kill = vel1 > 300 | vel2 > 300 | isnan(vel1) | isnan(vel2);
        vel1(kill) = [];
        vel2(kill) = [];
        
        vel_save{gi, ti} = [vel1(:), vel2(:)];
        
        ms(ti, 1, 1) = mean(vel1);
        ms(ti, 1, 2) = std(vel1);
        ms(ti, 1, 3) = length(vel1);
        ms(ti, 2, 1) = mean(vel2);
        ms(ti, 2, 2) = std(vel2);
        ms(ti, 2, 3) = length(vel2);
        
        n1 = hist(vel1, c);
        n1 = n1 / sum(n1);
        
        n2 = hist(vel2, c);
        n2 = n2 / sum(n2);
        
        out.(sprintf('speed_%s_%s_data', group_label{gi}, tasks{ti})) = {vel1, vel2};
        out.(sprintf('speed_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mean(vel1), std(vel1) / sqrt(length(vel1))],...
            [mean(vel2), std(vel2) / sqrt(length(vel2))]};
        out.(sprintf('speed_%s_%s_hist', group_label{gi}, tasks{ti})) = {n1, n2};
        out.(sprintf('speed_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = c;
    end

end

%% speed correlation
for gi = 1:length(groupfunc)
    for ti = 1:length(tasks)
        le = vel_save{gi, ti}(:, 2);
        re = vel_save{gi, ti}(:, 1);
        
        [rr, pp] = corrcoef(le, re);
        
        out.(sprintf('speed_%s_%s_rho', group_label{gi}, tasks{ti})) = [rr(1, 2), pp(1, 2)];
    end
    
end

%% drift velocities (two dimension)
useTrials = ~counts.exclude & ~counts.lowperformance;
vel2_save = cell(length(groupfunc), length(tasks));
for gi = 1:length(groupfunc)
    
    g = groupfunc{gi};

    ms = nan(2, 2, 3); % task x eye x (mean or sd)
    for ti = 1:length(tasks)
        vel1x = cellfun(@(x, y) g(...
            x.x.velocity_sm(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        vel2x = cellfun(@(x, y) g(x.x.velocity_sm(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            x.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))) & y.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,2), pptrials(useTrials, 1), 'UniformOutput', false);
        vel1y = cellfun(@(x, y) g(...
            x.y.velocity_sm(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        vel2y = cellfun(@(x, y) g(x.y.velocity_sm(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            x.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))) & y.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,2), pptrials(useTrials, 1), 'UniformOutput', false);

        vel1x = cat(2, vel1x{:}) * 1000; % arcmin / s
        vel2x = cat(2, vel2x{:}) * 1000; % arcmin / s
        vel1y = cat(2, vel1y{:}) * 1000; % arcmin / s
        vel2y = cat(2, vel2y{:}) * 1000; % arcmin / s
        
        
        kill = vel1x > 300 | vel2x > 300 | vel1y > 300 | vel2y > 300 |...
            isnan(vel1x) | isnan(vel2x) | isnan(vel1y) | isnan(vel2y);
        vel1x(kill) = [];
        vel2x(kill) = [];
        vel1y(kill) = [];
        vel2y(kill) = [];
        
        vel2_save{gi, ti} = [vel1x(:), vel1y(:), vel2x(:), vel2y(:)];
        
        [rrx, ppx] = corrcoef(vel1x, vel2x);
        [rry, ppy] = corrcoef(vel1y, vel2y);
        
        
        % computations for horizontal
        c = linspace(params.drift.vel_lims(1), params.drift.vel_lims(2),...
            params.nbins(gi));
        [n2, xedges, yedges] = histcounts2(vel1x, vel2x, c, c);
        
        out.(sprintf('velx_%s_%s_data', group_label{gi}, tasks{ti})) = {vel1x, vel2x};
        out.(sprintf('velx_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mean(vel1x), std(vel1x) / sqrt(length(vel1x))],...
            [mean(vel2x), std(vel2x) / sqrt(length(vel2x))]};
        out.(sprintf('velx_%s_%s_hist', group_label{gi}, tasks{ti})) = n2;
        out.(sprintf('velx_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = {xedges, yedges};
        out.(sprintf('velx_%s_%s_rho', group_label{gi}, tasks{ti})) = [rrx(1, 2), ppx(1, 2)];
        
        % computations for vertical
        [n3, xedges, yedges] = histcounts2(vel1y, vel2y, c, c);
        
        out.(sprintf('vely_%s_%s_data', group_label{gi}, tasks{ti})) = {vel1y, vel2y};
        out.(sprintf('vely_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mean(vel1y), std(vel1y) / sqrt(length(vel1y))],...
            [mean(vel2y), std(vel2y) / sqrt(length(vel2y))]};
        out.(sprintf('vely_%s_%s_hist', group_label{gi}, tasks{ti})) = n3;
        out.(sprintf('vely_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = {xedges, yedges};
        out.(sprintf('vely_%s_%s_rho', group_label{gi}, tasks{ti})) = [rry(1, 2), ppy(1, 2)];
        
    end
end

%% drift curvature (one dimension) - histograms
useTrials = ~counts.exclude & ~counts.lowperformance;
cur_save = cell(length(groupfunc), length(tasks));
for gi = 1:length(groupfunc)
    c = linspace(params.drift.curv_lims(1), params.drift.curv_lims(2),...
        params.nbins(gi));
    
    g = groupfunc{gi};
    
    fh = fh + 1;
    figure(fh); clf;
    ms = nan(2, 2, 3); % task x eye x (mean or sd)
    for ti = 1:length(tasks)
        cur1 = cellfun(@(x, y) g(...
            x.curvature(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            x.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))) & y.isdrift(round(x.(startTimes{ti})):round(x.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,1), pptrials(useTrials, 2), 'UniformOutput', false);
        cur2 = cellfun(@(x, y) g(x.curvature(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            x.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))) & y.isdrift(round(y.(startTimes{ti})):round(y.(stopTimes{ti}))),...
            buffer),...
            pptrials(useTrials,2), pptrials(useTrials, 1), 'UniformOutput', false);
        cur1 = cat(2, cur1{:}); % arcmin / s
        cur2 = cat(2, cur2{:}); % arcmin / s
        
        fprintf('%s curvature outliers: %1.2f%% and %1.2f%%\n',...
            tasks{ti}, ...
            sum(cur1 > 200) * 100/length(cur1),...
            sum(cur2 > 200) * 100/length(cur2));
        kill = cur1 > 200 | cur2 > 200 | isnan(cur1) | isnan(cur2);
        cur1(kill) = [];
        cur2(kill) = [];
        
        cur_save{gi, ti} = [cur1(:), cur2(:)];
       
        ms(ti, 1, 1) = mean(cur1);
        ms(ti, 1, 2) = std(cur1);
        ms(ti, 1, 3) = length(cur1);
        ms(ti, 2, 1) = mean(cur2);
        ms(ti, 2, 2) = std(cur2);
        ms(ti, 2, 3) = length(cur2);
        
        n1 = hist(cur1, c);
        n1 = n1 / sum(n1);
        
        n2 = hist(cur2, c);
        n2 = n2 / sum(n2);
        
        out.(sprintf('curv_%s_%s_data', group_label{gi}, tasks{ti})) = {cur1, cur2};
        out.(sprintf('curv_%s_%s_MS', group_label{gi}, tasks{ti})) = {...
            [mean(cur1), std(cur1) / sqrt(length(cur1))],...
            [mean(cur2), std(cur2) / sqrt(length(cur2))]};
        out.(sprintf('curv_%s_%s_hist', group_label{gi}, tasks{ti})) = {n1, n2};
        out.(sprintf('curv_%s_%s_hist_bins', group_label{gi}, tasks{ti})) = c;
    end
    
end

%% curvature correlation
for gi = 1:length(groupfunc)
    fh = fh + 1;
    figure(fh); clf;
    for ti = 1:length(tasks)
        le = cur_save{gi, ti}(:, 2);
        re = cur_save{gi, ti}(:, 1);
        
        [rr, pp] = corrcoef(le, re);
        out.(sprintf('curv_%s_%s_rho', group_label{gi}, tasks{ti})) = [rr(1, 2), pp(1, 2)];

%         subplot(1, length(tasks), ti);
%         scatter(re, le, ...
%             1, 'b', 'filled', 'MarkerFaceAlpha', .3);
%         text(double(quantile(re, .95)), double(quantile(le, .95)),...
%             sprintf('r = %1.2f', rr(1, 2)), 'FontSize', 14);
%         text(double(quantile(re, .95)), double(quantile(le, .85)),...
%             sprintf('p = %1.2f', pp(1, 2)), 'FontSize', 14);
%         axis image;
%         xlabel(sprintf('RIGHT %s curvature (1/arcmin)', group_label{gi}));
%         ylabel(sprintf('LEFT %s curvature (1/arcmin)', group_label{gi}));
%         standardPlot;
%         title(tasks{ti});
    end
    
%     annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
%         'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
%         'FontWeight', 'bold');
%     
%     if DOPLOT && ~isempty(imgPath)
%         fname = fullfile(imgPath, sprintf('%s_curvature_corr_%s', subjname, group_label{gi}));
%         print([fname, '.png'], '-dpng');
%         print([fname, '.eps'], '-depsc');
%     end
    
end

%% plotting if asked
if DOPLOT
    plotDriftInfo(out, params, subjname, imgPath);
end
end

function x = nanBuffer(x, buffer)
if ~exist('buffer', 'var')
    buffer = 20;
end

bin = ~isnan(x);

[startIndices, stopIndices] = getIndicesFromBin(bin);

for i = 1:length(startIndices)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        x(ss:ee) = nan;
        continue;
    end
    
    x(ss:ss+buffer) = nan;
    x(ee-buffer:ee) = nan;
end

end

function x = trimSegment(x, bin, buffer)
if ~exist('buffer', 'var')
    buffer = 20;
end

[startIndices, stopIndices] = getIndicesFromBin(bin);

m = cell(size(startIndices))';

for i = 1:length(m)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < max(50, buffer)
        continue;
    end
    
    m{i} = x(ss+buffer:ee-buffer);
end

x = cat(2, m{:});

end

function m = meanBySegment(x, bin, buffer)

[startIndices, stopIndices] = getIndicesFromBin(bin);
m = nan(size(startIndices))';

for i = 1:length(m)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        continue;
    end
    
    m(i) = mean(x(ss+buffer:ee-buffer));
end
end

function x = subtractMeanFromSegment(x, bin, buffer)
if ~exist('buffer', 'var')
    buffer = 20; 
end

[startIndices, stopIndices] = getIndicesFromBin(bin);

for i = 1:length(startIndices)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        x(ss:ee) = nan;
        continue;
    end
    
    x(ss:ss+buffer) = nan;
    x(ee-buffer:ee) = nan;
    
    x(ss:ee) = x(ss:ee) - nanmean(x(ss+buffer:ee-buffer));
end
end

function x = subtractStartFromSegment(x, bin, buffer)
if ~exist('buffer', 'var')
    buffer = 20; 
end

[startIndices, stopIndices] = getIndicesFromBin(bin);

for i = 1:length(startIndices)
    ss = startIndices(i);
    ee = stopIndices(i);
    
    if ee - ss < 50
        x(ss:ee) = nan;
        continue;
    end
    
    x(ss:ss+buffer) = nan;
    x(ee-buffer:ee) = nan;
    
    x(ss:ee) = x(ss:ee) - x(ss + buffer + 1);
end
end