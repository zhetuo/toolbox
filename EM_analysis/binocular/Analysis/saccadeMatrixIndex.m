% tasks: 0 = fixation, 1 = task, 2 = ramp/explore
% these correspond to columns in saccadePairs
EYE1_START = 1; % times of start and stop
EYE1_STOP = 2; 
EYE2_START = 3;
EYE2_STOP = 4;
EYE1_AMP = 5;
EYE2_AMP = 6;
EYE1_DIR = 7;
EYE2_DIR = 8;
EYE1_PKVEL = 9;
EYE2_PKVEL = 10;
EYE1_XSTART = 11;
EYE1_YSTART = 12;
EYE1_XSTOP = 13;
EYE1_YSTOP = 14;
EYE2_XSTART = 15;
EYE2_YSTART = 16;
EYE2_XSTOP = 17;
EYE2_YSTOP = 18;
SACC_BIN = 19; % 0 is binocular, 1 is right MS, 2 is left MS
TASK_INDEX = 20; % 0 is fixation, 1 is snellen, 2 is in between
TRIAL_INDEX = 21;