function psd_VergVers(pptrials, subjname, DOPLOT, imgPath)

Kval = logspace(-1, 1.7, 30);
Kang = 0:.628:2*pi;

nfft = 512;
window = hann(nfft);
f = (0:nfft-1) / nfft * 1000;

fname = sprintf('PSD_%i_%i_%i.mat', nfft, length(Kval), length(Kang));

if ~exist(fullfile(imgPath, fname), 'file')
    fprintf('computing PSD\n');
    PS1 = 0; PS2 = 0; cnt1 = 0;%Fixation vergence and versions
    PS3 = 0; PS4 = 0; cnt2 = 0;% Snellen vergence and versions
    
    for ii = 1:size(pptrials, 1) % loop through trials
        if rem(ii, 10) == 0
            fprintf('%i of %i trials done\n', ii, size(pptrials, 1)); 
        end
        isdrift = pptrials{ii, 1}.isdrift & pptrials{ii, 2}.isdrift;
        [start, stop] = getIndicesFromBin(isdrift);
        
        as = (pptrials{ii, 1}.analysisStart + pptrials{ii, 2}.analysisStart)/2;
        
        kill = start > pptrials{ii, 1}.timeRampStart & start < as;
        start(kill) = []; stop(kill) = [];
        start = min(pptrials{ii, 1}.samples, start + 15);
        stop = max(1, stop - 15);
        
        for si = 1:length(start)
            ss = start(si);
            ee = stop(si);
            x = pptrials{ii, 1}.vs_components(ss:ee, 1);
            y = pptrials{ii, 1}.vs_components(ss:ee, 2);
            
            vx = pptrials{ii, 1}.vs_components(ss:ee, 3);
            vy = pptrials{ii, 1}.vs_components(ss:ee, 4);
            
            if length(x) < nfft
                x = [x; zeros(nfft - length(x), 1)];
                y = [y; zeros(nfft - length(y), 1)];
                vx = [vx; zeros(nfft - length(vx), 1)];
                vy = [vy; zeros(nfft - length(vy), 1)];
            elseif length(x) > nfft
                x = x(1:nfft);
                y = y(1:nfft);
                vx = vx(1:nfft);
                vy = vy(1:nfft);
            end
            
            [pstmp1, f] = QRad_Welch_SingleTrace(Kval, nfft, Kang, x, y, window, 1000);
            pstmp2 = QRad_Welch_SingleTrace(Kval, nfft, Kang, vx, vy, window, 1000);
            
            if ss < pptrials{ii, 1}.timeRampStart
                PS3 = PS3 + pstmp1;
                PS4 = PS4 + pstmp2;
                cnt2 = cnt2 + 1;
            else
                PS1 = PS1 + pstmp1;
                PS2 = PS2 + pstmp2;
                cnt1 = cnt1 + 1;
            end
        end
        
        
    end % end trial loop
    PS1 = PS1 / cnt1;
    PS2 = PS2 / cnt1;
    PS3 = PS3 / cnt2;
    PS4 = PS4 / cnt2;
    save(fullfile(imgPath, fname), 'PS*', 'cnt*', 'f');
    
else
    fprintf('loading PSD\n');
    load(fullfile(imgPath, fname));
end

pw1 = pow2db(mean(PS1(f > 2 & f < 40, :)));
pw2 = pow2db(mean(PS2(f > 2 & f < 40, :)));
pw3 = pow2db(mean(PS3(f > 2 & f < 40, :)));
pw4 = pow2db(mean(PS4(f > 2 & f < 40, :)));

figure(500); clf;
subplot(2, 2, 1);
pcolor(Kval, f, pow2db(PS1));
shading interp; colormap hot;
set(gca, 'YScale', 'log', 'XScale', 'log', 'YTick', [1, 10, 30, 80],...
    'XTick', [1, 5, 10, 15, 30, 50]);
ylim([2, 80]); xlim([3, 51]);
title('Snellen version');
cl = caxis;

subplot(2, 2, 2);
pcolor(Kval, f, pow2db(PS2));
shading interp; colormap hot;
set(gca, 'YScale', 'log', 'XScale', 'log', 'YTick', [1, 10, 30, 80],...
    'XTick', [1, 5, 10, 15, 30, 50]);
ylim([2, 80]); xlim([3, 51]);
title('Snellen vergence');
caxis(cl);

subplot(2, 2, 3);
pcolor(Kval, f, pow2db(PS3));
shading interp; colormap hot;
set(gca, 'YScale', 'log', 'XScale', 'log', 'YTick', [1, 10, 30, 80],...
    'XTick', [1, 5, 10, 15, 30, 50]);
ylim([2, 80]); xlim([3, 51]);
title('Fixation version');
caxis(cl);

subplot(2, 2, 4);
pcolor(Kval, f, pow2db(PS4));
shading interp; colormap hot;
set(gca, 'YScale', 'log', 'XScale', 'log', 'YTick', [1, 10, 30, 80],...
    'XTick', [1, 5, 10, 15, 30, 50]);
ylim([2, 80]); xlim([3, 51]);
title('Fixation vergence');
caxis(cl);

annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
    'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
    'FontWeight', 'bold');


if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_psd_2d', subjname));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end

figure(501); clf; hold on;
plot(Kval, pw1, 'b-', 'linewidth', 2);
plot(Kval, pw2, 'r-', 'linewidth', 2);
plot(Kval, pw3, 'k--', 'linewidth', 2);
plot(Kval, pw4, 'm--', 'linewidth', 2);
legend('Snellen - vers', 'Snellen - verg', 'Fix - vers', 'Fix - verg', ...
    'Location', 'southeast');
set(gca, 'XScale', 'log',...
    'XTick', [1, 5, 10, 15, 30, 50]);
xlim([3, 51]);
xlabel('spatial frequency (cpd)');
ylabel('power (db)');
standardPlot;

annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
    'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
    'FontWeight', 'bold');


if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_psd_1d', subjname));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end
end