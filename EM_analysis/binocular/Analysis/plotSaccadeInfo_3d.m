function plotSaccadeInfo_3d(out, params, subjname, imgPath)
if length(subjname) > 4
    subjname_print = subjname(1:4);
else
    subjname_print = subjname; 
end

% out is the output of analyzeDrifts.m
if ~exist('imgPath', 'var')
    imgPath = '';
end

tasks = params.tasks;
plottasks = params.plottasks + 1;
cols = params.taskColors;

fh = 500;

FONTSIZE = 20;
LFONTSIZE = 16;
SCATTERDOTSIZE = 15;

eye_labels = {'right eye', 'left eye'};

cols4 = params.taskEyeColors;

%% vergence change vs prior positional offset (horizontal);
for ti = plottasks
    if ~isfield(out, sprintf('Vcorrx_%s_rho_data', tasks{ti}))
        continue; 
    end
    tmp = out.(sprintf('Vcorrx_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('Vcorrx_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), false, false);
    title(sprintf('%s: horizontal (arcmin)', tasks{ti}));
    xlabel('prior positional offset');
    ylabel('vergence change');
    
    mysavefig(sprintf('%s_Vcorrx-corr', tasks{ti}));
end

%% vergence change vs prior positional offset (vertical);
for ti = plottasks
    if ~isfield(out, sprintf('Vcorry_%s_rho_data', tasks{ti}))
        continue; 
    end
    tmp = out.(sprintf('Vcorry_%s_rho_data', tasks{ti}));
    tmpr = out.(sprintf('Vcorry_%s_rho', tasks{ti}));
    
    fh = fh + 1;
    figure(fh); clf; hold on;
    plotcorr(tmp{1}, tmp{2}, tmpr(1), tmpr(2), false, false);
    title(sprintf('%s: vertical (arcmin)', tasks{ti}));
    xlabel('prior positional offset');
    ylabel('vergence change');
    
    mysavefig(sprintf('%s_Vcorry-corr', tasks{ti}));
end

%% nested function for saving figures
    function mysavefig(figname)
        annotation('textbox',[.02, .9, .1, .1], 'String', subjname_print,...
            'FitBoxToText', 'on', 'FontSize', FONTSIZE, 'FontName', 'arial',...
            'FontWeight', 'bold');
        
        if ~isempty(imgPath)
            fname = fullfile(imgPath, sprintf('%s_%s', subjname_print, figname));
            print([fname, '.png'], '-dpng');
            print([fname, '.eps'], '-depsc');
        end
    end

%% nested function to plot 1d distributions (not polar) across tasks & eye
    function plot_1d_dist(prop)
        hh = nan(length(plottasks), 2);
        for tii = plottasks
            tmpcc = out.(sprintf('%s_%s_hist_bins', prop, tasks{tii+1}));
            tmphh = out.(sprintf('%s_%s_hist', prop, tasks{tii+1}));
            for EYEi = 1:2
                if length(tmphh) < EYEi
                    break; 
                end
                hh(tii+1, EYEi) = plot(tmpcc{EYEi}(1:end-1), tmphh{EYEi}(1:end-1),...
                    'linewidth', 2, 'DisplayName', tasks{tii+1},...
                    'Color', cols4{tii+1, EYEi});
            end
        end
        
        for tii = plottasks
            tmpms = out.(sprintf('%s_%s_MS', prop, tasks{tii+1}));
            for EYEi = 1:2
                vertLineThrough(tmpms{EYEi}(1), cols4{tii+1, EYEi}, gca, '--');
            end
        end
        
        standardPlot;
        legend(hh(:, 1), 'Location', 'northeast', 'FontSize', LFONTSIZE);
    end

%% nested function to plot correlation data
    function plotcorr(right, left, rho, p, islog, samelim)
        if ~exist('samelim', 'var')
            samelim = true; 
        end
        plot(right, left, '.', 'Color', cols{ti}, 'markersize', SCATTERDOTSIZE);
        xlabel('right eye'); ylabel('left eye');
        standardPlot;
        
        if islog
            set(gca, 'XScale', 'log', 'YScale', 'log');
        end
        
        xl = xlim;
        if samelim
            yl = xl;
            ylim(xl);
        else
            yl = ylim; 
        end
        
        if ~islog
            xx = xl(1) + .8 * diff(xl);
            yy = yl(1) + .05*diff(yl);
        else
            xx = 10.^(log10(xl(1)) + .8 * diff(log10(xl)));
            yy = 10.^(log10(yl(1)) + .05*diff(log10(yl)));
        end
        text(xx, yy,...
            sprintf('$$\\rho=%1.1f$$, $$p=%1.2f$$', rho, p),...
            'FontSize', FONTSIZE - 2, 'Interpreter', 'latex');
        
        xx1 = min(xl(1), yl(1));
        xx2 = max(xl(2), yl(2));
        
        plot([xx1, xx2], [xx1, xx2], 'k--', 'linewidth', 1);
        axis image;
    end


end

