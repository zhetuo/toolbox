function plotCorrelationVWinsize(out, winsizes, subjname, DOPLOT, imgPath)
% plot for individuals

flds = {'speed_instantaneous', 'speed_average', ...
    'velx_instantaneous', 'vely_instantaneous', ...
    'velx_average', 'vely_average', ...
    'curv_instantaneous', 'curv_average',...
    ...'speedx_instantaneous', 'speedy_instantaneous',...
    ...'speedx_average', 'speedy_average',...
    'vs_velx_instantaneous', 'vs_vely_instantaneous', ...
    'vs_velx_average', 'vs_vely_average'};

if isfield(out, 'vs_velx_instantaneous')
    lastfld = length(flds);
else
    lastfld = length(flds) - 4;
end

fh = 0;
for fi = 1:lastfld
    fh = fh + 1;
    plotCorr(out, winsizes, flds{fi}, fh, subjname, DOPLOT, imgPath);
end
end

function plotCorr(out, winsizes, fld, fh, subjname, DOPLOT, imgPath)
rho1 = cellfun(@(x) x.(sprintf('%s_Fixation_rho', fld)), out, 'UniformOutput', false);
rho1 = cat(1, rho1{:});
rho2 = cellfun(@(x) x.(sprintf('%s_Snellen_rho', fld)), out, 'UniformOutput', false);
rho2 = cat(1, rho2{:});

figure(fh); clf; hold on;
h1 = plot(winsizes, rho1(:, 1), 'k-', 'linewidth', 2);
h2 = plot(winsizes, rho2(:, 1), 'b-', 'linewidth', 2);

use = rho1(:, 2) < .05;
plot(winsizes(use), rho1(use), 'k*', 'linewidth', 2, 'markersize', 10);
plot(winsizes(~use), rho1(~use), 'ko', 'linewidth', 2, 'markersize', 10);

use = rho2(:, 2) < .05;
plot(winsizes(use), rho2(use), 'b*', 'linewidth', 2, 'markersize', 10);
plot(winsizes(~use), rho2(~use), 'bo', 'linewidth', 2, 'markersize', 10);

legend([h1, h2], {'Fixation', 'Snellen'}, 'Location', 'south');

set(gca, 'FontSize', 14);
xlabel('sgfilt window size (ms)');
ylabel('\rho');
title(strrep(fld, '_', ' '), 'FontWeight', 'bold');

grid on;
set(gca, 'YTick', -1:.2:1);
ylim([-1, 1]);
xlim([0, winsizes(end) + 10]);

annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
    'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
    'FontWeight', 'bold');

if DOPLOT
    fname = fullfile(imgPath, sprintf('%s_rho_%s', subjname, fld));
        print([fname, '.png'], '-dpng');
        print([fname, '.eps'], '-depsc');
end
end