function plotDriftInfo_3d(out, params, subjname, imgPath)
if length(subjname) > 4
    subjname_print = subjname(1:4);
else
    subjname_print = subjname; 
end

% out is the output of analyzeDrifts.m
if ~exist('imgPath', 'var')
    imgPath = '';
end

tasks = params.tasks;
plottasks = params.plottasks + 1;
cols = params.taskColors;

fh = 400;

FONTSIZE = 20;
LFONTSIZE = 16;

eye_labels = {'right eye', 'left eye'};

% note that the plots made here are based only on instantaneous
glabel = 'instantaneous';

%% plot distribution of 1d vergences including all drifts (all starting at 0)
fh = fh + 1;
figure(fh); clf; hold on;
figure(fh+1); clf; hold on;
for ti = plottasks
    tmp = out.(sprintf('vs_S_%s_%s_hist_bins', glabel, tasks{ti}));
    c1 = tmp{1}; c2 = tmp{2};
    tmp = out.(sprintf('vs_S_%s_%s_hist', glabel, tasks{ti}));
    nx = tmp{1}; ny = tmp{2};
    tmp = out.(sprintf('vs_S_%s_%s_MS', glabel, tasks{ti}));
    mx = tmp{1}(1); sdx = tmp{1}(2);
    my = tmp{2}(1); sdy = tmp{2}(2);
    
    
    figure(fh);
    plot(c1, nx, [cols{ti}, '-'], 'linewidth', 2);
    xlabel('horizontal version (arcmin)', 'FontSize', FONTSIZE);
    yl = ylim;
    text(c1(1)+.6*range(c1), .2-(ti-1)*.1, ...
        sprintf('%1.1f \\pm %1.1f', mx, sdx), 'Color', cols{ti},...
        'FontSize', FONTSIZE);
    standardPlot;
    xlim(c1([1, end]));
    
    figure(fh+1);
    plot(c2, ny, [cols{ti}, '-'], 'linewidth', 2);
    xlabel('vertical version (arcmin)', 'FontSize', FONTSIZE);
    yl = ylim;
    text(c2(1)+.6*range(c2), .2-(ti-1)*.1, ...
        sprintf('%1.1f \\pm %1.1f', my, sdy), 'Color', cols{ti},...
        'FontSize', FONTSIZE);
    standardPlot;
    xlim(c2([1, end]));
end

figure(fh);
mysavefig('version_x');
figure(fh + 1);
mysavefig('version_y');

%% plot distribution of 1d vergences including all drifts (all starting at 0)
fh = fh + 1;
figure(fh); clf; hold on;
figure(fh+1); clf; hold on;
for ti = plottasks
    tmp = out.(sprintf('vs_V_%s_%s_hist_bins', glabel, tasks{ti}));
    c1 = tmp{1}; c2 = tmp{2};
    tmp = out.(sprintf('vs_V_%s_%s_hist', glabel, tasks{ti}));
    nx = tmp{1}; ny = tmp{2};
    tmp = out.(sprintf('vs_V_%s_%s_MS', glabel, tasks{ti}));
    mx = tmp{1}(1); sdx = tmp{1}(2);
    my = tmp{2}(1); sdy = tmp{2}(2);
    
    
    figure(fh);
    plot(c1, nx, [cols{ti}, '-'], 'linewidth', 2);
    xlabel('horizontal vergence (arcmin)', 'FontSize', FONTSIZE);
    yl = ylim;
    if ti == 2
        text(c1(1), yl(2) - .1 * diff(yl), 'crossed',...
            'FontSize', FONTSIZE);
        text(c1(1)+.8*range(c1), yl(2) - .1 * diff(yl), 'uncrossed',...
            'FontSize', FONTSIZE);
    end
    text(c1(1)+.6*range(c1), .2-(ti-1)*.1, ...
        sprintf('%1.1f \\pm %1.1f', mx, sdx), 'Color', cols{ti},...
        'FontSize', FONTSIZE);
    standardPlot;
    xlim(c1([1, end]));
    
    figure(fh+1);
    plot(c2, ny, [cols{ti}, '-'], 'linewidth', 2);
    xlabel('vertical vergence (arcmin)', 'FontSize', FONTSIZE);
    yl = ylim;
    if ti == 2
        text(c2(1), yl(2) - .1 * diff(yl), 'left high',...
            'FontSize', FONTSIZE);
        text(c2(1)+.9*range(c2), yl(2) - .1 * diff(yl), 'right high', ...
            'FontSize', FONTSIZE);
    end
    text(c2(1)+.6*range(c2), .2-(ti-1)*.1, ...
        sprintf('%1.1f \\pm %1.1f', my, sdy), 'Color', cols{ti},...
        'FontSize', FONTSIZE);
    standardPlot;
    xlim(c2([1, end]));
end

figure(fh);
mysavefig('vergence_x');
figure(fh + 1);
mysavefig('vergence_y');

fh = fh + 1; % for double plotting this round

%% plot drift vergence vs version (2d)

for ti = plottasks
    tmp = out.(sprintf('vs_velx_%s_%s_data', glabel, tasks{ti}));
    velx = tmp{1}; velvx = tmp{2};
    n2 = out.(sprintf('vs_velx_%s_%s_hist', glabel, tasks{ti}));
    
    tmp = out.(sprintf('vs_velx_%s_%s_hist_bins', glabel, tasks{ti}));
    xedges = tmp{1}; yedges = tmp{2};
    [yy2, xx2] = meshgrid(yedges(1:end-1) + mean(diff(yedges))/2,...
            xedges(1:end-1) + mean(diff(xedges))/2);
        
    tmp = out.(sprintf('vs_velx_%s_%s_rho', glabel, tasks{ti}));
    rrx = tmp(1); ppx = tmp(2);
    
    % plot horizontal data
    fh = fh + 1;
    figure(fh); clf; hold on;
    scatter(velx, velvx, 1, 'b', 'filled', 'MarkerFaceAlpha', .3);
    contour(xx2, yy2, n2 / max(n2(:)), 5); colormap('hot');
    title(tasks{ti});
    axis image;
    ylabel('dV_x');
    xlabel('dx');
    set(gca, 'FontSize', FONTSIZE, 'FontName', 'arial');
    text(double(quantile(velx, .95)), double(quantile(velvx, .95)),...
        sprintf('r = %1.2f', rrx), 'FontSize', FONTSIZE-2);
    text(double(quantile(velx, .95)), double(quantile(velvx, .75)),...
        sprintf('p = %1.2f', ppx), 'FontSize', FONTSIZE-2);
    
    tmp = out.(sprintf('vs_vely_%s_%s_data', glabel, tasks{ti}));
    vely = tmp{1}; velvy = tmp{2};
    n2 = out.(sprintf('vs_vely_%s_%s_hist', glabel, tasks{ti}));
    
    tmp = out.(sprintf('vs_vely_%s_%s_hist_bins', glabel, tasks{ti}));
    xedges = tmp{1}; yedges = tmp{2};
    [yy2, xx2] = meshgrid(yedges(1:end-1) + mean(diff(yedges))/2,...
            xedges(1:end-1) + mean(diff(xedges))/2);
        
    tmp = out.(sprintf('vs_vely_%s_%s_rho', glabel, tasks{ti}));
    rry = tmp(1); ppy = tmp(2);
    mysavefig(sprintf('%s_velocity_vs_x_corr', tasks{ti}));
    
    % plot horizontal data
    fh = fh + 1;
    figure(fh); clf; hold on;
    scatter(vely, velvy, 1, 'b', 'filled', 'MarkerFaceAlpha', .3);
    contour(xx2, yy2, n2 / max(n2(:)), 5); colormap('hot');
    title(tasks{ti});
    axis image;
    ylabel('dV_y');
    xlabel('dy');
    set(gca, 'FontSize', FONTSIZE, 'FontName', 'arial');
    text(double(quantile(vely, .95)), double(quantile(velvy, .95)),...
        sprintf('r = %1.2f', rry), 'FontSize', FONTSIZE-2);
    text(double(quantile(vely, .95)), double(quantile(velvy, .75)),...
        sprintf('p = %1.2f', ppy), 'FontSize', FONTSIZE-2);
    
    mysavefig(sprintf('%s_velocity_vs_y_corr', tasks{ti}));
end

%% nested function for saving figures
    function mysavefig(figname)
        annotation('textbox',[.02, .9, .1, .1], 'String', subjname_print,...
            'FitBoxToText', 'on', 'FontSize', FONTSIZE, 'FontName', 'arial',...
            'FontWeight', 'bold');
        
        if ~isempty(imgPath)
            fname = fullfile(imgPath, sprintf('%s_%s', subjname_print, figname));
            print([fname, '.png'], '-dpng');
            print([fname, '.eps'], '-depsc');
        end
    end
end