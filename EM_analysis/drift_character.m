
% subjDrift{subj}.: x,y,xr,yr,x41,y41
%                   vx, vy, v
%                   cur, ang
%                   Dcoef
                   
% modes = 'all', 
%         'velHist', 'curHist', DcoefHist',
%         'exampleTraces', 'gaze2Dhist', 
%         'displaceTime', 'velCurTime','ACF_PACF',
%         'dirTime' 


function out=drift_character(subjDrift,outFileName,FigureOutFolder,modes)

subjN=length(subjDrift);
len=size(subjDrift{1}.x,2);
if ~exist('modes','var')
    modes='all';
end
%% velocity distribution
if any(strcmp(modes,'velHist')) || any(strcmp(modes,'all'))

    disp('plotting velocity distribution');
    edges=[0:5:180];
    figure;
    for subj=subjN:-1:1
        vel=1000*subjDrift{subj}.v(:); % arcmin/s
        velMean=mean(1000*subjDrift{subj}.v');

        h1=histogram(vel(vel<edges(end)),edges,'EdgeColor','none','Normalization','probability');
        velM(subj,:)=h1.Values;

        h2=histogram(velMean(velMean<edges(end)),edges,'EdgeColor','none','Normalization','probability');
        velMeanM(subj,:)=h2.Values;
    end
    out.velHist=velM;  out.velMeanHist=velMeanM; 

    figure('position',[100 100 600 350]); hold on; cols=get(gca,'colorOrder');
    xx=(edges(1:end-1)+edges(2:end))/2;     out.velHist_binCenter=xx;
    subplot(1,2,1); title('instant');
    myShadedErrorBar(velM,xx);
    % plot(xx,mean(velM),'color',cols(1,:), 'lineWidth',2);
    xlabel('drift velocity (arcmin/s)'); ylabel('probability');
    xlim([edges(1) edges(end)]); set(gca,'FontSize',14)

    subplot(1,2,2); title('average');
    myShadedErrorBar(velMeanM,xx);
    xlabel('drift velocity (arcmin/s)'); ylabel('probability');
    xlim([edges(1) edges(end)]); set(gca,'FontSize',14)
    if ~isempty(FigureOutFolder)
    saveFigure(['driftVel_' outFileName],FigureOutFolder);
    end
end

%% curvature distribution
if any(strcmp(modes,'curHist')) || any(strcmp(modes,'all'))
    disp('plotting curvature distribution');
    edges=[0:50];
    figure;
    for subj=subjN:-1:1
        cur=subjDrift{subj}.cur(:); % arcmin/s
        curMean=mean(subjDrift{subj}.cur');

        h1=histogram(cur(cur<edges(end)),edges,'EdgeColor','none','Normalization','probability');
        curM(subj,:)=h1.Values;

        h2=histogram(curMean(curMean<edges(end)),edges,'EdgeColor','none','Normalization','probability');
        curMeanM(subj,:)=h2.Values;
    end
    out.curHist=curM;  out.curMeanHist=curMeanM; 

    figure('position',[100 100 600 300]); hold on; cols=get(gca,'colorOrder');
    xx=(edges(1:end-1)+edges(2:end))/2;   out.curHist_binCenter=xx;
    subplot(1,2,1); title('instant');
    myShadedErrorBar(curM,xx);
    % plot(xx,mean(velM),'color',cols(1,:), 'lineWidth',2);
    xlim([edges(1) edges(end)]); xlabel('curvature (arcmin^{-1})'); ylabel('probability'); 
    set(gca,'FontSize',14)

    subplot(1,2,2); title('average');
    myShadedErrorBar(curMeanM,xx);
    % plot(xx,mean(velMeanM),'color',cols(2,:), 'lineWidth',2);
    xlim([edges(1) edges(end)]); xlabel('curvature (arcmin^{-1})'); ylabel('probability');
    set(gca,'FontSize',14)
    if ~isempty(FigureOutFolder)
    saveFigure(['driftCur_' outFileName],FigureOutFolder);
    end
end

%% diffusion coefficient distribution
if any(strcmp(modes,'DcoefHist')) || any(strcmp(modes,'all'))
    disp('plotting distribution of diffusion coefficient');
    edges=[0:0.5:20]; clear tmp;
    figure;
    for subj=subjN:-1:1
        tmp=subjDrift{subj}.Dcoef;
        h=histogram(tmp(tmp<edges(end)),edges,'EdgeColor','none','Normalization','probability');
        DM(subj,:)=h.Values;
    end
    out.DcoefHist=DM;  
    xx=(edges(1:end-1)+edges(2:end))/2; out.DcoefHist_binCenter=xx;
    figure; myShadedErrorBar(DM,xx);
    xlim([edges(1) edges(end)]);
    xlabel('Dcoef (arcmin^2/sec)'); ylabel('probability');
    set(gca,'FontSize',14)
    if ~isempty(FigureOutFolder)
    saveFigure(['driftDcoef_' outFileName],FigureOutFolder);
    end
end
%% example traces
if any(strcmp(modes,'exampleTraces')) || any(strcmp(modes,'all'))
    disp('plotting example drift trajectories');
    N=30;
    figure('position',[100 100 1100 500]);
    for subj=1:subjN
        if subj<9
            subplot(2,4,subj); hold on;
            for traceI=1:N
                plot(subjDrift{subj}.x(traceI,:)-subjDrift{subj}.x(traceI,1),subjDrift{subj}.y(traceI,:)-subjDrift{subj}.y(traceI,1)); 
            end
            xlabel('x (arcmin)'); ylabel('y (arcmin)'); xlim([-5 5]); ylim([-5 5]);
            set(gca,'FontSize',14)
        end
    end
    if ~isempty(FigureOutFolder)
    saveFigure(['driftTraceExample_' outFileName],FigureOutFolder);
    end
end
%% displacement 2D distribution
if any(strcmp(modes,'gaze2Dhist')) || any(strcmp(modes,'all'))
    disp('plotting 2D distribution gaze distribution');
    xv=-6:0.2:6; yv=-6:0.2:6;
    clear numV;
    for subj=subjN:-1:1
        numV(subj)=size(subjDrift{subj}.x,1);
    end
    N=min(numV);

    xM=[]; yM=[];
    for subj=1:subjN
        x=subjDrift{subj}.x-subjDrift{subj}.x(:,1);
        y=subjDrift{subj}.y-subjDrift{subj}.y(:,1);
        xM=[xM; x(1:N,:)]; yM=[yM; y(1:N,:)];

        figure('position',[100 100 800 300]);
        subplot(1,2,1); plot_2D_density(x(:,end),y(:,end),xv,yv); title('displacement');
        tmpX=x(:,1:10:100); tmpY=y(:,1:10:100);
        subplot(1,2,2); plot_2D_density(tmpX(:),tmpY(:),xv,yv); title('position');
    
        if ~isempty(FigureOutFolder)
        saveFigure([sprintf('displace_hist2D_subj%d_',subj) outFileName],FigureOutFolder); 
        print(gcf,'-depsc','-painters',[FigureOutFolder sprintf('displace_hist2D_subj%d_',subj) outFileName '.eps'])
        end
    end
    out.displace_x=xv;  out.displace_y=yv; 
    figure('position',[100 100 800 300]);
    subplot(1,2,1); out.displace_hist2D=plot_2D_density(xM(:,end),yM(:,end),xv,yv); 
    tmpX=xM(:,1:10:100); tmpY=yM(:,1:10:100);
    subplot(1,2,2); plot_2D_density(tmpX(:),tmpY(:),xv,yv); title('displacement');

    if ~isempty(FigureOutFolder)
    saveFigure(['displace_hist2D_' outFileName],FigureOutFolder); title('position');
    print(gcf,'-depsc','-painters',[FigureOutFolder 'displace_hist2D_' outFileName '.eps'])
    end
end

%% displacement over time.
if any(strcmp(modes,'displaceTime')) || any(strcmp(modes,'all'))
    disp('plotting drift displacement over time');
    for subj=subjN:-1:1
        for dur=len-1:-1:1
            d2=[];
            for t1=1:len-dur
               dx=subjDrift{subj}.x(:,t1+dur)-subjDrift{subj}.x(:,t1); 
               dy=subjDrift{subj}.y(:,t1+dur)-subjDrift{subj}.y(:,t1); 
               d2=[d2 (dx.^2+dy.^2)'];
            end
            out.displace(subj,dur)=mean(d2);
        end
    end

    
    figure; myShadedErrorBar(out.displace);
    if ~isempty(FigureOutFolder)
    saveFigure(['displace_time_' outFileName],FigureOutFolder);
    end
end

%% check autocorrelation of drift x,y,direction
if any(strcmp(modes,'ACF_PACF')) || any(strcmp(modes,'all'))
    disp('plotting ACF, PACF');
    for subj=subjN:-1:1  
          x=double(subjDrift{subj}.x);
          y=double(subjDrift{subj}.y);

          ang=double(subjDrift{subj}.ang);

          for i=size(x,1):-1:1
              pac{subj}.x(i,:)=parcorr(x(i,:));  ac{subj}.x(i,:)=autocorr(x(i,:));
              pac{subj}.y(i,:)=parcorr(y(i,:));  ac{subj}.y(i,:)=autocorr(y(i,:));
              pac{subj}.ang(i,:)=parcorr(ang(i,:));  ac{subj}.ang(i,:)=autocorr(ang(i,:));
          end  
    end
    out.pac=pac; out.ac=ac;


%%
    lagX=(1:size(ac{subj}.x,2))-1;
    figure; clear tmpAC tmpPAC;
    for subj=subjN:-1:1 
       tmpAC(subj,:)=mean(ac{subj}.x);   tmpPAC(subj,:)=mean(pac{subj}.x);
       subplot(subjN,2,2*(subj-1)+1); errorbar(lagX,mean(ac{subj}.x),std(ac{subj}.x));
       subplot(subjN,2,2*(subj-1)+2); errorbar(lagX,mean(pac{subj}.x),std(pac{subj}.x)); 
    end
    subplot(subjN,2,1); title('ACF');  subplot(subjN,2,2); title('PACF'); 
    if ~isempty(FigureOutFolder)
    saveFigure(['x_ACF_PACF_' outFileName],FigureOutFolder);
    end

    figure; 
    subplot(1,2,1); myErrorBar(tmpAC,lagX); title('ACF'); set(gca,'FontSize',14);
    subplot(1,2,2); myErrorBar(tmpPAC,lagX); title('PACF'); set(gca,'FontSize',14);
    if ~isempty(FigureOutFolder)
    saveFigure(['x_ACF_PACF_average' outFileName],FigureOutFolder);
    end

    figure; clear tmpAC tmpPAC;
    for subj=subjN:-1:1  
       tmpAC(subj,:)=mean(ac{subj}.y);   tmpPAC(subj,:)=mean(pac{subj}.y);
       subplot(subjN,2,2*(subj-1)+1); errorbar(1:size(ac{subj}.y,2),mean(ac{subj}.y),std(ac{subj}.y));
       subplot(subjN,2,2*(subj-1)+2); errorbar(1:size(pac{subj}.y,2),mean(pac{subj}.y),std(pac{subj}.y)); 
    end
    subplot(subjN,2,1); title('ACF'); subplot(subjN,2,2); title('PACF'); 
    if ~isempty(FigureOutFolder)
    saveFigure(['y_ACF_PACF_' outFileName],FigureOutFolder);
    end

    figure; 
    subplot(1,2,1); myErrorBar(tmpAC,lagX); title('ACF'); set(gca,'FontSize',14);
    subplot(1,2,2); myErrorBar(tmpPAC,lagX); title('PACF'); set(gca,'FontSize',14);
    if ~isempty(FigureOutFolder)
    saveFigure(['y_ACF_PACF_average' outFileName],FigureOutFolder);
    end

    figure; clear tmpAC tmpPAC;
    for subj=subjN:-1:1  
       tmpAC(subj,:)=mean(ac{subj}.ang);   tmpPAC(subj,:)=mean(pac{subj}.ang);
       subplot(subjN,2,2*(subj-1)+1); errorbar(1:size(ac{subj}.ang,2),mean(ac{subj}.ang),std(ac{subj}.ang));
       subplot(subjN,2,2*(subj-1)+2); errorbar(1:size(pac{subj}.ang,2),mean(pac{subj}.ang),std(pac{subj}.ang)); 
    end
    subplot(subjN,2,1); title('ACF'); subplot(subjN,2,2); title('PACF'); 
    if ~isempty(FigureOutFolder)
    saveFigure(['ang_ACF_PACF_' outFileName],FigureOutFolder);
    end

    figure; 
    subplot(1,2,1); myErrorBar(tmpAC,lagX); title('ACF'); set(gca,'FontSize',14);
    subplot(1,2,2); myErrorBar(tmpPAC,lagX); title('PACF'); set(gca,'FontSize',14);
    if ~isempty(FigureOutFolder)
    saveFigure(['ang_ACF_PACF_average' outFileName],FigureOutFolder);
    end
end

%% velocity and curvature over time
if any(strcmp(modes,'velCurTime')) || any(strcmp(modes,'all'))
    disp('plotting velocity and curvature over time');
    for subj=subjN:-1:1    
          out.curTime.mean(subj,:)=mean(subjDrift{subj}.cur.*(subjDrift{subj}.cur<100));
          out.curTime.std(subj,:)=std(subjDrift{subj}.cur.*(subjDrift{subj}.cur<100));

          out.velTime.xy.mean(subj,:)=mean(subjDrift{subj}.v);
          out.velTime.xy.std(subj,:)=std(subjDrift{subj}.v);

          out.velTime.x.mean(subj,:)=mean(subjDrift{subj}.vx);
          out.velTime.x.std(subj,:)=std(subjDrift{subj}.vx);

          out.velTime.y.mean(subj,:)=mean(subjDrift{subj}.vy);
          out.velTime.y.std(subj,:)=std(subjDrift{subj}.vy);
    end

    figure; 
    subplot(1,2,1); myShadedErrorBar(out.curTime.mean); title('mean');
    subplot(1,2,2); myShadedErrorBar(out.curTime.std); title('SD');
    if ~isempty(FigureOutFolder)
    saveFigure(['curvature_time_' outFileName],FigureOutFolder);
    end
    figure; 
    subplot(3,2,1); myShadedErrorBar(out.velTime.xy.mean);  title('velocity mean');
    subplot(3,2,2); myShadedErrorBar(out.velTime.xy.std);  title('velocity SD');
    subplot(3,2,3); myShadedErrorBar(out.velTime.x.mean);  title('vel x mean');
    subplot(3,2,4); myShadedErrorBar(out.velTime.x.std);  title('vel x SD');
    subplot(3,2,5); myShadedErrorBar(out.velTime.y.mean);  title('vel y mean');
    subplot(3,2,6); myShadedErrorBar(out.velTime.y.std);  title('vel y SD');
    if ~isempty(FigureOutFolder)
    saveFigure(['velocity_time_' outFileName],FigureOutFolder);
    end
end

%% direction over time
lagN=10; colors = parula(lagN);
edges=[-pi/2:pi/60:pi/2]; edgesX=(edges(1:end-1)+edges(2:end))/2; out.angHist_binCenter=edgesX;
if any(strcmp(modes,'dirTime')) || any(strcmp(modes,'all'))
    disp('plotting direction over time');
    figure('position',[10 100 subjN*200 700]); 
    for subj=subjN:-1:1  
          angV=double(subjDrift{subj}.ang(:,2:end));
          for t=size(angV,2):-1:1              
              out.angHist{subj}(:,t)=histcounts(angV(:,t),edges,'Normalization', 'probability')';
          end
          for lag=lagN:-1:1
              angDiff=double(subjDrift{subj}.ang(:,lag+1:end)-subjDrift{subj}.ang(:,1:end-lag));
              out.angDiffHist{subj}(:,lag)=histcounts(angDiff(:),edges,'Normalization', 'probability')';
          end
          
        subplot(3,subjN,subj); hold on;
        for li=1:lagN
            plot(edgesX,out.angDiffHist{subj}(:,li),'lineWidth',1,'color',colors(li,:));    
        end
        xlabel('delta dir'); 
        title(['S' num2str(subj)]);
        set(gca, 'FontSize',14);
        
        subplot(3,subjN,subj+subjN); imagesc(1:lagN,edgesX,out.angDiffHist{subj}); 
        xlabel('time (ms)'); 
        if subj==1
            ylabel('delta dir');
        end
        set(gca, 'FontSize',14);
        
        subplot(3,subjN,subj+2*subjN); imagesc(1:size(angV,2),edgesX,out.angHist{subj}); 
        set(gca, 'FontSize',14);
        xlabel('time (ms)'); 
        if subj==1
            ylabel('dir');
        end
    end
    if ~isempty(FigureOutFolder)
    saveFigure(['angHist_' outFileName],FigureOutFolder);
    end
end

%%
%save([outFolder outFileName '_driftChar.mat'],'out')
end