% TO DO: plot params

function plot_EM_debug(trace2T,EM)
    x=trace2T(1,:);
    y=trace2T(2,:);
    vx=sgfilt(x,3,2*round(20/EM.params.gain)+1,1);
    vy=sgfilt(y,3,2*round(20/EM.params.gain)+1,1);

    vel=sqrt(vx.^2+vy.^2);

        figure('units','normalized','outerposition',[0 0 1 1]);
        ax{1}=axes( 'position', [0.05 0.55 0.4 0.4] );
        hold on; plot(x-mean(x)); plot(y-mean(y));
        legend({'x','y'});
        
        ax{2}=axes( 'position', [0.05 0.05 0.4 0.4] );
        plot(vel);
        xl=xlim;
        line(xl,EM.params.minVel*[1 1],'color','r','lineStyle','--');
        line(xl,EM.params.minPeakVel*[1 1],'color','m','lineStyle','--');
        legend({'speed','min speed','min peak speed'});
        
        ax{3}=axes( 'position', [0.5 0.05 0.475 0.4] );
        trace_segment_plot(trace2T,EM,[],1,0);
        linkaxes([ax{1} ax{2} ax{3}],'x');
        
        
        ax{4}=axes( 'position', [0.55 0.75 0.2 0.2] );
        hold on; title('max speed');
        legendName=[];
        if ~isempty(EM.drifts) histogram([EM.drifts(:).maxVel],50); legendName=[legendName {'drift'}]; end
        if ~isempty(EM.smooth) histogram([EM.smooth(:).maxVel],50); legendName=[legendName {'smooth'}];end
        if ~isempty(EM.mSaccs) histogram([EM.mSaccs(:).peakVel],50); legendName=[legendName {'msacc'}];end
        legend(legendName); 
        
        ax{5}=axes( 'position', [0.775 0.75 0.2 0.2] );
        hold on; title('mean speed');
        legendName=[];
        if ~isempty(EM.drifts) histogram([EM.drifts(:).meanVel],50); legendName=[legendName {'drift'}]; end
        if ~isempty(EM.smooth) histogram([EM.smooth(:).meanVel],50); legendName=[legendName {'smooth'}]; end
        legend(legendName); 
        
%         ax{6}=axes( 'position', [0.55 0.5 0.2 0.2] );
%         hold on; title('mean speed 0');
%         legendName=[];
%         if ~isempty(EM.drifts) histogram([EM.drifts(:).meanVel0],50); legendName=[legendName {'drift'}]; end
%         if ~isempty(EM.smooth) histogram([EM.smooth(:).meanVel0],50); legendName=[legendName {'smooth'}]; end
%         legend(legendName); 
%         
%         ax{7}=axes( 'position', [0.775 0.5 0.2 0.2]);
%         hold on; title('mean speed ratio');
%         legendName=[];
%         if ~isempty(EM.drifts) histogram([EM.drifts(:).meanVel0]./[EM.drifts(:).meanVel],50); legendName=[legendName {'drift'}]; end
%         if ~isempty(EM.smooth) histogram([EM.smooth(:).meanVel0]./[EM.smooth(:).meanVel],50); legendName=[legendName {'smooth'}]; end
%         legend(legendName); 
        
        ax{6}=axes( 'position', [0.55 0.5 0.2 0.2] );
        hold on; title('saccade score dur');
        legendName=[];
        if ~isempty(EM.smooth) histogram([EM.smooth(:).saccScore_dur],50); legendName=[legendName {'smooth'}]; end
        if ~isempty(EM.mSaccs) histogram([EM.mSaccs(:).saccScore_dur],50); legendName=[legendName {'msacc'}]; end
        if ~isempty(EM.saccs) histogram([EM.saccs(:).saccScore_dur],50); legendName=[legendName {'sacc'}]; end
        legend(legendName); 
        
        ax{7}=axes( 'position', [0.775 0.5 0.2 0.2] );
        hold on; title('saccade score peakVel');
        legendName=[];
        if ~isempty(EM.smooth) histogram([EM.smooth(:).saccScore_peakVel],50); legendName=[legendName {'smooth'}]; end
        if ~isempty(EM.mSaccs) histogram([EM.mSaccs(:).saccScore_peakVel],50); legendName=[legendName {'mSaccs'}]; end
        if ~isempty(EM.saccs) histogram([EM.saccs(:).saccScore_peakVel],50); legendName=[legendName {'saccs'}]; end
        legend(legendName); 
end