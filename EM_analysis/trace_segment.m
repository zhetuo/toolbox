%x,y are eye trace arcmin at ms
function EM=trace_segment(trace2T,Fs,manual)
    gain=double(1e3/Fs);
    x=trace2T(1,:);
    y=trace2T(2,:);
    vx=sgfilt(x,3,2*round(20/gain)+1,1);
    vy=sgfilt(y,3,2*round(20/gain)+1,1);

    vel=sqrt(vx.^2+vy.^2);

    [~,~,range]=removeOutlier(vel,4);
%     minVel=0.24*gain;  % arcmin/ms       minimum velocity of eye movements
%     minPeakVel=1*gain;  % arcmin/ms    minimum velocity of saccade and blink
    minVel=range(2);  % arcmin/ms       minimum velocity of eye movements
    minMeanVel=minVel/3;
    minPeakVel=prctile(vel,88);
    
    
    minGap=round(20/gain);  %  20 ms               combine near eye movements
    saccMinAmp=30; % arcmin        saccade minimum amplitude
    msaccMinAmp=5; % arcmin       microsaccade minimum amplitude
    msaccMaxAmp=60; % arcmin       microsaccade maximum amplitude
    
    smooth=[];
    drifts=[];
    events=[];
    blinks=[];
    saccs=[];
    mSaccs=[];
    
    goodChar='N';
 while strcmpi(goodChar,'n')
   
    velFlag=vel>minVel;
    velDelta=velFlag(2:end)-velFlag(1:end-1);


    ps=find(velDelta==1)+1;
    et=1;
    for i=1:length(ps)
        st=ps(i);

        if st<=et
            continue
        end

     
        if st>et+minGap
            meanVel0=sqrt(mean(vx(et:st))^2+mean(vy(et:st))^2);
            meanVel1=mean(vel(et:st));
            if meanVel0/meanVel1<0.9%mean(vel(et:st))<minMeanVel
                clear tmp;
                tmp.startTime=et;
                tmp.endTime=st;
                tmp.duration=(st-et)/Fs*1E3;  % ms
                tmp.x=x(et:st);
                tmp.y=y(et:st);
                drifts=[drifts tmp];
            else
                clear tmp;
                tmp.startTime=et;
                tmp.endTime=st;
                tmp.duration=(st-et)/Fs*1E3;  % ms
                tmp.x=x(et:st);
                tmp.y=y(et:st);
                tmp.saccScore_peakVel=NaN;
                tmp.saccScore_dur=NaN;
                smooth=[smooth tmp];
            end
        end

        dur=find(velDelta(st:end)==-1,1); %skip the event that is shorter than minGap
        if isempty(dur)
            continue;
        else
            et=dur+st;
        end
        
%         %% combine adjacent events if the temporal gap between them is shorter than minGap
%         dur=find(velDelta(et:end)==1,1);
%         if ~isempty(dur)
%             st2=dur+et;
%             while (abs(st2-et)<minGap)
%                 et2=find(velDelta(st2:end)==-1,1)+st2;
%                 et=et2;
%                 st2=find(velDelta(et:end)==1,1)+et;
%             end
%         end
        
        tmpEvent.startTime=st;
        tmpEvent.endTime=et;
        events=[events tmpEvent];
        
        if isempty(et)
            continue;
        end

        %% st and et ready
        vxMax=max(vx(st:et)); vxMin=min(vx(st:et));
        vyMax=max(vy(st:et)); vyMin=min(vy(st:et));
        
        if (vxMax>minPeakVel && vxMin<-minPeakVel) && (vyMax>minPeakVel && vyMin<-minPeakVel)  %blink
            tmpBlink.startTime=st;
            tmpBlink.endTime=et;

            blinks=[blinks tmpBlink];

        else
            deltaX=x(et)-x(st);
            deltaY=y(et)-y(st);
            dist=sqrt(deltaX^2+deltaY^2);
            peakVel=max(vel(st:et));
            dur=et-st;
            
            [saccBool , saccScore]=detectSacc(dist/60,dur/Fs,peakVel/60*Fs,2.5);
            if ~saccBool
                clear tmp;
                tmp.startTime=st;
                tmp.endTime=et;
                tmp.duration=(et-st)/Fs*1E3;  % ms
                tmp.x=x(st:et);
                tmp.y=y(st:et);
                tmp.saccScore_peakVel=saccScore.peakVel;
                tmp.saccScore_dur=saccScore.dur;
                smooth=[smooth tmp];
            else

                if (dist>30) && any(vel(st:et)>minPeakVel)% saccade 
                    tmpSacc.startTime=st;
                    tmpSacc.endTime=et;
                    tmpSacc.duration=(et-st)/Fs*1E3;  % ms
                    tmpSacc.size=dist;
                    tmpSacc.x=x(st:et);
                    tmpSacc.y=y(st:et);
                    tmpSacc.peakVel=max(vel(st:et));
                    tmpSacc.amplitude=sqrt((y(et)-y(st))^2+(x(et)-x(st))^2);
                    tmpSacc.saccScore_peakVel=saccScore.peakVel;
                    tmpSacc.saccScore_dur=saccScore.dur;
                    saccs=[saccs tmpSacc];

                else   
                    if (dist>0.5) && (dist<60) && any(vel(st:et)>minPeakVel)% microsaccade
                        tmpMsacc.startTime=st;
                        tmpMsacc.endTime=et;
                        tmpMsacc.duration=(et-st)/Fs*1E3;  % ms
                        tmpMsacc.size=dist;
                        tmpMsacc.x=x(st:et);
                        tmpMsacc.y=y(st:et);
                        tmpMsacc.peakVel=max(vel(st:et));
                        tmpMsacc.amplitude=sqrt((y(et)-y(st))^2+(x(et)-x(st))^2);
                        tmpMsacc.saccScore_peakVel=saccScore.peakVel;
                        tmpMsacc.saccScore_dur=saccScore.dur;
                        mSaccs=[mSaccs tmpMsacc];
                    end
                end
            end
            
        end
    end

    if length(vel)-et>minGap
        st=length(vel);
        meanVel0=sqrt(mean(vx(et:st))^2+mean(vy(et:st))^2);
        meanVel1=mean(vel(et:st));
        if meanVel0/meanVel1<0.9%mean(vel(et:st))<minMeanVel
            clear tmp;
            tmp.startTime=et;
            tmp.endTime=st;
            tmp.duration=(st-et)/Fs*1E3;  % ms
            tmp.x=x(et:st);
            tmp.y=y(et:st);
            drifts=[drifts tmp];
        else
            clear tmp;
            tmp.startTime=et;
            tmp.endTime=st;
            tmp.duration=(st-et)/Fs*1E3;  % ms
            tmp.x=x(et:st);
            tmp.y=y(et:st);
            tmp.saccScore_peakVel=saccScore.peakVel;
            tmp.saccScore_dur=saccScore.dur;
            smooth=[smooth tmp];
        end
    end
    
    drifts2(1)=drifts(1); di2=1;
    for di=2:length(drifts)
       
        if (drifts(di).startTime-drifts2(di2).endTime)<minGap && any(vel(drifts2(di2).endTime:drifts(di).startTime)<minPeakVel)
            drifts2(di2).endTime=drifts(di).endTime;
            drifts2(di2).duration= (drifts2(di2).endTime-drifts2(di2).startTime)/Fs*1E3; % ms
            drifts2(di2).x=x(drifts2(di2).startTime:drifts2(di2).endTime);
            drifts2(di2).y=y(drifts2(di2).startTime:drifts2(di2).endTime);
        else
            di2=di2+1;
            drifts2(di2)=drifts(di);
        end
    end
    drifts2([drifts2(:).duration]<50)=[];
     for di=1:length(drifts2)
        st=drifts2(di).startTime; et=drifts2(di).endTime; 
        [span, mn_speed, mn_cur, varx, vary, smx, smy] = ...
        getDriftChar(x(st:et), y(st:et), 2*round(10/gain)+1, round(10/gain), inf, Fs);
    
        drifts2(di).minVel=min(vel(st:et)/60*Fs);
        drifts2(di).maxVel=max(vel(st:et)/60*Fs);
        drifts2(di).span = span;
        drifts2(di).meanVel = mean(vel(st:et)/60*Fs);
        drifts2(di).meanVel0 = sqrt(mean(vx(st:et)/60*Fs)^2+mean(vy(st:et)/60*Fs)^2);
        drifts2(di).meanCur = mn_cur;
        drifts2(di).varx = varx;
        drifts2(di).vary = vary;
     end
    
    
    smooth2(1)=smooth(1); di2=1;
    for di=2:length(smooth)
       
        if (smooth(di).startTime-smooth2(di2).endTime)<minGap &&  any(vel(smooth2(di2).endTime:smooth(di).startTime)<minPeakVel)
            smooth2(di2).endTime=smooth(di).endTime;
            smooth2(di2).duration= (smooth2(di2).endTime-smooth2(di2).startTime)/Fs*1E3; % ms
            smooth2(di2).x=x(smooth2(di2).startTime:smooth2(di2).endTime);
            smooth2(di2).y=y(smooth2(di2).startTime:smooth2(di2).endTime);
            smooth2(di2).saccScore_peakVel=min([smooth2(di2).saccScore_peakVel smooth(di).saccScore_peakVel]);
            smooth2(di2).saccScore_dur=min([smooth2(di2).saccScore_dur smooth(di).saccScore_dur]);
        else
            di2=di2+1;
            smooth2(di2)=smooth(di);
        end
    end
    
     for di=1:length(smooth2)
        st=smooth2(di).startTime; et=smooth2(di).endTime; 
        smooth2(di).minVel=min(vel(st:et)/60*Fs);
        smooth2(di).maxVel=max(vel(st:et)/60*Fs);
        smooth2(di).meanVel = mean(vel(st:et)/60*Fs);
        smooth2(di).meanVel0 = sqrt(mean(vx(st:et)/60*Fs)^2+mean(vy(st:et)/60*Fs)^2);
     end
    
    EM.params.minVel=minVel;
    EM.params.minMeanVel=minMeanVel;
    EM.params.minPeakVel=minPeakVel;
    EM.params.minGap=minGap;  
    EM.params.saccMinAmp=saccMinAmp;
    EM.params.msaccMinAmp=msaccMinAmp; 
    EM.params.msaccMaxAmp=msaccMaxAmp; 
    EM.params.gain=gain;
    EM.params.Fs=Fs;
    
    EM.events=events;
    EM.drifts=drifts2;
    EM.blinks=blinks;
    EM.saccs=saccs;
    EM.mSaccs=mSaccs;
    EM.smooth=smooth2;
    EM.eventStart=find(velDelta==1)+1;
    EM.eventEnd=find(velDelta==-1)+1;
    
    
     if manual

       
        plot_EM_debug(trace2T,EM);

        goodChar=input('Is the current threshold good? Y/N ','s');
        if strcmpi(goodChar,'n')
            sprintf('Current thresholds, minVel=%.2f, minPeakVel=%.2f, minMeanVel=%.2f \n', minVel,minPeakVel,minMeanVel);
            clear tmp;
            tmp=input('Please input the new threshold [minVel minPeakVel minMeanVel] ');
            minVel=tmp(1); minPeakVel=tmp(2); minMeanVel=tmp(3);
        end
     else
         goodChar='Y';
     end
end

end


