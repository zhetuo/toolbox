% Input: xRaw, yRaw: raw eye trajectory of the trial
%        Dur: time interval we care about
%        thre: velocity threshold for drift (arcmin/ms)
%        Fs: sampling frequency

function out=drift_traj_process(xRaw,yRaw,Dur,thre,driftCell,Fs)
    xRaw=double(xRaw); yRaw=double(yRaw);
    
    x=sgfilt(xRaw,3,11,0); y=sgfilt(yRaw,3,11,0);  
    x41=sgfilt(xRaw,3,41,0); y41=sgfilt(yRaw,3,41,0);
    vx=sgfilt(xRaw,3,41,1); vy=sgfilt(yRaw,3,41,1); v=sqrt(vx.^2+vy.^2);

    if length(v)>=Dur(end) && sum(v(Dur)>thre)==0 
        dx=gradient(x); dy=gradient(y); ddx=gradient(dx); ddy=gradient(dy);
        cur=abs(dx.*ddy-dy.*ddx)./((dx.^2+dy.^2).^(3/2));

        ang=double(atan2(dy,dx));

        if exist('Fs','var')
            sampling=Fs;
        else
            sampling=1000;
        end
        [~,~,D] = CalculateDiffusionCoef(sampling, struct('x',x(Dur), 'y', y(Dur)));
        if isempty(driftCell)
            out.xr=xRaw(Dur); out.yr=yRaw(Dur);
            out.x=x(Dur); out.y=y(Dur);
            out.x41=x41(Dur); out.y41=y41(Dur);
            out.vx=vx(Dur); out.vy=vy(Dur); out.v=v(Dur);
            out.cur=cur(Dur); out.ang=ang(Dur);
            out.Dcoef=D;
        else
            out.xr=[driftCell.xr; xRaw(Dur)]; out.yr=[driftCell.yr; yRaw(Dur)];
            out.x=[driftCell.x; x(Dur)]; out.y=[driftCell.y; y(Dur)];
            out.x41=[driftCell.x41; x41(Dur)]; out.y41=[driftCell.y41; y41(Dur)];
            out.vx=[driftCell.vx; vx(Dur)]; out.vy=[driftCell.vy; vy(Dur)]; out.v=[driftCell.v; v(Dur)];
            out.cur=[driftCell.cur; cur(Dur)];
            out.ang=[driftCell.ang; ang(Dur)];
            out.Dcoef=[driftCell.Dcoef D];
        end
    else
        out=driftCell;
    end
end



