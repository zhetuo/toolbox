function [onset,offset]=sacc_OnOffRefine(saccTraj,velTh)
    
    minGap=10;
    
    coef=pca(double(saccTraj'));
    in=coef'*saccTraj;
    saccX=in(1,:);  saccY=in(2,:);
    
   
    velX=diff(saccX);
    
    if ~exist('velTh','var')
       velTh=prctile(abs(velX),(1-70/size(saccTraj,2))*100); 
    end
    
    
    velBool=abs(velX)>velTh;
    velBoolRise=find(diff(velBool)==1);
    velBoolfall=find(diff(velBool)==-1);
    
    if length(velBoolRise)~=length(velBoolfall)
        if length(velBoolRise)>length(velBoolfall)
            velBoolRise(end)=[];
        end
        
        if length(velBoolRise)<length(velBoolfall)
            velBoolfall(1)=[];
        end
    end
    
        if velBoolfall(1)-velBoolRise(1)<0
            velBoolRise(end)=[]; velBoolfall(1)=[];
        end
            idx=find(velBoolRise(2:end)-velBoolfall(1:end-1)<minGap);
            velBoolRise(idx+1)=[];
            velBoolfall(idx)=[];
            
            [~,maxIdx]=max(velBoolfall-velBoolRise);
            onset=velBoolRise(maxIdx);
            offset=velBoolfall(maxIdx);
     
    
    
    if 1
        figure('position',[100 100 300 500]); 
        subplot(2,1,1); hold on;
        plot(saccTraj'); line([onset onset],ylim,'color','r'); line([offset offset],ylim,'color','k');
        subplot(2,1,2); hold on;
        plot(abs(velX)); line([onset onset],ylim,'color','r'); line([offset offset],ylim,'color','k');
        keyboard;
    end
   