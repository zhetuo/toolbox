function [coor_record]=generate_drifting_trajectory(driftLength,sigM)

coor_record=zeros(2,driftLength);

mu=[0,0];

drift_delta=mvnrnd(mu,sigM,driftLength)';

coor_record(:,1)=[0;0];
for i=2:driftLength
    coor_record(:,i)=coor_record(:,i-1)+drift_delta(:,i);
end  


% xVector=coor_record(1,:);
% yVector=coor_record(2,:);
% 
%  figure
%  hold
%  for frameIndex=2:driftLength
%       plot([xVector(frameIndex-1) xVector(frameIndex)],[yVector(frameIndex-1) yVector(frameIndex)],'-*');
%  end
%  hold off
 
 
end