function [span, mn_speed, mn_cur, varx, vary, smx, smy] = getDriftChar(x, y, smoothing, cutseg, maxSpeed,Fs)
%getDriftChar comptues drift characteristics on a single drift segment. 
% Inputs:
%   x, y: x and y traces of single drift segment
%   smoothing: window size for s-golay filter (must be odd integer)
%   cutseg: number of indices to remove from start and end of trace
%   maxSpeed: upper limit on speed to consider - good flag for missed
%       microsaccades
% Outputs:
%   span: radius of smallest circle that completely encompasses drift
%   mn_speed: mean speed of this drift segment
%   mn_cur: mean curvature of this drift segment
%   varx, vary: horizontal and vertical variances
%   smx, smy: filtered x and y traces


% compute drift span
mx = mean(x(cutseg:end-cutseg));
my = mean(y(cutseg:end-cutseg));
span = max(sqrt((x - mx).^2 + (y - my).^2));

% s-golay filtering
smx = sgfilt(x, 3, smoothing, 0);
smy = sgfilt(y, 3, smoothing, 0);
smx1 = sgfilt(x, 3, smoothing, 1);
smy1 = sgfilt(y, 3, smoothing, 1);

% compute drift speed
sp = Fs*sqrt(smx1(cutseg:end-cutseg).^2 + smy1(cutseg:end-cutseg).^2);

if any(sp > maxSpeed)
    mn_speed = nan;
else
    mn_speed = mean(sp);
end


% compute drift curvature
tmpCur = abs(curvature(smx(cutseg:end-cutseg), smy(cutseg:end-cutseg)));
mn_cur = mean(tmpCur);

varx = var(x(cutseg:end-cutseg));
vary = var(y(cutseg:end-cutseg));

smx = smx(cutseg:end-cutseg);
smy = smy(cutseg:end-cutseg);
end