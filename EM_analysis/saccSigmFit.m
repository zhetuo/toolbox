function [out,Err]=saccSigmFit(saccTraj,velTh)
    if ~exist('velTh','var')
       velTh=1.5; 
    end
    
    
    coef=pca(double(saccTraj'));
    in=coef'*saccTraj;
    saccX=in(1,:);  saccY=in(2,:);
    
    
    if length(velTh)>1
        saccOn=velTh(1); saccOff=velTh(2);
    else
        velX=diff(saccX);
        saccOn=find(abs(velX)>velTh,1,'first');
        saccOff=find(abs(velX)>velTh,1,'last')+10;
    end
    
    
    saccAmp=double(saccX(saccOff)-saccX(saccOn));
    
    
    saccIn=saccX(saccOn:saccOff);
    costFun=@(param)saccSigmPredict(param,saccIn);
    for iter=10:-1:1
        paramM(iter,:)=fmincon(costFun, [saccAmp 0.3+0.2*(rand()-0.5) 25+10*(rand()-0.5)],...
            [],[],[],[],...
            [saccAmp-60 0.01 1],[saccAmp+60 5 length(saccIn)],[],optimoptions('fmincon','Display','off'));

%         paramM(iter,:)=fmincon(costFun, [saccAmp 0.2 25],...
%             [],[],[],[],...
%             [saccAmp-60 0.01 1],[saccAmp+60 5 length(saccIn)],[],optimoptions('fmincon','Display','off'));

        costV(iter)=saccSigmPredict(paramM(iter,:),saccIn);
    end
    
    [~,minIdx]=min(costV);
    param=paramM(minIdx,:);
    [Err,saccPredict]=saccSigmPredict(param,saccIn);
    saccX(saccOn:saccOff)=saccPredict;

    
    out=coef*[saccX; saccY];
    
    if 0
        figure('position',[100 100 800 300]); 
        subplot(1,3,1);  plot([saccIn; saccPredict]');
        subplot(1,3,2);  plot([diff(saccIn); diff(saccPredict)]');
        subplot(1,3,3);  hold on; plot(saccTraj(:,saccOn-20:saccOff+20)'); plot(out(:,saccOn-20:saccOff+20)');
        keyboard;
    end
    
end




function [cost,modelTrace]=saccSigmPredict(param,saccTrace)
    
    modelTrace=(param(1) ./ (1 + exp(-param(2) * ([1:length(saccTrace)]-param(3))))+saccTrace(1));
    velErr=norm(diff(saccTrace)-diff(modelTrace))/length(saccTrace);
    traceErr=norm(saccTrace-modelTrace)/length(saccTrace);
    endErr=norm(saccTrace(end)-modelTrace(end))+norm(saccTrace(1)-modelTrace(1));
    endGradient=norm((saccTrace(end)-saccTrace(end-1))-(modelTrace(end)-modelTrace(end-1)));
    cost=double(5*traceErr+5*velErr+endErr+endGradient);
end
