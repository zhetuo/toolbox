% durCell: {startVar} {startIdx} {endVar} {endIdx} {margin}

function driftData=drift_filter(subjData,durCell,thre,driftDur)

    if nargin<3
        thre=3*60/1000; %arcmin/ms
        driftDur=100;
    end
    
    margin=durCell{5};
    
    for subjIdx=1:length(subjData)
        disp([num2str(subjIdx) '/' num2str(length(subjData))])
        trials=subjData{subjIdx};
        driftData{subjIdx}=[]; 

        for trialIndex=1:length(trials)
            
            trial=trials{trialIndex};
            
            if isfield(trial.x,'position')
                xRaw=trial.x.position; yRaw=trial.y.position;
            else
                xRaw=trial.x; yRaw=trial.y;
            end
            
            if length(xRaw)<driftDur+2*margin
                continue;
            end
            vx=sgfilt(xRaw,3,41,1); vy=sgfilt(yRaw,3,41,1); v=sqrt(vx.^2+vy.^2);

            if isempty(durCell{1})
                t_start=durCell{2}+1;
            else
                t_start=eval(['trial.' durCell{1} '+' num2str(durCell{2})]);
            end

            if strcmp(durCell{4},'end')
                t_end=length(xRaw);
            else
                if isempty(durCell{3})
                    t_end=durCell{4};
                else
                    t_end=eval(['trial.' durCell{3} '+' num2str(durCell{4})]);
                end
            end


            totDur=t_start:t_end;

            driftEdge=[1 find(abs(diff(v(totDur)<thre))) length(totDur)];
            
            [dmaxV,diV]=sort(diff(driftEdge),'descend');

            for i=1:length(dmaxV)
                if dmaxV(i)>driftDur+2*margin 
                    dt=t_start+round((driftEdge(diV(i))+driftEdge(diV(i)+1))/2);
                    fixDur=dt-driftDur/2:dt+driftDur/2-1;

                    if sum(v(fixDur)>thre)==0 
                        driftData{subjIdx}=drift_traj_process(xRaw,yRaw,fixDur,thre,driftData{subjIdx});
                    end
                else
                    break;
                end
            end
        end
      
    end
    disp('Done!')