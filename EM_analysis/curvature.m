function cur=curvature(x,y)

    dx=gradient(x); dy=gradient(y); ddx=gradient(dx); ddy=gradient(dy);
        cur=abs(dx.*ddy-dy.*ddx)./((dx.^2+dy.^2).^(3/2));
end