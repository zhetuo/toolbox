% This function calculates the are covered by the 2D displacement distribution
% at each delta t.
%
% function [Area] = DiffusionCoefficient2D (Fix,varargin)
%
% INPUT:
% Fix   : matrix with the eye movements data. This matrix should be
%         organized in the the following way:
%         Fix{sbj} is an array containing each fixation as a structure.
%         A given drift's horizontal and vertical angles (in arcmin)
%         can be accessed via Fix{sbj}(fx).x and Fix{sbj}(fx).x. where fx
%         is the fixation index
%
% Properties: Optional properties
%             'MaxTime'       : Maximum temporal interval
%                               (default: 256 ms)
%             'SpanLimit'     : Maximum span of an event to be considered
%                               events with a higher span will be discarded
%                               (default: 60 arcmin)
%             'FigKey'        : Set FigKey at one to generate a figure
%                               (default: 0)
%             'TimeInterval'  : Temporal segment of the event considered
%                               if it is set at 1 all the event segment is selected
%                               otherwise a 2x1 vector will indicate the temporal
%                               start and end of the segment
%                              (default: [1])
%             'GenerateMovie' : Set GenerateMovie at 1 to generate a movie of
%                               the evolution of the event displacement at
%                               different delta t
%                               (default: 0)
%             'MovieName'     : string used to name the title of the movie
%                               (default: 'test')
%             'FigName'       : string used to save the figures
%                               (default: 'test')
%             'Fsampling'     : sampling frequency (default 1000 Hz)
%             'RegressionExclusionLag' : in ms start regressing from lags
%                               larger than the RegressionExclusionLag
%                               mostly to avoid noise from 1-50 ms lags
%                               (default 50 ms)
%
% OUTPUT: 
% DCoef_DeTrended  : Diffusionn coefficient calculated with the detrended PCA method (arcmin^s per sec)
% Bias             : x and y coordinates of the bias at each temporal lag (time) (based on the PCA %                    method) in arcmin (total bias is given by sqrt(Bias(1,end)^2+Bias(2,end)^2))
% DCoef_Dsq        : Diffusionn coefficient non-detrended (includes the bias)
% RegLineDsqDetrended : Displacement squared (de-trended)(arcmin^2) at each delta t
% RegLineDsq       : Displacement squared(arcmin^2) at each delta t
% Time           : The temporal bins over which the area has been calculated (delta t).
% NFix           : Number of events considered for each delta t. AMC: the
%                   number of trials that are going into each segmentl; particulary important
%                   if different length drift segments are being analyzed
% SingleSegmentDsq  : distribution of each single Displacement squared per each drift segment %                    (not-detrended)   
% bint : returns a matrix bint of 95% confidence intervals for the
%           coefficient estimates. %%%%AMC ADDITION
%
% EXAMPLE USAGE: [DCoef_DeTrended Bias DCoef_Dsq RegLineDsqDetrended RegLineDsq SingleSegmentDsq Time NFix] = Calculate2DArea(fix,...
%    'FigKey', 1, 'TimeInterval', 1, 'GenerateMovie', 1, ...
%    'MovieName', 'exmpleMovie', ...
%    'FigName', 'exampleFig');

% FUNCTION NEEDED TO GENERATE MOVIES AND FIGURES:
% histogram2.m
% error_ellipse.m

% HISTORY:
% This function is based on the DiffusionCoefficients_DPI.m function by
% Murat, which is based on Xutao diffusion analysis function
%
% 2013 @APLAB

function [DCoef_DeTrended, Bias, DCoef_Dsq, DsqDetrended, Dsq, ...
    SingleSegmentDsq, Time, NFix, RegLineDsq, bint] =  CalculateDiffusionCoef (FSAMPLING,Fix,varargin)


% set global variables
global MINSTEP LIM FIGKEY  TIMEINTERVAL LIMIT GENERATEMOVIE MOVIENAME FIGNAME

% set default parameters
NT = 256;
% NT = 100;
FIGKEY = 0;
TIMEINTERVAL = [1]; %ms
LIMIT = 5; % arcmin (used for the movie boundaries)
GENERATEMOVIE = 0;
MOVIENAME = 'test';
FIGNAME = 'test';
DIMENSIONS = 2;
RegressionExclusionLag = 50;

% Interpret the user parameters
k = 1;
Properties = varargin;
while k <= length(Properties) && ischar(Properties{k})
    switch (Properties{k})
        case 'RegressionExclusionLag'
            RegressionExclusionLag =  Properties{k + 1};
            Properties(k:k+1) = [];
	    case 'Fsampling'
		    FSAMPLING = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'MaxSpace'
            NX = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'MaxTime'
            NT = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'FigKey'
            FIGKEY = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'TimeInterval'
            TIMEINTERVAL = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'GenerateMovie'
            GENERATEMOVIE = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'MovieName'
            MOVIENAME = Properties{k + 1};
            Properties(k:k+1) = [];
        case 'FigName'
            FIGNAME = Properties{k + 1};
            Properties(k:k+1) = [];
        otherwise
            k = k + 1;
    end
end


if length(TIMEINTERVAL)==2
    if (TIMEINTERVAL(2)-TIMEINTERVAL(1)-1)<NT 
%         warning(sprintf('Maximum Time %.0f ms is larger than the Time Interval considered [%.0f-%.0f]',...
%             NT, TIMEINTERVAL(1),TIMEINTERVAL(2)));
        NT = TIMEINTERVAL(2)-TIMEINTERVAL(1)-1;
%         warning(sprintf('Maximum time has been reset at %.0f' , NT));
    end
end



% minimum curvature step used to bin the data (curvature values ranges from
% 0 to 1.
MINSTEP = .05; % arcmin
% limit boundaries used for the 2D hist of the probability displacement
LIM = 15; % arcmin

Time = linspace(1,NT-1,NT-1)./FSAMPLING;


[DsqDetrended, Bias, Dsq, SingleSegmentDsq, NFix]  = probEyeMov (Fix, Time);
% start regressing from lag 50 (previous lags are affected by noise)
id = find(Time>(RegressionExclusionLag/FSAMPLING));
% regress the area and find the regression coefficients
for pp = 1:size(SingleSegmentDsq,1)
    RegCoef = regress(SingleSegmentDsq(pp,(id))',[Time(id)' ]);
    SingleSegmentDsq(pp) = (RegCoef(1)/(2*DIMENSIONS)); 
end

RegLineDsqDetrended = regress(DsqDetrended(id)',[Time(id)'  ]);
[RegLineDsq, bint] = regress(Dsq(id)',[Time(id)' ]); %%This will show how well the regression fits **DOUBLE CHECK

% rate of increase per sec
DCoef_Dsq = (RegLineDsq(1)/(2*DIMENSIONS));
DCoef_DeTrended = (RegLineDsqDetrended(1)/(2*DIMENSIONS));

if FIGKEY == 1
    figure
    plot(Time, Area, 'b', 'LineWidth', 2)
    hold on
    plot(Time, Time*RegCoef(1)+RegCoef(2), '--b')
    set(gca, 'FontSize', 10)
    xlabel('Time (ms)')
    ylabel('Area (arcmin^2)')
    text(10,25, sprintf('Regression coeff: %.3f (arcmin^2/s)', RegCoef(1)), 'FontSize', 10);
    ylim([0  30]);
    if length(TIMEINTERVAL)==1
        %             title(sprintf('NX: %.0f arcmin, NT: %.0f ms, TIMEINTERVAL: all period SPANLMT: %.0f arcmin', NX, NT, SPANLMT))
    else
        %             title(sprintf('NX: %.0f arcmin, NT: %.0f ms, TIMEINTERVAL: %.0f-%.0f ms SPANLMT: %.0f arcmin', NX, NT,...
        %                 TIMEINTERVAL(1),TIMEINTERVAL(2), SPANLMT))
    end
    
end


%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function  [DsqDetrended Bias Dsq SingleSegmentDsq NFix] = probEyeMov (fixs, time)

global MINSTEP LIM NX NT NY FIGKEY SPANLMT TIMEINTERVAL FSAMPLING LIMIT GENERATEMOVIE MOVIENAME FIGNAME

CTot = [];

Bias = [];
SingleSegmentDsq = [];

if GENERATEMOVIE
    figure
end
Pvel = [];
DriftIdx = [];
TrialIdx = [];
NFix = zeros(1,length(time));
% loop through all delta t
for dt = 1:length(time);
    d_x= [];% horizontal displacements
    d_y = []; % vertical displacements
    dd2 = []; % radial displacements
    d_tot = [];
    cont = 0;
    CTot = [];
    % loop through all events
    for fx = 1:length(fixs)
        
        if length(TIMEINTERVAL)==2
            hor = fixs(fx).x(TIMEINTERVAL(1):TIMEINTERVAL(2));
            ver = fixs(fx).y(TIMEINTERVAL(1):TIMEINTERVAL(2));
        else
            hor = fixs(fx).x(TIMEINTERVAL(1):end);
            ver = fixs(fx).y(TIMEINTERVAL(1):end);
        end
        
       
        % pick all the possible couples of values based on nT and the
        % maximum time interval
        enD = length(hor)-dt;
        % keep count of the total number of fixations selected per dt
        if enD>0
            NFix(dt) = NFix(dt)+1;
        end
        d_x = [ d_x (hor(1+dt:enD+dt)-hor(1:enD)) ];
        d_y = [ d_y (ver(1+dt:enD+dt)-ver(1:enD)) ];
        dd2 = [ dd2 (hor(1+dt:enD+dt)-hor(1:enD)).^2 + (ver(1+dt:enD+dt)-ver(1:enD)).^2 ];
        SingleSegmentDsq(fx, dt) = mean((hor(1+dt:enD+dt)-hor(1:enD)).^2 + (ver(1+dt:enD+dt)-ver(1:enD)).^2);
    end
    Dsq(dt) = mean(dd2);
	% calculate the displacement sq based on the PCA (de-trended)
	mat = [d_x - mean(d_x); ...
          d_y - mean(d_y)];    
    sigmas = eig(mat*mat')./length(d_x);;
    DsqDetrended(dt) = sum(sigmas);
	
    Bias(dt,1:2) = [mean(d_x) mean(d_y)];
    
    if GENERATEMOVIE
        aviobj = VideoWriter(sprintf('%s.avi',MOVIENAME ), 'Uncompressed AVI');%, 'compression', 'none', 'fps', 30, 'quality', 100);
        aviobj.FrameRate = 25;
        vid.Quality = 100;
        open(aviobj);
        n = 100;
        %plotting the 2D histogram for each delta t
        out = histogram2(d_x,d_y,[-LIMIT,LIMIT,n; -LIMIT,LIMIT,n]);
        h = fspecial('average', 5);
        out = filter2(h, out);
        out = out/max(max(out));
        pcolor(linspace(-LIMIT,LIMIT,n),linspace(-LIMIT,LIMIT,n), out')
        shading interp
        C = cov(d_x,d_y);
        hold on
        error_ellipse(C, [mean(d_x); mean(d_y)],'conf', .95, 'style', 'w');
        hold on
        plot([-LIMIT LIMIT], [0 0], '--w')
        plot([0 0],[-LIMIT LIMIT], '--w')
        axis([-LIMIT LIMIT -LIMIT LIMIT])
        axis square
        xlabel('horizontal displacement (arcmin)')
        ylabel('vertical displacement (arcmin)')
        title(sprintf('%s dt: %.2f ms cov: %.2f Area: %.2f arcmin^2',MOVIENAME, dt, C(1,2), Dsq(dt)))
        set(gca, 'FontSize', 12)
        set(gcf,'renderer','zbuffer')
        colormap hot
        colorbar
        caxis([0 1])
        drawnow
        currFrame(dt) = getframe(gcf);
        writeVideo(aviobj,currFrame);
        if dt < NT-1
            cla
        end
        
    end
end
th = cart2pol(d_x,d_y);
std_th = std(th);



if GENERATEMOVIE
    close(aviobj);
    close all
end
if FIGKEY
    figure
    % 2d graph for the last delta t
    n = 100;
    %plotting the 2D histogram for each delta t
    out = histogram2(d_x,d_y,[-LIMIT,LIMIT,n; -LIMIT,LIMIT,n]);
    %h = fspecial('average', 5);
    %out = filter2(h, out);
    pcolor(linspace(-LIMIT,LIMIT,n),linspace(-LIMIT,LIMIT,n), out')
    shading interp
    C = cov(d_x,d_y);
    hold on
    error_ellipse(C, [mean(d_x); mean(d_y)],'conf', .95, 'style', 'w');
    hold on
    axis([-LIMIT LIMIT -LIMIT LIMIT])
    axis square
    xlabel('horizontal displacement (arcmin)')
    ylabel('vertical displacement (arcmin)')
    title(sprintf('%s dt: %.2f ms cov: %.2f Area: %.2f arcmin^2',FIGNAME, dt, C(1,2), Dsq(dt)))
    set(gca, 'FontSize', 12)
    set(gcf,'renderer','zbuffer')
    colormap hot
    %    print2eps(sprintf('./Figures/2dhist_%s.eps', FIGNAME))
    % 	%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
end
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



