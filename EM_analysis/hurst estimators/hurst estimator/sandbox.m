% sandbox by Janis
load FGN.mat
x = fgn070_S;

x = fbm1d(1.7/2, 2^8, 1);

h_absval = hurst_estimate(x, 'absval', 1, 1);

h_aggvar = hurst_estimate(x, 'aggvar', 1);

disp([h_absval, h_aggvar]);