function F = DFA(DATA, win_length, order)
% F = DFA(data, win_length, order)
% This function implements detrended fluctuation analysis:
% INPUTS:
%   DATA - series data (for example, the step size taken at each time in a
%       random walk)
%   win_length - window length (in indices) into which to segment DATA
%   order - polynomial order for fitting each window
% OUTPUTS:
%   F - mean squared residual for this window length ("fluctuations")
%
% A good description of this approach is in Bryce & Sprague (2012),
% Scientific Reports. "Revisiting detrended fluctuation analysis."
%
% downloaded from mathworks by Janis Intoy on January 26, 2020 - janis has
% since modified by adding comments, simplifying code, ...
% Made by: Guan Wenye
% contact:guanwenye@tju.edu.cn

% only use data up to multiple of window size
N=length(DATA);
n=floor(N/win_length);
N1=n*win_length;

% initialize cumulative data (y) and fit to dta (Yn) and fit coefficients
% (fitcoef)
y=zeros(N1,1);
Yn=zeros(N1,1);
fitcoef=zeros(n,order+1);

% accumulate data with mean subtracted (y)
mean1=mean(DATA(1:N1));
for i=1:N1
    y(i)=sum(DATA(1:i)-mean1);
end
y=y';

% for each window segment, fit polynomial and save fit coefficients
for j=1:n
    fitcoef(j,:) = polyfit(...
        1:win_length,... % x-axis
        y(((j-1)*win_length+1):j*win_length),... % y-axis
        order); % polynomial order
end

% for each window, compute predicted value (Yn) based on polynomial coef
for j=1:n
    Yn(((j-1)*win_length+1):j*win_length) = polyval(...
        fitcoef(j,:),... % fit coefficients
        1:win_length); % y-axis
end

% compute residuals and return mean-squared residual (F)
sum1=sum((y'-Yn).^2)/N1;
sum1=sqrt(sum1);
F=sum1;
