% script to test DFA

%% example usage
%{
H = 1.9;
doPlot = true;

[H_est, deltaT, mF, F] = estimateHurst_DFA(H, [], doPlot);
%}

%% test in multiple trials
ntrials = 20;
H_list = 0.1:0.1:1.9;

H_est_list = nan(length(H_list), ntrials*2);


% default Fs = 1024, dur = 2, ntrials = 50
doPlot = false;
for hi = 1:length(H_list)
    for ni = 1:ntrials
        H_est = estimateHurst_DFA(H_list(hi), [], doPlot);
        H_est_list(hi, 2*(ni-1) + (1:2)) = H_est;
    end
end

save('test_DFA.mat');

%%
figure(1); clf;
plot([0, 2], [0, 2], '--', 'Color', [.5, .5, .5]);
plot(H_list, H_est_list, 's');
errorbar(H_list, nanmean(H_est_list, 2), nanstd(H_est_list, [], 2), 'k.-',...
    'markersize', 20, 'capsize', 12);
xlabel('H');
ylabel('estimated H');

print('test_DFA.png', '-dpng');
