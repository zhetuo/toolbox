function [H_est, deltaT, mF, F] = estimateHurst_DFA(fix, Fs, doPlot)
% [H_est, deltaT, mF, F] = estimateHurst_DFA(fix, Fs, doPlot)
% [H_est, deltaT, mF, F] = estimateHurst_DFA(H, [], doPlot)
% A good description of this approach is in Bryce & Sprague (2012),
% Scientific Reports. "Revisiting detrended fluctuation analysis."
% INPUTS:
%   fix = struct array with 'x' and 'y' fields containing drift traces
%   Fs = sampling frequency of data
%   doPlot = flag for plotting or not
%   H = can input Hurst index [0, 2] as first input to simulate fBM traces
% OUTPUTS
%   H_est = estimated Hurst index
%   deltaT = size of windows for DFA approach (ms)
%   mF = mean "fluctuations" from DFA approach
%   F = all fluctuations
%
% use detrended fluctuation analysis to estimate Hurst exponent of FBM
% process. Note that many algorithms assume input is fractional
% brownian noise (or the difference signal of FBM). Also note that we don't
% use the convential definition of "H" - ours is double the usual so that
% it spans [0, 2] where H = 1 is Brownian motion
%
%  author: Janis Intoy, January 29, 2020

if ~isstruct(fix) % assume fix is a number representing H
    H = fix;
    dur = 2;
    Fs = 2^10;
    ntrials = 50;
    clear fix;
    fix(ntrials) = struct('x', [], 'y', []);
    for fi = 1:ntrials
        fix(fi).x = fbm1d(H/2, Fs * dur, dur);
        fix(fi).y = fbm1d(H/2, Fs * dur, dur);
    end
end

if ~exist('Fs', 'var') || isempty(Fs)
    Fs = 1000;
end

if ~exist('doPlot', 'var')
    doPlot = false;
end

% get list of durations
fixDurs = nan(length(fix), 1);
for fi = 1:length(fix)
    fixDurs(fi) = length(fix(fi).x); 
end
maxDurs = floor(quantile(fixDurs(fixDurs > 200), .9));

deltaN = 50:max(255, floor(maxDurs/2));
deltaT = deltaN / Fs;

polyorder = 1;

F = nan(length(fix), length(deltaN), 2);
ntrials = 0;

fprintf('computing F for %i traces\n', length(fix));
for fi = 1:length(fix)
    if rem(fi, 10) == 0
        fprintf('\t %i of %i traces\n', fi, length(fix));
    end
    x = fix(fi).x;
    y = fix(fi).y;
    
    if length(x) / Fs * 1000 < 200 % skip short segments
        continue;
    end
    ntrials = ntrials + 1;
    F(fi, :, 1) = computeOneF(x, deltaN, polyorder);
    F(fi, :, 2) = computeOneF(y, deltaN, polyorder);
end

if ntrials < 10
  H_est = [nan, nan];
  deltaT = nan;
  mF = nan; 
  F = nan;
  return;
end

%% compute mean F and do regression to get slope (H-estimate)

mF = squeeze(nanmean(F, 1));

logF = log(mF);
logT = log(deltaT);

% compute slopes from log-transformed data
slope_data = bsxfun(@rdivide, diff(log(F), [], 2), diff(logT));
slope_mn = bsxfun(@rdivide, diff(logF), diff(logT'));

mdls = cell(2, 1);
for di = 1:2
    mdls{di} = fitlm(logT(:), logF(:, di));
end
H_est = [mdls{1}.Coefficients{2, 1} * 2, mdls{2}.Coefficients{2, 1} * 2];

if exist('H', 'var')
    fprintf('test: H = %1.2f\tH_est = %1.2f, %1.2f\t%i repeats\n', H, H_est(1), H_est(2), ntrials);
end

%% plotting
if ~doPlot
    return;
end

%
labels = {'x', 'y'};
figure(1); clf;
for di = 1:2
    subplot(2, 2, 1 + (di-1)*2); hold on;
    plot(logT, log(F(1:7, :, di)), '.', 'MarkerSize', 1);
    plot(logT, log(mF(:, di)), 'k', 'linewidth', 2);
    plot(logT, mdls{di}.Fitted, '--', 'linewidth', 2, 'Color', [.5, .5, .5]);
    ylabel('log F (\Delta n)');
    xlim(logT([1, end]));
    set(gca, 'FontSize', 10);
    title(labels{di});
    
    subplot(2, 2, 2 + (di-1)*2); hold on;
    plot(logT(1:end-1), slope_data(1:7, :, di), '.', 'MarkerSize', 1);
    plot(logT(1:end-1), slope_mn(:, di), 'k', 'linewidth', 2);
    plot(logT(1:end-1), nanmean(slope_mn(:, di)) * ones(length(deltaT) - 1, 1), '--', 'linewidth', 2, 'Color', [.5, .5, .5]);
    ylabel('local slope');
    ylim([-1, 1]);
    xlim(logT([1, end]));
    set(gca, 'FontSize', 10);
    
    xlabel('log time (s)');
end

end

function F_deltaN = computeOneF(in, deltaN, polyorder)
in = in - mean(in);

F_deltaN = nan(size(deltaN));
for wi = 1:length(deltaN)
    winsize = deltaN(wi);
    
    F_deltaN(wi) = DFA(diff(in), winsize, polyorder);
end
end