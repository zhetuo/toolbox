function [out,Err]=saccGammaFit(saccTraj,velTh)

    if ~exist('velTh','var')
       velTh=1.5; 
    end
    
    coef=pca(double(saccTraj'));
    in=coef'*saccTraj;
    saccX=in(1,:);  saccY=in(2,:);
    
    if length(velTh)>1
        saccOn=velTh(1); saccOff=velTh(2);
    else
        velX=diff(saccX);
        saccOn=find(abs(velX)>velTh,1,'first');
        saccOff=find(abs(velX)>velTh,1,'last')+10;
    end
    
    saccAmp=double(saccX(saccOff)-saccX(saccOn));
   
    saccIn=saccX(saccOn:saccOff);
    costFun=@(param)saccGammaPredict(param,saccIn);
    param=fmincon(costFun, [saccAmp 5*rand()+0.5],...
        [],[],[],[],...
        [saccAmp-60 4],[saccAmp+60 6],[],optimoptions('fmincon','Display','off'));

    [Err,saccPredict]=saccGammaPredict(param,saccIn);
    
    saccX(saccOn:saccOff)=saccPredict;

    
    out=coef*[saccX; saccY];
    
    if 0
        figure('position',[100 100 800 300]); 
        subplot(1,3,1);  plot([saccIn; saccPredict]');
        subplot(1,3,2);  plot([diff(saccIn); diff(saccPredict)]');
        subplot(1,3,3);  hold on; plot(saccTraj(:,saccOn-20:saccOff+20)'); plot(out(:,saccOn-20:saccOff+20)');
        keyboard;
    end
end


function [cost,modelTrace]=saccGammaPredict(param,saccTrace)
    
    modelTrace=param(1)*gamcdf(1:length(saccTrace),6,param(2))+saccTrace(1);
    velErr=norm(diff(saccTrace)-diff(modelTrace))/length(saccTrace);
    traceErr=norm(saccTrace-modelTrace)/length(saccTrace);
    endErr=norm(saccTrace(end)-modelTrace(end));
    endGradient=norm((saccTrace(end)-saccTrace(end-1))-(modelTrace(end)-modelTrace(end-1)));
    cost=double(traceErr+velErr+endErr+endGradient);
end