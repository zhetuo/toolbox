
function [saccBool , score]=detectSacc(amp,dur,peakVel,nSD) % amp in deg; dur in sec; peakVel in deg/s
    amp=log10(amp);
    dur=log10(dur);
    peakVel=log10(peakVel);
    
    if ~exist('nSD','var')
        nSD=3;
    end
    
    score.dur=abs(dur-(0.31*amp-1.686));
    score.peakVel=abs(peakVel-(0.743*amp+1.764));
    if score.dur>nSD*0.2 || score.peakVel>nSD*0.1365
        saccBool=0;
    else
        saccBool=1;
    end
end
