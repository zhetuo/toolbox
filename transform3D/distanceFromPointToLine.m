function d = distanceFromPointToLine(p, a, n)
% d = distanceFromPointToLine(p, a, n)
% d = euclidean distance
% p = point vector
% a = point on the line
% n = unit vector in direction of the line
% https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Vector_formulation

% p = [3, 3];
% a = [0, 0];
% n = [1, 1] *sqrt(2)/2;

p = p(:);
a = a(:);
n = n(:);

d = sqrt(sum(((a - p) - ((a - p)' * n) * n).^2));
end