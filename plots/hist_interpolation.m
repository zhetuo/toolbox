function [prob, binCenter,probBoot]=hist_interpolation(data,range,nBin, BootN,interpN)

    switch nargin
        case 3
             interpN=11;
             BootN=0;
        case 4
            interpN=11;
    end
    
    xmax=range(2); xmin=range(1);
    

    [~,edgeTry]=histcounts(data,linspace(xmin, xmax, nBin));
    step=mean(diff(edgeTry));
    
    
    for j=interpN:-1:1
        shiftX=step/interpN*(j-floor((interpN+1)/2));
        [probM(j,:),edgeM(j,:)] = histcounts(data,linspace(xmin+shiftX, xmax+shiftX, nBin), 'Normalization', 'probability');
    end
    centerM=(edgeM(:,1:end-1)+edgeM(:,2:end))/2;
    [binCenter, idx]=sort(centerM(:));
    prob=probM(:); prob=prob(idx);
    
    startIdx=find(binCenter>xmin+step/2,1);
    endIdx=find(binCenter<xmax-step/2,1, 'last');
    binCenter=binCenter(startIdx:endIdx);
    prob=prob(startIdx:endIdx);
    
    if BootN>0
        for i=BootN:-1:1
            clear probM;
            for j=interpN:-1:1
                shiftX=step/interpN*(j-floor((interpN+1)/2));
                probM(j,:) = histcounts(randsample(data,length(data),1),linspace(xmin+shiftX, xmax+shiftX, nBin), 'Normalization', 'probability');
            end
            tmp=probM(:); probBoot(i,:)=tmp(idx);
        end
        probBoot=probBoot(:,startIdx:endIdx);
    else
        probBoot=nan;
    end
    
    if 0
        figure; hold on;
        histogram(data,linspace(xmin, xmax, nBin), 'Normalization', 'probability');
        
        if BootN>0
            shadedErrorBar(binCenter,mean(probBoot),std(probBoot),'m',1)
        end
        plot(binCenter,prob,'k','lineWidth',2);
        
    end
    
end