function [n,mids,ranges] = histcounts3(x, y, z, nbins)
% [n, xmid, ymid, zmid] = histcounts3(x, y, z, nbins)
% histcounts3 in three dimensions (n) with bin centers. nbins specifies the
% number of bins to use in each dimension [y, x, z] or the bin centers as a
% cell {ymid, xmid, zmid}
if iscell(nbins)
     xmid = nbins{2};
     ymid = nbins{1};
     zmid = nbins{3};
     
     nbins = [length(ymid), length(xmid), length(zmid)];
else
    [~,~,rx] = removeOutlier(x);
    xmid = linspace(rx(1), rx(2), nbins(2));
    
    [~,~,ry] = removeOutlier(y);
    ymid = linspace(ry(1), ry(2), nbins(2));
    
    [~,~,rz] = removeOutlier(z);
    zmid = linspace(rz(1), rz(2), nbins(2));
end

mids{1}=xmid; mids{2}=ymid; mids{3}=zmid;
ranges{1}=rx; ranges{2}=ry; ranges{3}=rz;

xsub = interp1(xmid, 1:nbins(2), x, 'nearest');
ysub = interp1(ymid, 1:nbins(1), y, 'nearest');
zsub = interp1(zmid, 1:nbins(3), z, 'nearest');

kill = isnan(xsub) | isnan(ysub) | isnan(zsub);
xsub(kill) = [];
ysub(kill) = [];
zsub(kill) = [];

ind = sub2ind(nbins, ysub, xsub, zsub);

uind = unique(ind);

n = zeros(nbins);
for ii = 1:length(uind)
    [r, c, d] = ind2sub(nbins, uind(ii));
    n(r, c, d) = sum(ind == uind(ii));
end

end