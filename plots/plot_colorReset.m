function h=plot_colorReset(X, varargin)
    h=plot(X, varargin{:});
    set(gca, 'ColorOrder', circshift(get(gca, 'ColorOrder'), numel(h)));
    
    if nargout==0
        clear h
    end
end