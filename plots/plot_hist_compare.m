function [prob,binCenter,mu,sd]=plot_hist_compare(data_all,legendName,xName,nBin,BootN,LOG,interpN)
    switch nargin
        case 3
             nBin=50; LOG=0;  BootN=0; interpN=11;
        case 4
            LOG=0;  BootN=0; interpN=11;
        case 5
            LOG=0; interpN=11;
        case 6
            interpN=11;
    end

    %figure; clf; 
    hold on; cols=get(gca, 'colorOrder'); 
    for i=length(data_all):-1:1

        [data{i},~,rangeM(i,:)]=removeOutlier(data_all{i},4);
    end

    xmin=min(rangeM(:));  xmax=max(rangeM(:));

    
    clear N edges h; 
    for i=length(data):-1:1
        if LOG
            [prob(i,:), binCenter,probBoot]=hist_interpolation_LOG(data{i},[xmin xmax],nBin, BootN,interpN);
     
            if BootN>0
                shadedErrorBar(binCenter,probBoot,{@mean,@std},{'col',cols(i,:),'lineWidth',1},1);
            end
            h(i)=plot(binCenter,prob(i,:),'color',cols(i,:),'lineWidth',2);
            
        else
            [prob(i,:), binCenter,probBoot]=hist_interpolation(data{i},[xmin xmax],nBin, BootN,interpN);
     
            if BootN>0
                shadedErrorBar(binCenter,probBoot,{@mean,@std},{'col',cols(i,:),'lineWidth',1},1);
            end
            h(i)=plot(binCenter,prob(i,:),'color',cols(i,:),'lineWidth',2);
            
        end
%         data(data<dataMin(i)|data>dataMax(i))=[];
        mu(i)=mean(data{i}); sd(i)=std(data{i});

        yl=ylim;
        plot(mu(i) * [1, 1], yl, 'color',cols(i,:),'lineStyle','--');
        if mu(i)<1
            text(mu(i), yl(1) + .8*diff(yl)+(i-1.5)*0.01,sprintf('%1.3f\\pm%1.3f', mu(i),sd(i)),'FontSize', 14, 'color',cols(i,:));
        else
            if mu(i)>10
                text(mu(i), yl(1) + .8*diff(yl)+(i-1.5)*0.01,sprintf('%1.1f\\pm%1.1f', mu(i),sd(i)),'FontSize', 14, 'color',cols(i,:));
            else
                text(mu(i), yl(1) + .8*diff(yl)+(i-1.5)*0.01,sprintf('%1.2f\\pm%1.2f', mu(i),sd(i)),'FontSize', 14, 'color',cols(i,:));
            end
        end
    end  
   
    if isempty(legendName)
        legend(h,legendName,'location','northwest');
    end
    xlabel(xName); ylabel('probability');
    ylim([0 yl(2)]);
    set(gca,'FontSize',14);
    if LOG
        set(gca,'Xscale','log')
    end
end