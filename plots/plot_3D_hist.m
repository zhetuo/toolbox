function [N, edges,ranges]=plot_3D_hist(data3,nBins)

    if ~exist('nBins','var')
        nBins=[30, 30, 30];
    end
    
    [N, edges, ranges] = histcounts3(data3(1, :), data3(2, :),data3(3, :), nBins);
    

    N = N / sum(N(:)); % [X, distance, Y] for 3d plotts
    
    [X2, Y2, Z2] = meshgrid(edges{1}, edges{2}, edges{3});
    
    q = 1-[.9 .5 .1]; % had standard deviations but it didn't look nice
    t = [.05, .3, 1];
    colfrac = [.2, .5, .99];
    
    tmp = sort(N(:));     tmp(tmp == 0) = [];
    tmpindex = interp1(cumsum(tmp), 1:length(tmp), q, 'nearest');
    qv = tmp(tmpindex);
    
    cols = flipud(colormap('hot'));
    cols = cols(:, [3, 2, 1]);
    cols = cols(1:end-10, :);
    for qi = 1:length(q)
        p = patch(isosurface(X2, Y2, Z2, N, qv(qi)));
        isonormals(X2, Y2, Z2, N, p);
        p.FaceAlpha = t(qi);
        
        p.FaceColor = cols(round(colfrac(qi)*size(cols, 1)), :);
        p.EdgeColor = 'none';
    end
    axis equal; view(3); grid on;
    %daspect([1 1 1]);    view(3);
    camlight; lighting gouraud;
    xlim(ranges{1}); ylim(ranges{2}); zlim(ranges{3}); 
    axis image;
end