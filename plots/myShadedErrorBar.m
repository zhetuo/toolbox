function myShadedErrorBar(data,xIn)
    if exist('xIn','var')
        x=xIn;
    else
        x=1:size(data,2);
    end
    hold on;
    plot(x,data','--');
    shadedErrorBar(x,mean(data),std(data)/sqrt(size(data,1)),{'col',[0 0 0],'LineWidth', 1.5},1);
    set(gca, 'FontSize',14)
    xlim([x(1) x(end)]); 
end