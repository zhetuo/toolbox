function [densityMap,mids,range,p,R2]=plot_2D_hist(input2,nbins,rangeK,PLOT_LINE,normAxis)
    % XD: a vector of x positions
    % YD: a vector of y positions
    % nbins: [yDim xDim]
    
    switch nargin 
        case 1
            nbins=[30 30]; rangeK=3; PLOT_LINE=1; normAxis=[];
        case 2
            rangeK=3; PLOT_LINE=1; normAxis=[];
        case 3
            PLOT_LINE=1; normAxis=[];
        case 4
            normAxis=[];
    end
    
    if length(nbins)==1
        nbins=[nbins nbins];
    end
    XD=input2(1,:); YD=input2(2,:);
    [~,idx1,range{1}] = removeOutlier(XD,rangeK);
    mids{1} = linspace(range{1}(1), range{1}(2), nbins(2));
    
    [~,idx2,range{2}] = removeOutlier(YD);
    mids{2} = linspace(range{2}(1), range{2}(2), nbins(1));
    idx=unique([idx1 idx2]);
    XD(idx)=[]; YD(idx)=[]; 

    xsub = interp1(mids{1}, 1:nbins(2), XD, 'nearest');
    ysub = interp1(mids{2}, 1:nbins(1), YD, 'nearest');
   
    kill = isnan(xsub) | isnan(ysub);
    xsub(kill) = []; ysub(kill) = [];

    ind = sub2ind(nbins, ysub, xsub);

    uind = unique(ind);

    densityMap = zeros(nbins);
    for ii = 1:length(uind)
        [r, c] = ind2sub(nbins, uind(ii));
        densityMap(r, c) = sum(ind == uind(ii));
    end

    DM_smooth=imgaussfilt(densityMap,2); 
    if isempty(normAxis)
        DM_normalized=DM_smooth/max(DM_smooth(:));
        DM_normalized(DM_smooth<5)=NaN;
    else
        if strcmp(normAxis,'x')
            for xi=size(DM_smooth,2):-1:1
                DM_normalized(:,xi)=DM_smooth(:,xi)/sum(DM_smooth(:,xi));
                DM_normalized(DM_smooth<5)=NaN;
            end
        elseif strcmp(normAxis,'y')
            for yi=size(DM_smooth,1):-1:1
                DM_normalized(yi,:)=DM_smooth(yi,:)/sum(DM_smooth(yi,:));
                DM_normalized(DM_smooth<5)=NaN;
            end
        else
            DM_normalized=DM_smooth/max(DM_smooth(:));
            DM_normalized(DM_smooth<5)=NaN;
        end
    end
    
    
    hold on;
    imagesc(mids{1}, flip(mids{2}), flipud(DM_normalized));
    
    p=polyfit(XD,YD,1);
    Y1=polyval(p,XD);
 
    if PLOT_LINE
        plot( mids{1}, polyval(p,mids{1}),'r-');
    end
    SS_tot=sum((YD-mean(YD)).^2);
    SS_res=sum((YD-Y1).^2);
    R2=1-SS_res/SS_tot;
    
    xlabel('horizontal (arcmin)'); ylabel('vertical (arcmin)');
    grid on;
    set(gca,'FontSize',14)
    set(gca, 'ydir', 'normal');
    
    
    colorM=parula(256);
    stepN=30;
    color0=[(1:-(1-colorM(1,1))/stepN:colorM(1,1))' (1:-(1-colorM(1,2))/stepN:colorM(1,2))' (1:-(1-colorM(1,3))/stepN:colorM(1,3))'];
    if isempty(normAxis)
        caxis([0 1])
    end
    colormap( [color0; parula(256)] )
    colorbar;
    

        
    hold off;
end
