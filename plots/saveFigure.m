function saveFigure(fileName, outputFolder,HighRes,fig)

    switch nargin
        case 2
            HighRes=0; figBool=0;
        case 3 
            figBool=0;
        case 4   
            figBool=1;
        otherwise
            error('the number of input arguments are not correct!');
    end
    
    if figBool     
        saveas(fig,[outputFolder fileName '.fig']);
    else
        saveas(gcf,[outputFolder fileName '.fig']);
    end

    
    if HighRes
        if figBool
             print(fig,[outputFolder fileName '.png'],'-dpng','-r300');
             print(fig,[outputFolder fileName '.eps'], '-depsc2', '-tiff','-r300', '-painters');
        else
             print(gcf,[outputFolder fileName '.png'],'-dpng','-r300');
             print([outputFolder fileName '.eps'], '-depsc2', '-tiff','-r300', '-painters');
        end
    else
        if figBool
            saveas(fig,[outputFolder fileName '.png']);
            saveas(fig,[outputFolder fileName], 'epsc');
        else
            saveas(gcf,[outputFolder fileName '.png']);
            saveas(gcf,[outputFolder fileName], 'epsc');
        end
        
    end
end