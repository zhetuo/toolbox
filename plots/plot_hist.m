function [prob, binCenter,probBoot,mu,sd]=plot_hist(XD,nBin,BootN,interpN,mode)
    switch nargin
        case 1
            nBin=30; BootN=0; interpN=11; mode='mean';
        case 2
            BootN=0; interpN=11; mode='mean';
        case 3
            interpN=11; mode='mean';
        case 4 
            mode='mean';
    end
    

    XD=double(XD);
    
    
    [XD,~,ranges]=removeOutlier(XD,4);

%     xmin=prctile(XD,2.5); xmax=prctile(XD,97.5);
%     range=[xmin xmax]; XD(XD<xmin & XD>xmax)=[];

    [prob, binCenter,probBoot]=hist_interpolation(XD,[ranges(1) ranges(end)],nBin, BootN,interpN);

    hold on;
    if BootN>0
        shadedErrorBar(binCenter,probBoot,{@mean,@std},{'col',0.3*ones(1,3),'lineWidth',1},1);
    end
    plot(binCenter,prob,'color',0.3*ones(1,3),'lineWidth',2);
    
    if strcmp(mode,'mean')
        yl=ylim; mu=mean(XD); sd=std(XD);
        plot(mu * [1, 1], yl,'lineStyle','--','lineWidth',1.5,'color','k');
        text(mu, yl(1) + .8*diff(yl),sprintf('%1.3f\\pm%1.3f', mu,sd),'FontSize', 14);
    elseif strcmp(mode,'median')
        yl=ylim; mu=median(XD); %sd=std(XD);
        plot(mu * [1, 1], yl,'lineStyle','--','lineWidth',1.5,'color','k');
        %text(mu, yl(1) + .8*diff(yl),sprintf('%1.3f\\pm%1.3f', mu,sd),'FontSize', 14);
    end

    ylabel('probability');
    yl=ylim;
    ylim([0 yl(2)]);
    hold off;
    
end
