function [prob, binCenter,probBoot]=hist_interpolation_LOG(data,range,nBin, BootN,interpN)

    switch nargin
        case 3
             interpN=11;
             BootN=0;
        case 4
            interpN=11;
    end
    
    xmax=range(2); xmin=range(1);
    

    [~,tmp]=histcounts(data,logspace(log10(xmin), log10(xmax), nBin));
    edgeTry=log10(tmp);
    step=mean(diff(edgeTry))/interpN;
    
    
    for j=interpN:-1:1
        shiftX=step*(j-floor((interpN+1)/2));
        [probM(j,:),edgeM(j,:)] = histcounts(data,10.^(edgeTry+shiftX), 'Normalization', 'probability');
    end
    centerM=10.^((log10(edgeM(:,1:end-1))+log10(edgeM(:,2:end)))/2);
    [binCenter, idx]=sort(centerM(:));
    prob=probM(:); prob=prob(idx);
    
    startIdx=find(binCenter>xmin+step/2,1);
    endIdx=find(binCenter<xmax-step/2,1, 'last');
    binCenter=binCenter(startIdx:endIdx);
    prob=prob(startIdx:endIdx);
    
    if BootN>0
        for i=BootN:-1:1
            clear probM;
            for j=interpN:-1:1
                shiftX=step*(j-floor((interpN+1)/2));
                probM(j,:) = histcounts(randsample(data,length(data),1),10.^(edgeTry+shiftX), 'Normalization', 'probability');
            end
            tmp=probM(:); probBoot(i,:)=tmp(idx);
        end
        probBoot=probBoot(:,startIdx:endIdx);
    else
        probBoot=nan;
    end
    
    if 0
        figure; hold on;
        histogram(data,logspace(log10(xmin), log10(xmax), nBin), 'Normalization', 'probability');
        
        if BootN>0
            shadedErrorBar(binCenter,mean(probBoot),std(probBoot),'m',1);
        end
        plot(binCenter,prob,'k','lineWidth',2);
        %set(gca,'Xscale','log');
        
    end
    
end