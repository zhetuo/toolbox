function [f,X,p]=myFFT(x,Fs,PLOT)

    L=size(x,2);
    f = Fs*(0:(L/2))/L;
 
    for i=size(x,1):-1:1
        x(i,:)=x(i,:)-mean(x(i,:));
        
        fft_tmp=fft(x(i,:));
        X_tmp=abs(fft_tmp);
        X_tmp=X_tmp(1:L/2+1); 
        XM(i,:)=X_tmp/norm(X_tmp);
    
        p_tmp=angle(fft_tmp);
        pM(i,:)=p_tmp(1:L/2+1);
    end
    
    if size(x,1)>1
        X.mu=mean(XM);
        X.sem=std(XM)/sqrt(size(x,1));
        
        p.mu=mean(pM);
        p.sem=std(pM)/sqrt(size(x,1));
        
        if exist('PLOT','var')
            if PLOT==1
                figure; shadedErrorBar(f,X.mu,X.sem);
            end
        end
        
    else
        X=XM; p=pM;
       
        if exist('PLOT','var')
            if PLOT==1
                figure; plot(f,X);
            end
        end
    end
end