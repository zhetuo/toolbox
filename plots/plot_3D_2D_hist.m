function [n, edges,ranges]=plot_3D_2D_hist(data3,nBins,q,clevels,l3)

    switch nargin
        case 1
            nBins=[30, 30, 30];
            q = 1-[.95, .85, 0.7]; % quantile levels of each blob
            clevels = linspace(.1, .9, 5); % isocontour levels (actual values)
        case 2
            q = 1-[.95, .85, 0.7]; % quantile levels of each blob
            clevels = linspace(.1, .9, 5); % isocontour levels (actual values)
        case 3
            clevels = linspace(.1, .9, 5); % isocontour levels (actual values)
    end
    [N, edges, ranges] = histcounts3(data3(1, :), data3(2, :),data3(3, :), nBins);
    

    n = N / sum(N(:)); % [X, distance, Y] for 3d plotts
    
    
    [X2, Y2, Z2] = meshgrid(edges{1},edges{2}, edges{3});

    
t = [.1, .3, .5]; % transparancies of blobs
colfrac = [.2, .5, .99]; % colormap level for each blob

cols_tmp = flipud(colormap('hot'));
cols_tmp = cols_tmp(:, [3, 2, 1]);
cols_tmp = cols_tmp(10:end-10, :);

bcolors = nan(length(q), 3);
for qi = 1:length(q)
    bcolors(qi, :) = cols_tmp(round(colfrac(qi)*size(cols_tmp, 1)), :);
end

% 2d marginal parameters
ccolors = flipud(cool(length(clevels) + 1)); % colors for isocontour levels
ccolors(1, :) = [];

clear cols_tmp, colfrac;
    % moving average
    for ii = 2:size(n, 1)-1
        for jj = 2:size(n, 2)-1
            for kk = 2:size(n, 3)-1
                tmp=n(ii-1:ii+1, jj-1:jj+1, kk-1:kk+1);
                n(ii, jj, kk) = mean(tmp(:));
            end
        end
    end

    tmp = sort(n(:));
    tmp(tmp == 0) = [];
    tmpindex = interp1(cumsum(tmp), 1:length(tmp), q, 'nearest');
    qv = tmp(tmpindex);

    fig=figure('position',[100 30 500 450]); clf; hold on;
    for qi = 1:length(q)
        p = patch(isosurface(X2,Y2 , Z2, n, qv(qi)));
        isonormals(X2,Y2 , Z2, n, p);
        p.FaceAlpha = t(qi);

        p.FaceColor = bcolors(qi, :);
        p.EdgeColor = 'none';
    end
    %daspect([1 1 1])
    view(3);
    axis tight;
    camlight('headlight');
    %     lighting gouraud;

    
    for i=1:3
        margin(i)=abs(ranges{i}(2)-ranges{i}(1))/2;
    end
    
    if ~exist('l3','var')
        xl=[ranges{1}(1) ranges{1}(2)+margin(1)];
        yl=[ranges{2}(1) ranges{2}(2)+margin(2)];
        zl=[ranges{3}(1)-margin(3) ranges{3}(2)];
    else
        xl=l3{1};
        yl=l3{2};
        zl=l3{3};
    end
    % 1: version x - version y
    % 2: vergence x - version y
    % 3: vergence x - version x
%     for ei=1:3
%         edges_interp{ei}=interp1(edges{ei},2,'linear');
%     end
%     [X2_interp, Y2_interp, Z2_interp] = meshgrid(edges_interp{1},edges_interp{2}, edges_interp{3});

       
    for di = 1:3
        n2 = squeeze(sum(n, di));
        n2 = n2 / max(n2(:));

        n2_interp = interp2(n2,2,'spline');  n2_interp=n2_interp/max(n2_interp(:));
        
        
        
        for ci = 1:length(clevels)
            switch di
                case 1
                    [M, c] = contour(interp2(squeeze(Y2(:, 1, :)),2,'linear'), interp2(squeeze(Z2(:, 1, :)),2,'linear'), n2_interp, clevels(ci)*[1,1]);
                    idx=[1 find(diff(diff(M(2,:)))<-2)+1 length(M(2,:))];
                    [~,i]=max(diff(idx));
                    M=M(:,idx(i)+1:idx(i+1));
                    h3 = plot3(xl(2) * ones(size(M, 2)), M(1, :), -M(2, :));
                case 2
                    [M, c] = contour(interp2(squeeze(X2(1, :, :)),2,'linear'), interp2(squeeze(Z2(1, :, :)),2,'linear'), n2_interp, clevels(ci)*[1,1]);
                    idx=[1 find(diff(diff(M(2,:)))<-2)+1 length(M(2,:))];
                    [~,i]=max(diff(idx));
                    M=M(:,idx(i)+1:idx(i+1));
                    h3 = plot3(M(1, :), yl(2) * ones(size(M, 2)), -M(2, :));
                case 3
                    [M, c] = contour(interp2(squeeze(X2(:, :, 1)),2,'linear'), interp2(squeeze(Y2(:, :, 1)),2,'linear'), n2_interp, clevels(ci)*[1,1]);
                    idx=[1 find(diff(diff(M(2,:)))<-2)+1 length(M(2,:))];
                    [~,i]=max(diff(idx));
                    M=M(:,idx(i)+1:idx(i+1));
                    h3 = plot3(M(1, :), M(2, :), zl(1) * ones(size(M, 2)));
            end
            set(h3, 'Color', ccolors(ci, :), 'LineWidth', 2);
            delete(c);
        end
    end
    grid on;

    
    xlim(xl); ylim(yl); zlim(zl); 
    %axis image;
    
    ax = gca;
box off;
% ax.BoxStyle = 'full';
ax.Color = 'none';

%%%%% move the axes lines to the back of the 3d space
% XRuler may be called XAxis in newer matlab versions
ax.XRuler.FirstCrossoverValue = yl(2); % X crossover wtih Y axis
ax.XRuler.SecondCrossoverValue = zl(1); % X crossover with Z axis
ax.YRuler.FirstCrossoverValue = xl(2); % Y crossover with X axis
ax.YRuler.SecondCrossoverValue = zl(1); % Y crossover with Z axis
ax.ZRuler.FirstCrossoverValue = yl(2); % Z crossover with X axis
ax.ZRuler.SecondCrossoverValue = xl(2); % Z crossover with Y axis

% ax.XRuler.FirstCrossoverValue = 0; % X crossover wtih Y axis
% ax.XRuler.SecondCrossoverValue = 0; % X crossover with Z axis
% ax.YRuler.FirstCrossoverValue = 0; % Y crossover with X axis
% ax.YRuler.SecondCrossoverValue = 0; % Y crossover with Z axis
% ax.ZRuler.FirstCrossoverValue = 0; % Z crossover with X axis
% ax.ZRuler.SecondCrossoverValue = 0; % Z crossover with Y axis


%%%%%%%%%%% axes labels
% ly = ylabel('x version', 'FontSize', 14, 'FontName', 'arial');
% lz = zlabel('y version', 'FontSize', 14, 'FontName', 'arial');
% lx = xlabel('x vergence', 'FontSize', 14, 'FontName', 'arial');
% 
% set(lz, 'Position', [xl(2) + .1 * diff(yl),...
%                      yl(2) + .1 * diff(xl),...
%                      zl(2)]);
set(gca,'FontSize',16);
% set(ax.XRuler.SecondaryLabel, 'Visible','on', 'String', 'horz vergence');  % main label at bottom
% set(ax.YRuler.SecondaryLabel, 'Visible','on', 'String', 'horz version');  % main label at bottom
% set(ax.ZRuler.SecondaryLabel, 'Visible','on', 'String', 'vert version');  % main label at bottom

%%%%%%%%%% move tick labels farther from axes
ax.XRuler.TickLabelGapOffset = 10; 
ax.YRuler.TickLabelGapOffset = 10; 
ax.ZRuler.TickLabelGapOffset = 10; 

% ax.PositionConstraint = 'innerposition';
% margin = 0.1;
% outpos = get(fig, 'outerPosition');
% outpos(3) = (1+2*margin)*outpos(3);
% outpos(4) = (1+2*margin)*outpos(4);
% set(fig, 'outerPosition', outpos)      
end