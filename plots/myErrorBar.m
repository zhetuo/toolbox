function myErrorBar(data,xIn,lsIn)
    if nargin<2
        x=1:size(data,2);
    else
        x=xIn;
    end
    
    
    if nargin<3
        ls='--';
    else
        ls=lsIn;
    end
    
    xsize=mean(diff(x));
    hold on; 
    if strcmp(ls,'--')
        plot(x-0.05*xsize,data','lineStyle',ls);
    else
        plot(x-0.05*xsize,data','lineStyle',ls,'lineWidth',1.5,'Marker','square');
    end
    errorbar(x+0.05*xsize,mean(data),std(data)/sqrt(size(data,1)),'color',[0 0 0],'lineWidth',2)
    set(gca, 'FontSize',16)
    xlim([x(1)-0.2 x(end)+0.2]); 
    hold off;
end