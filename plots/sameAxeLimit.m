function sameAxeLimit(axs) % axs(1)=gca; 
    
    xMax=max([axs.XLim]); xMin=min([axs.XLim]);
    yMax=max([axs.YLim]); yMin=min([axs.YLim]);
    
    for i = 1:length(axs)
        xlim(axs(i),[xMin xMax]); 
        ylim(axs(i),[yMin yMax]); 
    end
end