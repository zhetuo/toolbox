function myBar(data,xIn)
    if exist('xIn','var')
        x=xIn;
    else
        x=1:size(data,2);
    end
    hold on; 
    plot(x,data','--o')
    errorbar(x,mean(data),std(data)/sqrt(size(data,1)),'color',[0 0 0],'lineWidth',2, 'LineStyle', 'none')
    bar(x,mean(data),'FaceColor','none')
    set(gca, 'FontSize',16)
    xlim([0.5 size(data,2)+0.5]); 
end