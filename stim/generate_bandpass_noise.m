% Dim: dimension of the image, which determines the resolution of temporal frequency
% freqBand: [Low High] the low and high edges of the frequency band. To generate low-pass, Low=0, high-pass, High=inf
% Fs: sampling frequency, pixels for degree
% 
function [out,outFFT,k]=generate_bandpass_noise(freqBand,Fs,PLOT,Dim)

    switch nargin
        case 2
            PLOT=0; Dim=4000; 
        case 3
            Dim=4000; 
        case 4
            
        otherwise
            error('Too many input arguments');
            
    end
    rawImg=wgn(Dim,Dim,1);
    I=fftshift(fft2(rawImg));
    Hi=freqBand(2); Lo=freqBand(1);
    freqMask=ones(Dim,Dim);
    for y=1:Dim
        for x=1:Dim
            r=sqrt((x-(1+Dim)/2)^2+(y-(1+Dim)/2)^2);
            if r<Lo/Fs*Dim||r>Hi/Fs*Dim
                freqMask(y,x)=0;
            end
        end
    end
    out=ifft2(ifftshift(I.*freqMask),'symmetric');   

    outFFT=RadialProfile(fftshift(fft2(out)),Dim/2);
    k=Fs*(1:round(Dim/2))/Dim;
    if PLOT
        figure('position',[100 100 800 400]);
        subplot(1,2,1); imshow(out,[]);
        subplot(1,2,2); plot(k,abs(outFFT),'lineWidth',2);
        set(gca,'xScale','log','yScale','log');
        xlabel('spatial frequency (cpd)');
        ylabel('amplitude');
    end
end