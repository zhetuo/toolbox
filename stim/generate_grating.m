function stim=generate_grating(orient,sf,pxAng,stimSize)
Fs=60/pxAng; 
if ~exist('stimSize','var')
    stimSize = round(4*60/sf/pxAng);
    GaussStd = 40/sf/pxAng;
else
    GaussStd = stimSize/6;
end
grat_gauss = GaussStd^2*2*pi*...
    fspecial('gaussian', stimSize, GaussStd);
grat_gauss(grat_gauss<0.004) = 0;
grat_gauss = grat_gauss./max(max(grat_gauss));

stim=zeros(stimSize,stimSize);
for y=1:stimSize
    for x=1:stimSize
        if orient==0
            dist=x-(stimSize+1)/2;
        else
            a=1/tan(orient);
            dist=(y-(stimSize+1)/2+a*(x-(stimSize+1)/2))/sqrt(1+a^2);
        end


        val=sin(2*pi*sf*dist/Fs);
        stim(y,x)=val;
    end 
end
stim=stim.*grat_gauss;
end
       