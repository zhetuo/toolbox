clear; close all;

figurePath=createFolder('./bpImg/');
MONITOR_DISTANCE = 1200; %mm
PIXEL_X = 1920; 
PIXEL_Y = 1080; 
DisplayWidth = 545; %mm

pxAng = atan(DisplayWidth/2/MONITOR_DISTANCE)/pi*180*60/(PIXEL_X/2); % arcmin of each pixel
Fs=60/pxAng; % pixels for degree

stimSize = PIXEL_X;
randN=500;

%%
h = waitbar(0, 'creating stimulus');
for randI=randN:-1:1
   
    waitbar((randN-randI+1)/randN, h);
    
    [img,FFT(randI,:),k]=generate_bandpass_noise([0 0.1],Fs);
    
end
close(h);