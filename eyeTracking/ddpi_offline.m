function data = ddpi_offline(videoFile, videoSize, p4Intensity, p4Radius, params, visualize, GPU)
% This function applies offline digital Dual Purkinje Image (dDPI) alogrithm 
% to an input video and tracks eye.
%
% Example:
%     ddpi_offline(videoFileName, 150, 8)
%
%     ddpi_offline(videoFileName, [], [], [], false)
%
%     params.downSampling = 8;
%     params.gaussianKernelSize = 3;
%     params.pauseDuration = 0.01;
%     params.p1.threshold = 250;
%     params.p1.roi = 128;
%     params.p4.roi = 64;
%     params.p4.templateSize = 4;  
%     ddpi_offline(videoFileName, [], [], params)
%
% INPUT:
%     videoFile - the aboslute path of the raw video file (avi)
%     p4Intensity - the intensity of P4 template
%     p4Radius - the radius of P4 template
%     params - the parameters of dDPI
%         params.downSampling - downsampling rate 
%         params.gaussianKernelSize - the size of the Gaussian blur kernel
%         params.pauseDuration - the puase duration between each frame
%         params.p1.threshold - the threshold of P1 mask 
%         params.p1.roi - the size of P1 ROI
%         params.p4.roi - the size of P4 ROI
%     visualize - true if the video is shown when tracking
%     GPU - true if use GPU to do computation (need matlab supported GPU)
%
% OUTPUT:
%    data - eye tracking results.
%         data.p1.trace - the trace of P1
%         data.p1.intensity- the intensity of P1
%         data.p1.roi - the ROI of P1
%         data.p4.trace - the trace of P4
%         data.p4.intensity - the intensity of P4
%         data.p4.radius - the radius of P4
%         data.p4.score - the score of P4
%         data.p4.roi - the ROI of P4

    if ~exist('videoFile', 'var')
        error("Video file path can not be empty.");
    end
    
    if ~exist('videoSize','var')
        videoSize = [992  2048]
    end
    
    if (~exist('p4Intensity', 'var') || isempty(params))
        p4Intensity = 150;
    end

    if (~exist('p4Radius', 'var') || isempty(params))
        p4Radius = 8;
    end

    if (~exist('params', 'var') || isempty(params))
        params.downSampling = 8;
        params.gaussianKernelSize = 3;
        params.pauseDuration = 0.01;

        params.p1.threshold = 250;
        params.p1.roi = 256;% 128;

        params.p4.roi = 64;
        params.p4.templateSize = 4;  
    end

    if ~exist('visualize', 'var')
        visualize = true;
    end
    
    if ~exist('GPU', 'var')
        GPU = false;
    end
    
    p4Template = fspecial('gaussian', params.p4.templateSize, p4Radius / params.downSampling);
    p4Template = int32(round(p4Template / max(p4Template, [], 'all') * p4Intensity));

    tMatcher = vision.TemplateMatcher(...
        'Metric', 'Sum of squared differences', ...
        'OutputValue', 'Metric matrix');

    % read image
    v = rawVideoReader(videoFile, videoSize(2), videoSize(1));
    
    
    % get image format
    dsImgSize = videoSize / params.downSampling;
    nFrames   = round(v.totalFrames);

    if visualize
        vfig = figure();
        handles = imshow(zeros(videoSize), [0 255]); hold on;
        handleP1ROI = plot(handles.Parent, [0], [0], '-', 'color', 'r', 'LineWidth', 2);
        handleP4ROI = plot(handles.Parent, [0], [0], '-', 'color', 'r', 'LineWidth', 2);
        hold off;
    end

    [xIdx, yIdx] = meshgrid(1:dsImgSize(2), 1:dsImgSize(1));
    [p1xIdx, p1yIdx] = meshgrid(1:params.p1.roi, 1:params.p1.roi);
    [p4xIdx, p4yIdx] = meshgrid(1:params.p4.roi, 1:params.p4.roi);
    if (GPU)
        xIdx = int32(gpuArray(xIdx));
        yIdx = int32(gpuArray(yIdx));
        p1xIdx = double(gpuArray(p1xIdx));
        p1yIdx = double(gpuArray(p1yIdx));
        p4xIdx = double(gpuArray(p4xIdx));
        p4yIdx = double(gpuArray(p4yIdx));
    else 
        xIdx = int32(xIdx);
        yIdx = int32(yIdx);
    end
    
    data.p1.trace = zeros(2, nFrames);
    data.p1.intensity = zeros(1, nFrames);
    data.p1.roi = zeros(2, nFrames);
    data.p4.trace = zeros(2, nFrames);
    data.p4.intensity = zeros(1, nFrames);
    data.p4.radius = zeros(1, nFrames);
    data.p4.score = zeros(1, nFrames);
    data.p4.roi = zeros(2, nFrames);
    for frameIdx=1:nFrames

        % read image
        vis = v.getNextFrame;
        if (GPU)
            gImg = int32(gpuArray(vis(:, :, 1)));
            dsImg = gImg(1:params.downSampling:end,1:params.downSampling:end,:);
            cdsImg = gather(dsImg);
        else
            gImg = int32(vis(:, :, 1));
            dsImg = gImg(1:params.downSampling:end,1:params.downSampling:end,:);
            cdsImg = dsImg;
        end

        % threshold to evaluate p1
        p1MaskImg = dsImg;
        p1Mask = p1MaskImg < params.p1.threshold;
        p1MaskImg(p1Mask) = 0;

        % estimate p1 ROI by center of mass
        p1c0 = double(sum(p1MaskImg, 'all'));
        p1cX = double(sum(p1MaskImg .* xIdx, 'all')) / p1c0;
        p1cY = double(sum(p1MaskImg .* yIdx, 'all')) / p1c0;
        if (GPU)
            p1cX = gather(p1cX);
            p1cY = gather(p1cY);
        end

        p1Loc = int32(round(params.downSampling * [p1cX, p1cY]));

        % calculate p1 ROI
        [p1ROIx, p1ROIxEnd, p1ROIy, p1ROIyEnd] = ...
            getROI(p1Loc, params.p1.roi, videoSize);

        % blur p1 ROI
        p1ROI = gImg(p1ROIy:p1ROIyEnd, p1ROIx:p1ROIxEnd);
        p1ROI = imgaussfilt(double(p1ROI), params.gaussianKernelSize);

        % localize p1 ROI by center of mass
        p10 = sum(p1ROI, 'all');
        p1x = sum(p1ROI .* p1xIdx, 'all') / p10;
        p1y = sum(p1ROI .* p1yIdx, 'all') / p10;
        if (GPU)
            p10 = gather(p10);
            p1x = gather(p1x) ;
            p1y = gather(p1y) ;
        end
        
        data.p1.trace(1, frameIdx) = p1x + double(p1ROIx);
        data.p1.trace(2, frameIdx)  = p1y + double(p1ROIy);
        data.p1.intensity(frameIdx) = p10 / params.p1.roi / params.p1.roi;
        data.p1.roi(:, frameIdx) = [p1ROIx p1ROIy];
        
        
        dsP1ROI = ...
            int32(round(double([p1ROIx, p1ROIy]) / params.downSampling * 0.875));
        dsP1ROIEnd = ...
            int32(round(double([p1ROIxEnd, p1ROIyEnd]) / params.downSampling * 1.125));
        cdsImg(dsP1ROI(2):dsP1ROIEnd(2), dsP1ROI(1):dsP1ROIEnd(1)) = 0;
        
        % estimate p4 ROI by template matching
        p4Error = tMatcher(cdsImg, p4Template);
        if (GPU)
            p4Error = gpuArray(p4Error);
        end
        [p4Score, p4ErrorIdx]  = min(p4Error(:));
        [p4LocY, p4LocX] = ind2sub(size(p4Error), p4ErrorIdx);
        if (GPU)
            p4LocX = gather(p4LocX);
            p4LocY = gather(p4LocY);
        end
        p4Loc = int32(round(params.downSampling * [p4LocX, p4LocY]));

         % calculate p4 ROI
        [p4ROIx, p4ROIxEnd, p4ROIy, p4ROIyEnd] = getROI(p4Loc, params.p4.roi, videoSize);

        % blur p4 ROI
        p4ROI = double(gImg(p4ROIy:p4ROIyEnd, p4ROIx:p4ROIxEnd));

        % double pass
        p4ROI4 = power(p4ROI, 4);
        p40 = sum(p4ROI4, 'all');
        p4cx = int32(round(sum(p4ROI4 .* p4xIdx, 'all') / p40));
        p4cy = int32(round(sum(p4ROI4 .* p4yIdx, 'all') / p40));
        if (GPU)
            p4cx = gather(p4cx);
            p4cy = gather(p4cy);
        end
        p4Loc = [p4cx + p4ROIx, p4cy + p4ROIy];

         % calculate p4 ROI
        [p4ROIx, p4ROIxEnd, p4ROIy, p4ROIyEnd] = getROI(p4Loc, params.p4.roi, videoSize);

        % blur p4 ROI
        p4ROI = double(gImg(p4ROIy:p4ROIyEnd, p4ROIx:p4ROIxEnd));
        sp4ROI = imgaussfilt(p4ROI, params.gaussianKernelSize);

        % localize p4 ROI by radial symmtry center
        [p4x, p4y, p4Radius] = radialcenter(sp4ROI, GPU);
        if (GPU)
            p40 = gather(p40);
            p4x = gather(p4x);
            p4y = gather(p4y);
            p4Radius = gather(p4Radius);
            p4Score = gather(p4Score);
        end
   
        data.p4.trace(1, frameIdx) = p4x + double(p4ROIx);
        data.p4.trace(2, frameIdx) = p4y + double(p4ROIy);
        data.p4.radius(frameIdx) = p4Radius;
        data.p4.score(frameIdx) = p4Score;
        data.p4.intensity(frameIdx) = p40 / params.p4.roi / params.p4.roi; 
        data.p4.roi(:, frameIdx) = [p4ROIx p4ROIy];
        
        % visualization
        if (visualize)
            set(handles, 'CData', vis);
            set(handleP1ROI, ...
                'XData', [p1ROIx, p1ROIxEnd, p1ROIxEnd, p1ROIx, p1ROIx], ...
                'YData', [p1ROIy, p1ROIy, p1ROIyEnd, p1ROIyEnd, p1ROIy]);
             set(handleP4ROI, ...
                'XData', [p4ROIx, p4ROIxEnd, p4ROIxEnd, p4ROIx, p4ROIx], ...
                'YData', [p4ROIy, p4ROIy, p4ROIyEnd, p4ROIyEnd, p4ROIy]);
            drawnow();
        end
        %fprintf('frameIdx: %d\n', frameIdx)
        pause(params.pauseDuration);
    end
    data.trace = data.p4.trace - data.p1.trace;
    
    if (visualize)
        close(vfig);
    end
    fprintf("Tracking finished\n");
end


function [ROIx, ROIxEnd, ROIy, ROIyEnd] = getROI(loc, boxSize, imgSize)
    ROIx = int32(loc(1) - boxSize / 2 + 1);
    ROIx = max(1, ROIx);
    ROIxEnd = ROIx + boxSize - 1;
    ROIxEnd = min(imgSize(2), ROIxEnd);
    
    ROIy = int32(loc(2) - boxSize / 2 + 1);
    ROIy = max(1, ROIy);
    ROIyEnd = ROIy + boxSize - 1;
    ROIyEnd = min(imgSize(1), ROIyEnd);
end

function [xc, yc] = lsradialcenterfit(m, b, w)
    wm2p1 = w./(m.*m+1);
    sw  = sum(wm2p1, 'all');
    smmw = sum(m.*m.*wm2p1, 'all');
    smw  = sum(m.*wm2p1, 'all');
    smbw = sum(m.*b.*wm2p1, 'all');
    sbw  = sum(b.*wm2p1, 'all');
    det = smw*smw - smmw * sw;
    xc = (smbw*sw - smw*sbw) / det;    % relative to image center
    yc = (smbw*smw - smmw*sbw) / det; % relative to image center

end

function [xc, yc, sigma] = radialcenter(I, GPU)

    % Number of grid points
    [Ny, Nx] = size(I);
    h = ones(3) / 9;  % simple 3x3 averaging filter
    if GPU
        Nx = gpuArray(Nx);
        Ny = gpuArray(Ny);
        h = gpuArray(h);
    end
    
    % grid coordinates are -n:n, where Nx (or Ny) = 2*n+1
    % grid midpoint coordinates are -n+0.5:n-0.5;
    xm_onerow = -(Nx - 1) / 2.0 + 0.5:(Nx - 1) / 2.0 - 0.5;
    xm = xm_onerow(ones(Ny-1, 1), :);
    ym_onecol = (-(Ny-1) / 2.0 + 0.5:(Ny-1) / 2.0 - 0.5)';  % Note that y increases "downward"
    ym = ym_onecol(:,ones(Nx-1,1));

    % Calculate derivatives along 45-degree shifted coordinates (u and v)
    % Note that y increases "downward" (increasing row number) -- we'll deal
    % with this when calculating "m" below.
    dIdu = I(1:Ny-1, 2:Nx) - I(2:Ny, 1:Nx-1);
    dIdv = I(1:Ny-1, 1:Nx-1) - I(2:Ny, 2:Nx);
    
    % Smoothing -- 
    fdu = conv2(dIdu, h, 'same');
    fdv = conv2(dIdv, h, 'same');
    dImag2 = fdu.*fdu + fdv.*fdv; % gradient magnitude, squared

    % Slope of the gradient .  Note that we need a 45 degree rotation of 
    % the u,v components to express the slope in the x-y coordinate system.
    % The negative sign "flips" the array to account for y increasing
    % "downward"
    m = -(fdv + fdu) ./ (fdu-fdv); 

    % *Very* rarely, m might be NaN if (fdv + fdu) and (fdv - fdu) are both
    % zero.  In this case, replace with the un-smoothed gradient.
    NNanm = sum(isnan(m(:)));
    if NNanm > 0
        unsmoothm = (dIdv + dIdu) ./ (dIdu-dIdv);
        m(isnan(m))=unsmoothm(isnan(m));
    end
    % If it's still NaN, replace with zero. (Very unlikely.)
    NNanm = sum(isnan(m(:)));
    if NNanm > 0
        m(isnan(m))=0;
    end

    % Almost as rarely, an element of m can be infinite if the smoothed u and v
    % derivatives are identical.  To avoid NaNs later, replace these with some
    % large number -- 10x the largest non-infinite slope.  The sign of the
    % infinity doesn't matter
    try
        m(isinf(m))=10 * max(m(~isinf(m)));
    catch
        % if this fails, it's because all the elements are infinite.  Replace
        % with the unsmoothed derivative.  There's probably a more elegant way
        % to do this.
        m = (dIdv + dIdu) ./ (dIdu-dIdv);
    end

    % Shorthand "b", which also happens to be the
    % y intercept of the line of slope m that goes through each grid midpoint
    b = ym - m .* xm;

    % Weighting: weight by square of gradient magnitude and inverse 
    % distance to gradient intensity centroid.
    sdI2 = sum(dImag2(:));
    xcentroid = sum(dImag2.*xm, 'all') / sdI2;
    ycentroid = sum(dImag2.*ym, 'all') / sdI2;
    w  = dImag2./sqrt((xm-xcentroid).*(xm-xcentroid)+(ym-ycentroid).*(ym-ycentroid));  

    % least-squares minimization to determine the translated coordinate
    % system origin (xc, yc) such that lines y = mx+b have
    % the minimal total distance^2 to the origin:
    % See function lsradialcenterfit (below)
    [xc, yc] = lsradialcenterfit(m, b, w);

    %%
    % Return output relative to upper left coordinate
    xc = xc + (Nx+1)/2.0;
    yc = yc + (Ny+1)/2.0;

    % A rough measure of the particle width.
    % Not at all connected to center determination, but may be useful for tracking applications; 
    % could eliminate for (very slightly) greater speed
    Isub = I - min(I(:));
    [px,py] = meshgrid(1:Nx,1:Ny);
    xoffset = px - xc;
    yoffset = py - yc;
    r2 = xoffset.*xoffset + yoffset.*yoffset;
    sigma = sqrt(sum( sum(Isub .*r2)) / sum(Isub(:) )) / 2;  % second moment is 2*Gaussian width

    if GPU
        xc = gather(xc);
        yc = gather(yc);
        sigma = gather(sigma);
    end
    
end
