function G = HumanStabilized_Temporal(tempFreq, spatFreq, flag)
% This m file computes the temporal sensitivity profile of a human under
% stabilized conditions according to Kelly (1979) Motion and vision II
% (Equation 5-8) though it is written here as a function of temporal
% frequency instead of velocity.
%G = HumanStabilized_Temporal(tempFreq, spatFreq, flag)
% G(a, v) = [6.1+7.3abs(log(v/3))^3)*va^2 exp(-2a(v+2)/45.9)
% inputs:
%   tempFreq = vector of temporal frequencies (or vector of velocities)
%   spatFreq = vector of spatial frequencies
%   flag = 'Freq' or 'Vel' for tempFreq being frequencies of velocities
% outputs:
%   G is 2D array with dimensions [len(tempFreq), len(spatFreq)]
% author: Janis Intoy, August 22, 2017

switch flag
    case 'Freq'
        tempFreq = tempFreq(:) * 2 * pi; % convert to angular frequencies
        spatFreq = spatFreq(:)' * 2 * pi;% by Kelly's definition
        
        v = bsxfun(@rdivide, tempFreq, spatFreq); % convert to velocity
    case 'Vel'
        v = repmat(tempFreq(:), [1, length(spatFreq)]);
        spatFreq = spatFreq(:)' * 2 * pi;
end

k = 6.1 + 7.3 * abs(log10(v / 3)).^3; % equation 6
amax = 45.9 ./ (v + 2); % equation 7

tmp1 = bsxfun(@times, v, spatFreq.^2) .* k; % first part of equation 5
tmp2 = exp(-2 * bsxfun(@rdivide, spatFreq, amax)); % second part of equation 5

G = tmp1 .* tmp2; % equation 5 or equivalantly equation 8
end