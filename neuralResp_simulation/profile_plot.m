tf=0.01:0.1:30;

sf=0.1:0.1:30; 

figure; hold on;
PixelAngle=1;
for Ecc=[0 7 15 25 40]
[~, ~,MC,MS] = MacaqueRetinaM_Spatial_CK(PixelAngle, Ecc, 100, 100,sf);
[~, ~,PC,PS] = MacaqueRetinaP_Spatial_CK(PixelAngle, Ecc, 100, 100,sf);
plot(sf,MC-MS,'color',[1-Ecc/50 Ecc/50 Ecc/50],'lineWidth',2,'lineStyle','--');
plot(sf,PC-PS,'color',[Ecc/50 Ecc/50 1-Ecc/50],'lineWidth',2);
end
set(gca,'xscale','log','yscale','log');
xlim([0.4 30]); ylim([0.01 20]);


%%
[~,K_m,w_m] = MacaqueRetinaM_Temporal_BK();
[~,K_p,w_p] = MacaqueRetinaP_Temporal_BK();

figure; hold on; 
plot(w_p*1000,abs(K_p),'lineWidth',2)
plot(w_m,abs(K_m),'lineWidth',2)
set(gca,'xscale','log','yscale','log');