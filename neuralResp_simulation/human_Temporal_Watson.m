function K= human_Temporal_Watson(w)
% [h_Temporal, K, w] = human_Temporal_Watson(NSteps)
% input:
%   NSteps - number of time steps in h_Temporal
% outputs:
%   h_Temporal - impulse response
%   K - transfer function
%   w - frequencies of K
%
% Implementation based on Watson 1986
% written by Janis Intoy 10/17/2016

% parameters from Figure 6.5 (c)
kappa = 1.33;
n1 = 9;
n2 = 10;

tau = 4.94 / 1000;
zeta = 1; % transient response
ksi = 200; % sensitivity factor

% if nargin==0
%     NSteps = 300;
% end

% time step (IN S!)
TStep = .001;
MaxFreq = .5*(1/TStep);

% impulse response
% t = (0:NSteps-1) * TStep;
% h1 = (t / tau).^(n1 - 1) .* exp(-t / tau) / (tau * factorial(n1-1));
% h2 = (t / (tau*kappa)).^(n2 - 1) .* exp(-t / (tau*kappa)) / (tau*kappa * factorial(n2-1));
% h_Temporal = ksi * (h1 - zeta * h2);

% transfer function
% w = linspace(0, MaxFreq, NSteps);
H1 = (2 * pi * 1i * w * tau + 1).^(-n1);
H2 = (2 * pi * 1i * w * tau*kappa + 1) .^ (-n2);

H = ksi * (H1 - zeta * H2);
K = abs(H);

% sanity check - there's a scaling issue but this might be okay for now?
%{
freq = (0:NSteps/2 + 1) / TStep / NSteps;
F = fft(h_Temporal) / (NSteps / 2);
F = abs(F(1:length(freq)));
Fmax = max(F);

figure(1001); clf; hold on;
plot(freq, abs(F(1:length(freq))), 'k');
plot(w, K * Fmax / max(K), 'r-');
%}
end
